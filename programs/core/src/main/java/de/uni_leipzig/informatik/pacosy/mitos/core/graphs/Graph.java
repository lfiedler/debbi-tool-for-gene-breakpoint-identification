package de.uni_leipzig.informatik.pacosy.mitos.core.graphs;

import de.uni_leipzig.informatik.pacosy.mitos.core.fastalignment.PairwiseAligner;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.ColumnFunctions;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.Spark;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.ColumnIdentifier;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.DBGEdgeDT;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.expressions.Window;
import org.apache.spark.sql.expressions.WindowSpec;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.graphframes.GraphFrame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.IntegerType;
import static org.apache.spark.sql.types.DataTypes.StringType;

public class Graph {
    public static int k =12;

    public static String EDGE_TABLE_NAME = ProjectDirectoryManager.getGRAPH_DATA_DIR() + "/k"+k+"/EDGE";
    public static String TOPOLOGY_EDGE_TABLE_NAME = ProjectDirectoryManager.getGRAPH_DATA_DIR() + "/k"+k+ "/TOPOLOGY_EDGE";



    public static void setK(int k) {
        Graph.k = k;
        EDGE_TABLE_NAME = ProjectDirectoryManager.getGRAPH_DATA_DIR() + "/k"+k+"/EDGE";
        String VERTEX_TABLE_NAME = ProjectDirectoryManager.getGRAPH_DATA_DIR() + "/k"+k+ "/VERTEX";
        TOPOLOGY_EDGE_TABLE_NAME = ProjectDirectoryManager.getGRAPH_DATA_DIR() + "/k"+k+ "/TOPOLOGY_EDGE";
    }
    private static Logger LOGGER = LoggerFactory.getLogger(Graph.class);


    public static Dataset<Row> createPropertyRanges(String pathToBedFiles, Dataset<Row> recordData) {
        return
                Spark.getInstance().read().text(pathToBedFiles+"/*.bed").
                        withColumn("s",split(col(ColumnIdentifier.VALUE),"\t")).
                filter(size(col("s")).equalTo(6)).
                drop(ColumnIdentifier.VALUE).
//                withColumn(ColumnIdentifier.NAME,element_at(col("s"),1)).
        withColumn(ColumnIdentifier.NAME,regexp_replace(element_at(col("s"),1)," ","")).

                        withColumn(ColumnIdentifier.START,element_at(col("s"),2).cast(DataTypes.IntegerType)).
                        withColumn(ColumnIdentifier.END,element_at(col("s"),3).cast(DataTypes.IntegerType)).
                        withColumn(ColumnIdentifier.STRAND,element_at(col("s"),6)).

                        withColumn(ColumnIdentifier.PROPERTY,element_at(col("s"),4)).
                        withColumn(ColumnIdentifier.PROPERTY,regexp_replace(col(ColumnIdentifier.PROPERTY),"\\(.*\\)","")).
                        withColumn(ColumnIdentifier.PROPERTY,regexp_replace(col(ColumnIdentifier.PROPERTY),"_.*","")).
                        withColumn(ColumnIdentifier.CATEGORY,
                                when(col(ColumnIdentifier.PROPERTY).isInCollection(Arrays.asList("OH","OL")),"repOrigin").
                                        when(substring(col(ColumnIdentifier.PROPERTY),1,3).equalTo("trn"),"trna").
                                        when(substring(col(ColumnIdentifier.PROPERTY),1,3).equalTo("rrn"),"rrna").
                                        otherwise("protein")).
//                groupBy().agg(collect_set(ColumnIdentifier.NAME)).
        join(recordData.select(col(ColumnIdentifier.NAME).as("n"),col(ColumnIdentifier.RECORD_ID),col(ColumnIdentifier.LENGTH).as(ColumnIdentifier.SEQUENCE_LENGTH)),
        col(ColumnIdentifier.NAME).equalTo(col("n"))).drop("n","s",ColumnIdentifier.NAME).
                        withColumn(ColumnIdentifier.END,
                                when(col(ColumnIdentifier.END).notEqual(col(ColumnIdentifier.SEQUENCE_LENGTH).minus(1)),col(ColumnIdentifier.END).minus(1)).
                                        otherwise(col(ColumnIdentifier.END))).
                        withColumn(ColumnIdentifier.LENGTH, ColumnFunctions.getLinearRangeLength(ColumnIdentifier.START,ColumnIdentifier.END)).
                        withColumn(ColumnIdentifier.PROPERTY,regexp_replace(col(ColumnIdentifier.PROPERTY),"trn","")).
                        withColumn(ColumnIdentifier.GENE,concat_ws("",col(ColumnIdentifier.PROPERTY),col(ColumnIdentifier.STRAND))).
                        withColumn(ColumnIdentifier.STRAND,when(col(ColumnIdentifier.STRAND).equalTo("+"),true).otherwise(false));
    }

    private static GraphFrame createDBGraph(Map<Integer,String> sequenceMap, Map<Integer,Boolean> strandMap, int k, Map<Integer,Boolean> topologyMap, String edgePath, String vertexPath) {



        int counter = sequenceMap.size();
        for(Map.Entry<Integer,String> m : sequenceMap.entrySet()) {
            List<DBGEdgeDT> edges = new ArrayList<>();
            String sequence = m.getValue();
            boolean topology = topologyMap.get(m.getKey());
            boolean strand = strandMap.get(m.getKey());
            for(int pos = 0; pos < sequence.length()-k; pos++) {
                String kmer = sequence.substring(pos,pos+k+1);
                if(!strand) {
                    kmer = PairwiseAligner.reverseComplement(kmer);
                    edges.add(new DBGEdgeDT(
                            kmer.substring(0,kmer.length()-1),
                            kmer.substring(1),
                            m.getKey(),
                            pos,
                            strand
                    ));
                }
                else {
                    edges.add(new DBGEdgeDT(
                            kmer.substring(0,kmer.length()-1),
                            kmer.substring(1),
                            m.getKey(),
                            pos+k,
                            strand
                    ));
                }

            }
//            System.out.println("end:");
            if(topology) {
                for(int pos = k; pos > 0; pos--) {
                    String kmer = sequence.substring(sequence.length()-pos,sequence.length()) + sequence.substring(0,-pos+k+1);
                    if(!strand) {
                        kmer = PairwiseAligner.reverseComplement(kmer);
                        edges.add(new DBGEdgeDT(
                                kmer.substring(0,kmer.length()-1),
                                kmer.substring(1),
                                m.getKey(),
                                sequence.length()-pos,
                                strand
                        ));
                    }
                    else {
                        edges.add(new DBGEdgeDT(
                                kmer.substring(0,kmer.length()-1),
                                kmer.substring(1),
                                m.getKey(),
                                -pos+k,
                                strand
                        ));
                    }


                }
            }
            Dataset<DBGEdgeDT> edgeDataset = SparkComputer.createDataFrame(edges,DBGEdgeDT.ENCODER);
            SparkComputer.persistDataFrameORCAppend(edgeDataset,edgePath);
            LOGGER.info(counter+"");
            counter--;

        }

//        edgeDataset.show();
//        edgeDataset.select(col(ColumnIdentifier.SRC).as(ColumnIdentifier.ID)).
//                except(edgeDataset.select(col(ColumnIdentifier.DST).as(ColumnIdentifier.ID))).show();
//        edgeDataset.select(col(ColumnIdentifier.DST).as(ColumnIdentifier.ID)).
//                except(edgeDataset.select(col(ColumnIdentifier.SRC).as(ColumnIdentifier.ID))).show();
//        System.out.println(edgeDataset.select(col(ColumnIdentifier.SRC).as(ColumnIdentifier.ID)).
//                join(edgeDataset,col(ColumnIdentifier.DST).equalTo(ColumnIdentifier.ID),SparkComputer.JOIN_TYPES.LEFT).
//                filter(isnull(col(ColumnIdentifier.DST))).count());

//        edgeDataset.select(col(ColumnIdentifier.SRC).as(ColumnIdentifier.ID)).
//                join(edgeDataset,col(ColumnIdentifier.DST).equalTo(col(ColumnIdentifier.ID)),SparkComputer.JOIN_TYPES.LEFT).
//                filter(isnull(col(ColumnIdentifier.DST))).show();
//        edgeDataset.join(SparkComputer.appendIdentifier(edgeDataset,"2"), col(ColumnIdentifier.SRC+"2").equalTo(col(ColumnIdentifier.DST))).show();
        Dataset<Row> edgeDataset = SparkComputer.readORC(edgePath);
        SparkComputer.persistDataFrameORC(edgeDataset.select(col(ColumnIdentifier.SRC).as(ColumnIdentifier.ID)).
                union(edgeDataset.select(col(ColumnIdentifier.DST).as(ColumnIdentifier.ID))).distinct(),Arrays.asList(ColumnIdentifier.ID),vertexPath);
        Dataset<Row> vertexDataset = SparkComputer.readORC(vertexPath);
        return new GraphFrame(vertexDataset,edgeDataset);

    }

    public static GraphFrame createDBGraph(Dataset<Row> sequences, int k, Dataset<Row> recordDTDataset, boolean bothStrands, String edgePath, String vertexPath) {
        Map<Integer,String> sequenceMap = new HashMap<>();
        Map<Integer,Boolean> strandMap = new HashMap<>();
        Map<Integer,Boolean> topologyMap = new HashMap<>();
        for(Row r: sequences.collectAsList()) {
            int rec = r.getInt(r.fieldIndex(ColumnIdentifier.RECORD_ID));
            sequenceMap.put(r.getInt(r.fieldIndex(ColumnIdentifier.RECORD_ID)),r.getString(r.fieldIndex(ColumnIdentifier.SEQUENCE)));
            strandMap.put(r.getInt(r.fieldIndex(ColumnIdentifier.RECORD_ID)),true);
            topologyMap.put(rec,recordDTDataset.filter(col(ColumnIdentifier.RECORD_ID).equalTo(rec)).select(ColumnIdentifier.TOPOLOGY).as(Encoders.BOOLEAN()).first());
        }


        if(!bothStrands){
            return createDBGraph(sequenceMap,strandMap,k,topologyMap,edgePath,vertexPath);
        }
        else  {
            GraphFrame g = createDBGraph(sequenceMap,strandMap,k,topologyMap,edgePath,vertexPath+"_temp");
            Dataset<Row> edges = SparkComputer.readORC(edgePath);
            Dataset<Row> inverseEdges = computeInverseStrandEdges(edges,recordDTDataset);
            Dataset<Row> vertices = SparkComputer.readORC(vertexPath+"_temp");
            vertices = vertices.union(inverseEdges.select(col(ColumnIdentifier.SRC).as(ColumnIdentifier.ID)).
                    union(inverseEdges.select(col(ColumnIdentifier.DST).as(ColumnIdentifier.ID))).distinct()).distinct();

            SparkComputer.persistDataFrameORC(vertices,
                    Arrays.asList(ColumnIdentifier.ID),vertexPath);
            Spark.clearDirectory(vertexPath+"_temp");
//            strandMap.clear();
//            for(Row r: sequences.collectAsList()) {
//                strandMap.put(r.getInt(r.fieldIndex(ColumnIdentifier.RECORD_ID)),false);
//            }
//            GraphFrame g2 = createDBGraph(sequenceMap,strandMap,k,topologyMap,edgePath,vertexPath);
            SparkComputer.persistDataFrameORCAppend(inverseEdges,edgePath);
            edges = SparkComputer.readORC(edgePath);
            return new GraphFrame(vertices,edges);
        }
    }

    public static void createGraphWithAnnotations(Dataset<Row> propertyRanges, String path, int k) {
        Dataset<Row> recordData = SparkComputer.readORC(path+"/../RECORD_DATA");
        Dataset<Row> sequences =    SparkComputer.readORC(path+"/../SEQUENCES");
        createDBGraph(sequences,k,recordData,true,path+"/EDGE_temp",path+"/VERTEX");
        Dataset<Row> edges = SparkComputer.readORC(path+"/EDGE_temp");

        propertyRanges = propertyRanges.join(recordData.withColumnRenamed(ColumnIdentifier.RECORD_ID,"r").select("r",ColumnIdentifier.TOPOLOGY),
                col(ColumnIdentifier.RECORD_ID).equalTo(col("r"))).drop("r");
        SparkComputer.persistDataFrameORC(createGeneAnnotationGraph(propertyRanges,edges,ColumnIdentifier.GENE),Arrays.asList("src","dst","recordId"),path+"/EDGE");
        Spark.clearDirectory(path+"/EDGE_temp");
    }

    public static Dataset<Row> createGeneAnnotationGraph(Dataset<Row> propertyRanges, Dataset<Row> edges, String propertyIdentifier){
        return createGeneAnnotationGraph(propertyRanges,edges,propertyIdentifier,SparkComputer.JOIN_TYPES.LEFT);

    }

    private static Dataset<Row> createGeneAnnotationGraph(Dataset<Row> propertyRanges, Dataset<Row> edges, String propertyIdentifier, String joinType){
        int k = edges.select(length(col(ColumnIdentifier.SRC)).cast(DataTypes.IntegerType)).as(Encoders.INT()).first();
        propertyRanges = propertyRanges.filter(col(ColumnIdentifier.LENGTH).geq(k+1)).
                withColumn(ColumnIdentifier.K,lit(k));

        Dataset<Row> plusStrandProp =  propertyRanges.withColumn(ColumnIdentifier.STRAND,lit(true)).
                withColumn(ColumnIdentifier.START,pmod(col(ColumnIdentifier.START).
                        plus(col(ColumnIdentifier.K)),col(ColumnIdentifier.SEQUENCE_LENGTH))).
                withColumn("p",
                        explode(concat(array(col(ColumnIdentifier.START),col(ColumnIdentifier.END)),
                                ColumnFunctions.computeIntermediatePositions(ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.SEQUENCE_LENGTH,
                                        ColumnIdentifier.TOPOLOGY,ColumnIdentifier.STRAND,null)))).
                withColumnRenamed(ColumnIdentifier.RECORD_ID,"r").withColumnRenamed(ColumnIdentifier.STRAND,"s").
                select("r","s","p",ColumnIdentifier.CATEGORY,propertyIdentifier);

        Dataset<Row> minusStrandProp =  propertyRanges.
                withColumn(ColumnIdentifier.END,pmod(col(ColumnIdentifier.END).
                        minus(col(ColumnIdentifier.K)),col(ColumnIdentifier.SEQUENCE_LENGTH))).
                withColumn(ColumnIdentifier.STRAND,lit(true)).
                withColumn("p",
                        explode(concat(array(col(ColumnIdentifier.START),col(ColumnIdentifier.END)),
                                ColumnFunctions.computeIntermediatePositions(ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.SEQUENCE_LENGTH,
                                        ColumnIdentifier.TOPOLOGY,ColumnIdentifier.STRAND,null)))).
                withColumn(ColumnIdentifier.STRAND,lit(false)).
                withColumnRenamed(ColumnIdentifier.RECORD_ID,"r").withColumnRenamed(ColumnIdentifier.STRAND,"s").
                select("r","s","p",ColumnIdentifier.CATEGORY,propertyIdentifier);
        propertyRanges = plusStrandProp.union(minusStrandProp);
        return edges.join(propertyRanges,
                col("r").equalTo(col(ColumnIdentifier.RECORD_ID)).
                        and(col("p").equalTo(col(ColumnIdentifier.POSITION))).
                        and(col("s").equalTo(col(ColumnIdentifier.STRAND))),joinType).
                select(ColumnIdentifier.SRC,ColumnIdentifier.DST,ColumnIdentifier.RECORD_ID,ColumnIdentifier.POSITION,
                        ColumnIdentifier.STRAND,ColumnIdentifier.CATEGORY,propertyIdentifier).
                groupBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.SRC,ColumnIdentifier.DST,ColumnIdentifier.STRAND,ColumnIdentifier.POSITION).
                agg(collect_set(col(ColumnIdentifier.CATEGORY)).as(ColumnIdentifier.CATEGORIES),
                        collect_list(col(propertyIdentifier)).as(ColumnIdentifier.GENES));

    }

    public static Dataset<Row> extractBreakPointsPerGeneGroupSynthetic(Dataset<Row> genomeMapping) {


        genomeMapping =genomeMapping.filter(col(ColumnIdentifier.CATEGORY).notEqual("void")).withColumn(ColumnIdentifier.ROW_NUMBER,
                row_number().over(Window.partitionBy(ColumnIdentifier.RECORD_ID).orderBy(ColumnIdentifier.START)));
        WindowSpec w = Window.partitionBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"R").orderBy(ColumnIdentifier.ROW_NUMBER);
        WindowSpec w2 = Window.partitionBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"R").orderBy(desc(ColumnIdentifier.ROW_NUMBER));
        long maxRow = genomeMapping.select(ColumnIdentifier.ROW_NUMBER).distinct().groupBy().agg(max(ColumnIdentifier.ROW_NUMBER)).as(Encoders.INT()).first();


//        genomeMapping = genomeMapping.withColumnRenamed(ColumnIdentifier.ROW_NUMBER,ColumnIdentifier.GENE).
//                withColumnRenamed(ColumnIdentifier.ROW_NUMBER+"2",ColumnIdentifier.ROW_NUMBER).
//                withColumn(ColumnIdentifier.RECORD_ID,when(col(ColumnIdentifier.RECORD_ID).notEqual(recordId),col(ColumnIdentifier.RECORD_ID).plus(1)).otherwise(recordId));

        Dataset<Row> genomeMappingCopy = genomeMapping;


        genomeMapping = genomeMapping.
                join(SparkComputer.appendIdentifier(genomeMapping,"R"),
                                (col(ColumnIdentifier.RECORD_ID).notEqual(col(ColumnIdentifier.RECORD_ID+"R"))).
                                and(col(ColumnIdentifier.GENE).equalTo(col(ColumnIdentifier.GENE+"R")))).
                drop(ColumnIdentifier.GENE+"R",ColumnIdentifier.CATEGORY+"R",ColumnIdentifier.PROPERTY+"R").
                select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.SEQUENCE_LENGTH,ColumnIdentifier.CATEGORY,ColumnIdentifier.PROPERTY,ColumnIdentifier.STRAND,
                        ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.LENGTH,
                        ColumnIdentifier.GENE,ColumnIdentifier.ROW_NUMBER,
                        ColumnIdentifier.RECORD_ID+"R",ColumnIdentifier.SEQUENCE_LENGTH+"R",
//                        ColumnIdentifier.CATEGORY+"R",ColumnIdentifier.PROPERTY+"R",
                        ColumnIdentifier.STRAND+"R",
                        ColumnIdentifier.START+"R",ColumnIdentifier.END+"R",ColumnIdentifier.LENGTH+"R",
//                        ColumnIdentifier.GENE+"R",
                        ColumnIdentifier.ROW_NUMBER+"R");


        genomeMapping = genomeMapping.withColumn(ColumnIdentifier.FILTER,
                        pmod(col(ColumnIdentifier.ROW_NUMBER+"R").minus(lag(col(ColumnIdentifier.ROW_NUMBER+"R"),1).over(w)),
                                lit(maxRow))).
                withColumn(ColumnIdentifier.BREAKPOINT,
                        lag(col(ColumnIdentifier.GENE),1).over(w)).
                withColumn(ColumnIdentifier.ROW_PAIR,
                        lag(col(ColumnIdentifier.ROW_NUMBER+"R"),1).over(w)).
                filter(col(ColumnIdentifier.FILTER).notEqual(1)).
                withColumn(ColumnIdentifier.BREAKPOINT+"2",col(ColumnIdentifier.GENE)).
                withColumn(ColumnIdentifier.ROW_PAIR+"2",col(ColumnIdentifier.ROW_NUMBER+"R")).
                drop(ColumnIdentifier.FILTER).
                union(
                        genomeMapping.filter(col(ColumnIdentifier.ROW_NUMBER).isInCollection(Arrays.asList(1,maxRow))).
                                withColumn(ColumnIdentifier.FILTER,
                                        pmod(col(ColumnIdentifier.ROW_NUMBER+"R").minus(lag(col(ColumnIdentifier.ROW_NUMBER+"R"),1).
                                                over(w2)),lit(maxRow))).
                                withColumn(ColumnIdentifier.BREAKPOINT,
                                        lag(col(ColumnIdentifier.GENE),1).over(w2)).
                                withColumn(ColumnIdentifier.ROW_PAIR,
                                        lag(col(ColumnIdentifier.ROW_NUMBER+"R"),1).over(w2)).
                                filter(col(ColumnIdentifier.FILTER).notEqual(1)).
                                withColumn(ColumnIdentifier.BREAKPOINT+"2",col(ColumnIdentifier.GENE)).
                                withColumn(ColumnIdentifier.ROW_PAIR+"2",col(ColumnIdentifier.ROW_NUMBER+"R")).
                                drop(ColumnIdentifier.FILTER)
                );



        genomeMapping = SparkComputer.addRowNumber(genomeMapping.
                withColumn(ColumnIdentifier.RANGE,
                        when(col(ColumnIdentifier.ROW_PAIR).lt(col(ColumnIdentifier.ROW_PAIR+"2")),
                                sequence(pmod(col(ColumnIdentifier.ROW_PAIR).plus(1),lit(maxRow)),col(ColumnIdentifier.ROW_PAIR+"2").minus(1))).
                                otherwise(
                                        when(col(ColumnIdentifier.ROW_PAIR).notEqual(maxRow),

                                                concat(
                                                        sequence(col(ColumnIdentifier.ROW_PAIR).plus(1),lit(maxRow)),
                                                        sequence(lit(1),greatest(col(ColumnIdentifier.ROW_PAIR+"2").minus(1),lit(1))))
                                        ).otherwise(
                                                sequence(lit(1),greatest(col(ColumnIdentifier.ROW_PAIR+"2").minus(1),lit(1)))
                                        )
                                )),ColumnIdentifier.INDEX+"computed");

        WindowSpec GENE_WINDOW = Window.partitionBy(ColumnIdentifier.INDEX+"computed").
                orderBy(ColumnIdentifier.ID,
                        ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"R",
                        ColumnIdentifier.CATEGORY,ColumnIdentifier.PROPERTY,ColumnIdentifier.GENE,
                        ColumnIdentifier.LENGTH,ColumnIdentifier.START,ColumnIdentifier.END,
                        ColumnIdentifier.LENGTH+"R",ColumnIdentifier.START+"R",ColumnIdentifier.END+"R",
                        ColumnIdentifier.BREAKPOINT,ColumnIdentifier.BREAKPOINT+"2",
                        ColumnIdentifier.ROW_PAIR,ColumnIdentifier.ROW_PAIR+"2",
                        ColumnIdentifier.RANGE);
        WindowSpec GENE_WINDOW_UNBOUNDED = GENE_WINDOW.
                rowsBetween(Window.unboundedPreceding(),Window.unboundedFollowing());

        genomeMapping = genomeMapping.select(col("*"),posexplode(col(ColumnIdentifier.RANGE)).as(new String[]{ColumnIdentifier.ID,ColumnIdentifier.POSITION})).
                join(genomeMappingCopy.
                                select(col(ColumnIdentifier.RECORD_ID).as("rec"),
                                        col(ColumnIdentifier.ROW_NUMBER).as("row"),
                                        col(ColumnIdentifier.GENE).as("g"),
                                        col(ColumnIdentifier.LENGTH).as("l")),
                        col(ColumnIdentifier.RECORD_ID+"R").equalTo(col("rec")).
                                and(col("row").equalTo(col(ColumnIdentifier.POSITION)))).
                drop("rec","row").
                withColumn(ColumnIdentifier.INTERMEDIATE_GENES,collect_list("g").over(GENE_WINDOW_UNBOUNDED)).
                withColumn(ColumnIdentifier.DISTANCE,sum("l").over(GENE_WINDOW_UNBOUNDED)).
                withColumn(ColumnIdentifier.RANK, dense_rank().over(GENE_WINDOW)).filter(col(ColumnIdentifier.RANK).equalTo(1)).
                withColumnRenamed(ColumnIdentifier.GENE_ORDER_INDEX+"R",ColumnIdentifier.GENE_ORDER_INDEX+"2").
                withColumnRenamed(ColumnIdentifier.RECORD_ID+"R",ColumnIdentifier.RECORD_ID+"2");



        return genomeMapping.
                withColumn("last",ColumnFunctions.lastArrayElement(col(ColumnIdentifier.INTERMEDIATE_GENES))).
                withColumn(ColumnIdentifier.INTERMEDIATE_GENES,when(col(ColumnIdentifier.BREAKPOINT+"2").equalTo(col("last")),
                        ColumnFunctions.arrayRemoveLastElement(ColumnIdentifier.INTERMEDIATE_GENES)).otherwise(col(ColumnIdentifier.INTERMEDIATE_GENES))).
                select(ColumnIdentifier.INDEX+"computed",
                        ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"2",
                        ColumnIdentifier.BREAKPOINT,ColumnIdentifier.BREAKPOINT+"2",
                        ColumnIdentifier.ROW_PAIR,ColumnIdentifier.ROW_PAIR+"2",
                        ColumnIdentifier.RANGE,ColumnIdentifier.INTERMEDIATE_GENES, ColumnIdentifier.DISTANCE
                );

    }

    public static Dataset<Row> computeInverseStrandEdges(Dataset<Row> edges, Dataset<Row> recordData ){
        boolean originalstrand = edges.select(ColumnIdentifier.STRAND).first().getBoolean(0);
        int k = edges.select(length(col(ColumnIdentifier.SRC)).cast(DataTypes.IntegerType)).first().getInt(0);


        return
                edges.withColumnRenamed(ColumnIdentifier.SRC,ColumnIdentifier.SRC+"old").
//                        withColumn(ColumnIdentifier.DST+"old",col(ColumnIdentifier.DST)).
//                        withColumn(ColumnIdentifier.POSITION+"old",col(ColumnIdentifier.POSITION)).
        withColumn(ColumnIdentifier.SRC,reverse(translate(col(ColumnIdentifier.DST),"ACTGUYRSWKMBVDHN","TGACARYSWMKVBHDN"))).
                        withColumn(ColumnIdentifier.DST,reverse(translate(col(ColumnIdentifier.SRC+"old"),"ACTGUYRSWKMBVDHN","TGACARYSWMKVBHDN"))).
                        drop(ColumnIdentifier.SRC+"old")
//                        withColumn(ColumnIdentifier.POSITION,col(ColumnIdentifier.POSITION).minus(k))
                        .join(recordData.select(col(ColumnIdentifier.RECORD_ID).as("r"),col(ColumnIdentifier.LENGTH)),
                                col(ColumnIdentifier.RECORD_ID).equalTo(col("r"))).drop("r")
                        .withColumn(ColumnIdentifier.POSITION,pmod(col(ColumnIdentifier.POSITION).minus(k),col(ColumnIdentifier.LENGTH)))
//                        .withColumn(ColumnIdentifier.POSITION,
//                                when(col(ColumnIdentifier.POSITION).lt(0), col(ColumnIdentifier.LENGTH).minus(col(ColumnIdentifier.POSITION))).
//                            otherwise(col(ColumnIdentifier.POSITION)))
                        .withColumn(ColumnIdentifier.STRAND,lit(!originalstrand)).select(SparkComputer.getColumns(edges.columns()));
    }

    public static Dataset<Row> createEdgesFromSequences(Dataset<Row> sequences, int k, boolean strand, boolean cyclic) {
        StructType edgeStruct = new StructType(new StructField[]{
                new StructField(ColumnIdentifier.RECORD_ID, IntegerType,true, Metadata.empty()),
                new StructField(ColumnIdentifier.POSITION, IntegerType,true, Metadata.empty()),
                new StructField(ColumnIdentifier.SRC, StringType,true, Metadata.empty()),
                new StructField(ColumnIdentifier.DST, StringType,true, Metadata.empty()),
        });

        List<Row> edgesList = new ArrayList<>();
        for(Row row: sequences.collectAsList()) {
            String sequence = row.getString(row.fieldIndex(ColumnIdentifier.SEQUENCE));

            int recordId = row.getInt(row.fieldIndex(ColumnIdentifier.RECORD_ID));

            for(int pos = 0; pos < sequence.length()-k; pos++) {
                String kmer = sequence.substring(pos,pos+k+1);
                if(!strand) {
                    kmer = PairwiseAligner.reverseComplement(kmer);
                    edgesList.add(RowFactory.create(
                            recordId,
                            pos,
                            kmer.substring(0,kmer.length()-1),
                            kmer.substring(1)));
                }
                else {
                    edgesList.add(RowFactory.create(
                            recordId,
                            pos+k,
                            kmer.substring(0,kmer.length()-1),
                            kmer.substring(1)));
                }

            }
//            System.out.println("end:");
            if(cyclic) {
                for(int pos = k; pos > 0; pos--) {
                    String kmer = sequence.substring(sequence.length()-pos,sequence.length()) + sequence.substring(0,-pos+k+1);
                    if(!strand) {
                        kmer = PairwiseAligner.reverseComplement(kmer);
                        edgesList.add(RowFactory.create(
                                recordId,
                                sequence.length()-pos,
                                kmer.substring(0,kmer.length()-1),
                                kmer.substring(1)));
                    }
                    else {
                        edgesList.add(RowFactory.create(
                                recordId,
                                -pos+k,
                                kmer.substring(0,kmer.length()-1),
                                kmer.substring(1)));
                    }


                }
            }


        }
        return Spark.getInstance().createDataFrame(edgesList,edgeStruct);
    }

    public static Dataset<Row> createEdgesFromSequences(Dataset<Row> sequences, int k) {
        return
                sequences
                        .withColumn("s",concat_ws("", col(ColumnIdentifier.SEQUENCE),substring(col(ColumnIdentifier.SEQUENCE),1,k+5)))
                        .withColumn(ColumnIdentifier.POSITION,explode(sequence(lit(0),length(col(ColumnIdentifier.SEQUENCE)).minus(1))))
                        .withColumn(ColumnIdentifier.SRC,ColumnFunctions.substring(col("s"),col(ColumnIdentifier.POSITION),lit(k)))
                        .withColumn(ColumnIdentifier.DST,ColumnFunctions.substring(col("s"),col(ColumnIdentifier.POSITION).plus(1),lit(k)))
                        .withColumn(ColumnIdentifier.POSITION,pmod(col(ColumnIdentifier.POSITION).plus(k),length(col(ColumnIdentifier.SEQUENCE))))
                        .select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.POSITION,ColumnIdentifier.SRC,ColumnIdentifier.DST);

//                sequences
//
//                .withColumn(ColumnIdentifier.POSITION,explode(sequence(lit(0),length(col(ColumnIdentifier.SEQUENCE)))))
//                .withColumn(ColumnIdentifier.SRC, ColumnFunctions.substring(col(ColumnIdentifier.SEQUENCE),col(ColumnIdentifier.POSITION).minus(k),lit(k)))
//                .withColumn("d",col(ColumnIdentifier.POSITION).minus(k))
//                .withColumn("d2",lit(k).plus(col("d")).plus(1))
////                .withColumn("s",ColumnFunctions.substring(col(ColumnIdentifier.SEQUENCE),lit(0),col("d2")))
//                .withColumn(ColumnIdentifier.SRC, when(col("d").geq(-1),col(ColumnIdentifier.SRC))
//                        .otherwise(concat_ws("",col(ColumnIdentifier.SRC),
//                                ColumnFunctions.substring(col(ColumnIdentifier.SEQUENCE),lit(0),col("d2")))))
//                .withColumn(ColumnIdentifier.DST,
//                        ColumnFunctions.substring(col(ColumnIdentifier.SEQUENCE),col(ColumnIdentifier.POSITION).minus(k).plus(1),lit(k)))
//                .withColumn("d",col("d").plus(1))
//                .withColumn("d2",col("d2").plus(1))
//                .withColumn(ColumnIdentifier.DST, when(col("d").geq(-1),col(ColumnIdentifier.DST))
//                .otherwise(concat_ws("",col(ColumnIdentifier.DST),
//                        ColumnFunctions.substring(col(ColumnIdentifier.SEQUENCE),lit(0),col("d2")))))
//                .orderBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.POSITION).
//                select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.POSITION,ColumnIdentifier.SRC,ColumnIdentifier.DST);

    }




}
