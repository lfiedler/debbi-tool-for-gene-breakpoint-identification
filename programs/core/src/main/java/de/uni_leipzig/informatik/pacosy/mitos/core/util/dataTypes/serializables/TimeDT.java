package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.Auxiliary;
import org.apache.spark.sql.*;

import java.io.Serializable;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.round;

public class TimeDT implements Serializable {
    protected String name;
    protected String taxonomicGroupName;
    protected int taxId;
    protected long dataPersistenceTime;
    protected long gapBridgingTime;
    protected long clusteringTime;
    public static final Encoder<TimeDT> ENCODER = Encoders.bean(TimeDT.class);
    public static final String TIMES_DIR = ProjectDirectoryManager.getSTATISTICS_DIR()+"/times";



    public TimeDT(String name, int taxId, long dataPersistenceTime, long gapBridgingTime, long clusteringTime) {
        this.name = name;

        this.taxId = taxId;
        this.dataPersistenceTime = dataPersistenceTime;
        this.gapBridgingTime = gapBridgingTime;
        this.clusteringTime = clusteringTime;
    }


    public TimeDT() {
    }

    public static Dataset<TimeDT> fetchTimes(){
        return SparkComputer.read(TIMES_DIR,ENCODER);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTaxonomicGroupName() {
        return taxonomicGroupName;
    }

    public void setTaxonomicGroupName(String taxonomicGroupName) {
        this.taxonomicGroupName = taxonomicGroupName;
    }

    public int getTaxId() {
        return taxId;
    }

    public void setTaxId(int taxId) {
        this.taxId = taxId;
    }

    public long getDataPersistenceTime() {
        return dataPersistenceTime;
    }

    public void setDataPersistenceTime(long dataPersistenceTime) {
        this.dataPersistenceTime = dataPersistenceTime;
    }

    public long getGapBridgingTime() {
        return gapBridgingTime;
    }

    public void setGapBridgingTime(long gapBridgingTime) {
        this.gapBridgingTime = gapBridgingTime;
    }

    public long getClusteringTime() {
        return clusteringTime;
    }

    public void setClusteringTime(long clusteringTime) {
        this.clusteringTime = clusteringTime;
    }

    public static double asMinutes(long time) {
        return time/60000.0;
    }

    public double fullTimeAsMinutes() {
        return asMinutes(gapBridgingTime+clusteringTime+dataPersistenceTime);
    }

    public static void prettyPrint(Dataset<TimeDT> times) {
        Auxiliary.printSeq(times.collectAsList());
    }
    @Override
    public String toString() {
        return //"TimeDT{" +
                "name='" + name + '\'' +
                ", taxonomicGroupName='" + taxonomicGroupName + '\'' +
                ", taxId=" + taxId +
                ", dataPersistenceTime=" + asMinutes(dataPersistenceTime) +
                ", gapBridgingTime=" + asMinutes(gapBridgingTime) +
                ", clusteringTime=" + asMinutes(clusteringTime);
//                '}';
    }


}
