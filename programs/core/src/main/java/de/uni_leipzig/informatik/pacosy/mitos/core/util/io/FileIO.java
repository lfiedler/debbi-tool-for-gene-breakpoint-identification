package de.uni_leipzig.informatik.pacosy.mitos.core.util.io;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.uni_leipzig.informatik.pacosy.mitos.core.fastalignment.PairwiseAligner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileIO {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileIO.class);
    private static ObjectMapper objectMapper;

    static {
        objectMapper = new ObjectMapper();
    }

    private FileIO() {

    }

    public static List<String> getFileNamesStartWith(String directory, String startIdentifiers) {
        try (Stream<Path> walk = Files.walk(Paths.get(directory)).
                filter(p ->  p.toFile().getName().startsWith(startIdentifiers))) {
            return walk.map(path -> path.toFile().getName()).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<String> getFileNamesStartWith(String directory, List<String> startIdentifiers) {
        return startIdentifiers.stream().flatMap( s -> getFileNamesStartWith(directory,s).stream()).collect(Collectors.toList());
    }

    public static List<File> getFilesStartWith(String directory, String startIdentifiers) {
        try (Stream<Path> walk = Files.walk(Paths.get(directory)).
                filter(p ->  p.toFile().getName().startsWith(startIdentifiers))) {
            return walk.map(path -> path.toFile()).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<File> getFilesStartWith(String directory, List<String> startIdentifiers) {
        return startIdentifiers.stream().flatMap( s -> getFilesStartWith(directory,s).stream()).collect(Collectors.toList());
    }

    public static String getFilePath(String filename, String defaultDirectory) {
        if(!filename.contains("/")) {
            filename = defaultDirectory +"/" + filename;
        }
        return filename;
    }

    public static void showFileContent(String filename) {

        try {
            BufferedReader br = new BufferedReader(new FileReader(filename));
            String line;
            while((line = br.readLine()) != null) {
                System.out.println(line);
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String serializeJsonAsString(Object object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void serializeJson(File file, Object object) {
        try {
            objectMapper.writeValue(file,object);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    public static <T> T deSerializeJson(File file, Class<T> tClass) {
        try {
            return objectMapper.readValue(file,tClass);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return null;
    }

    public static <T> T deSerializeJson(String jsonString, Class<T> tClass) {
        try {
            return objectMapper.readValue(jsonString,tClass);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return null;
    }

    public static <T> List<T> deSerializeJsonList(File file, Class<T> tClass) {
        try {
            return objectMapper.readValue(file,objectMapper.getTypeFactory().constructCollectionType(List.class, tClass) );
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return null;
    }

    public static void showFileContent(String filename, Integer linelimit) {

        try {
            int linecounter = 0;
            BufferedReader br = new BufferedReader(new FileReader(filename));
            String line;

            while((line = br.readLine()) != null && linecounter <= linelimit) {
                System.out.println(line);
                linecounter++;
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String readStringFromFile(String fileName, char commentCharacter, boolean newLine) {
        StringBuilder contentBuilder = new StringBuilder();

        Function<String,String> parser = newLine ? (s -> s + "\n") : (s -> s);
        try (Stream<String> stream = Files.lines( Paths.get(fileName), StandardCharsets.UTF_8))
        {

            stream.filter(s -> s.charAt(0) != commentCharacter)
                    .forEach(s ->  contentBuilder.append(parser.apply(s)));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return contentBuilder.toString();
    }

    public static String readStringFromFileSkipLines(String filename, int lineSkip) {

        String result = "";
        if(!new File(filename).exists()) {
            LOGGER.warn("File " + filename + " does not exist");
            return null;
        }
        try {
            BufferedReader br = new BufferedReader(new FileReader(filename));
            String line;
            long linecounter = 0;
            while((line = br.readLine()) != null) {
                if(linecounter >= lineSkip) {
                    result += line +"\n";
                }
                linecounter++;
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String readStringFromFile(File fileName, char commentCharacter, boolean newLine) {
        return readStringFromFile(fileName.getAbsolutePath(),commentCharacter,newLine);
    }

    public static <K> void readSeqFromFile(String filename, Collection<K> m, boolean header, long lineSkip, long lineStop, Function<String,K> mapper) {
        if(lineSkip > lineStop) {
            LOGGER.error("Lineskip " + lineSkip + " exceeds linestop" + lineStop);
            return;
        }
        try {
            BufferedReader br = new BufferedReader(new FileReader(filename));
            String line;
            if(header) {
                line = br.readLine();
            }
            long linecounter = 0;
            while((line = br.readLine()) != null && linecounter < lineStop) {
                if(linecounter >= lineSkip) {
                    m.add(mapper.apply(line));
                }
                linecounter++;
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static <K> void readSeqFromFile(String filename, Collection<K> m, boolean header, long lineSkip, long lineStop, Class<K> elementClass) {
        if(lineSkip > lineStop) {
            LOGGER.error("Lineskip " + lineSkip + " exceeds linestop" + lineStop);
            return;
        }
        if(!new File(filename).exists()) {
            LOGGER.warn("File " + filename + " does not exist");
            return;
        }
        try {
            BufferedReader br = new BufferedReader(new FileReader(filename));
            String line;
            if(header) {
                line = br.readLine();
            }
            long linecounter = 0;
            while((line = br.readLine()) != null && linecounter < lineStop) {
                if(linecounter >= lineSkip) {
                    if(elementClass.equals(Integer.class)) {
                        m.add((K)Integer.valueOf(line));
                    }
                    else if(elementClass.equals(Long.class)) {
                        m.add((K)Long.valueOf(line));
                    }
                    else if(elementClass.equals(String.class)) {
                        m.add((K)line);
                    }
                }
                linecounter++;
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Read data in stored in file of name filename into map m.
     * Running this method first with lineskip=0, lineStop=n and then with lineskip=n,linestop=Long.MAX_VALUE effectively reads all data.
     * @param filename
     * @param m Map to read data into
     * @param header If file contains a header that should be skipped
     * @param lineseparator
     * @param lineSkip Amount of lines that are skipped (e.g. lineSkip=2 starts at the third line)
     * @param lineStop Amount of lines to be read
     * @param keyClass
     * @param valueClass
     * @param <K>
     * @param <V>
     */
    public static <K,V> void readMapFromFile(String filename, Map<K,V> m, boolean header, String lineseparator,
                                              long lineSkip, long lineStop, Class<K> keyClass, Class<V> valueClass) {
        if(lineSkip > lineStop) {
            LOGGER.error("Lineskip " + lineSkip + " exceeds linestop" + lineStop);
            return;
        }
        try {
            BufferedReader br = new BufferedReader(new FileReader(filename));
            String line;
            if(header) {
                line = br.readLine();
            }
            long linecounter = 0;

            while((line = br.readLine()) != null && linecounter < lineStop) {
                if(linecounter >= lineSkip) {
                    String[] parts = line.split(lineseparator);
                    if(keyClass.equals(Integer.class)) {
                        if(valueClass.equals(Integer.class)) {
                            m.put((K)Integer.valueOf(parts[0]),(V)Integer.valueOf(parts[1]));
                        }
                        else if(valueClass.equals(Long.class)) {
                            m.put((K)Integer.valueOf(parts[0]),(V)Long.valueOf(parts[1]));
                        }
                        else if(valueClass.equals(String.class)) {
                            m.put((K)Integer.valueOf(parts[0]),(V)parts[1]);
                        }

                    }
                    else if(keyClass.equals(Long.class)) {
                        if(valueClass.equals(Integer.class)) {
                            m.put((K)Long.valueOf(parts[0]),(V)Integer.valueOf(parts[1]));
                        }
                        else if(valueClass.equals(Long.class)) {
                            m.put((K)Long.valueOf(parts[0]),(V)Long.valueOf(parts[1]));
                        }
                        else if(valueClass.equals(String.class)) {
                            m.put((K)Long.valueOf(parts[0]),(V)parts[1]);
                        }

                    }
                    else if(keyClass.equals(String.class)) {
                        if(valueClass.equals(Integer.class)) {
                            m.put((K)parts[0],(V)Integer.valueOf(parts[1]));
                        }
                        else if(valueClass.equals(Long.class)) {
                            m.put((K)parts[0],(V)Long.valueOf(parts[1]));
                        }
                        else if(valueClass.equals(String.class)) {
                            m.put((K)parts[0],(V)parts[1]);
                        }

                    }
                    else if(keyClass.equals(PairwiseAligner.NUCLEOTIDE.class)) {
                        if(valueClass.equals(Integer.class)) {
                            m.put((K)PairwiseAligner.NUCLEOTIDE.get(parts[0].charAt(0)),(V)Integer.valueOf(parts[1]));
                        }
                        else if(valueClass.equals(Long.class)) {
                            m.put((K)PairwiseAligner.NUCLEOTIDE.get(parts[0].charAt(0)),(V)Long.valueOf(parts[1]));
                        }
                        else if(valueClass.equals(String.class)) {
                            m.put((K)PairwiseAligner.NUCLEOTIDE.get(parts[0].charAt(0)),(V)parts[1]);
                        }

                    }
                }
                linecounter++;
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static <K,V> void readMapFromFile(String filename, Map<K,V> m, boolean header, String lineseparator,
                                              long lineSkip, long lineStop, Function<String,K> keyMapper, Function<String,V> valueMapper) {
        if(lineSkip > lineStop) {
            LOGGER.error("Lineskip " + lineSkip + " exceeds linestop" + lineStop);
            return;
        }
        try {
            BufferedReader br = new BufferedReader(new FileReader(filename));
            String line;
            if(header) {
                line = br.readLine();
            }
            long linecounter = 0;
            while((line = br.readLine()) != null && linecounter < lineStop) {
                if(linecounter >= lineSkip) {
                    String[] parts = line.split(lineseparator);
                    m.put(keyMapper.apply(parts[0]),valueMapper.apply(parts[1]));
                }
                linecounter++;
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static <K,V> void  writeMapToFile(Map<K,V> m, String filename, boolean append) {
        writeMapToFile(m,filename,append,"",",");
    }

    public static <K,V> void  writeMapToFile(Map<K,V> m, String filename, boolean append, String header) {
        writeMapToFile(m,filename,append,header,",");
    }

    public static <K,V> void  writeMapToFile(Map<K,V> m, String filename, boolean append, String header, String lineseparator) {

        System.out.println(filename);
        try{
            BufferedWriter writer = new BufferedWriter( new FileWriter(filename,append));
            if(!header.equals("")) {
                writer.write(header + "\n");
            }
            m.forEach((u,v)-> {
                try {
                    writer.write(u + lineseparator+ v + "\n");

                } catch (IOException e) {
                    e.printStackTrace();
                }

            });
            writer.close();


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static <K> void  writeSeqToFile(Collection<K> m, String filename, boolean append) {
        writeSeqToFile(m,filename,append,"");
    }

    public static <K> void  writeSeqToFile(Collection<K> m,  String filename, boolean append, String header) {

        try{
            BufferedWriter writer = new BufferedWriter( new FileWriter(filename,append));
            if(!header.equals("")) {
                writer.write(header + "\n");
            }
            m.forEach((u)-> {
                try {
                    writer.write(u.toString() +"\n");

                } catch (IOException e) {
                    e.printStackTrace();
                }

            });
            writer.close();


        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public static <K> void  writeToFile(String m,  String filename, boolean append, String header) throws IOException {


            BufferedWriter writer = new BufferedWriter( new FileWriter(filename,append));
            if(!header.equals("")) {
                writer.write(header + "\n");
            }

            try {
                writer.write(m);

            } catch (IOException e) {
                e.printStackTrace();
            }
            writer.close();




    }

    public static String getEntryFromPropertyFile(File propertiesFile, String key) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(propertiesFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties.getProperty(key);
    }

    public static String getEntryFromPropertyFile(InputStream in, String key) {
        Properties properties = new Properties();
        try {
            properties.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties.getProperty(key);
    }

}
