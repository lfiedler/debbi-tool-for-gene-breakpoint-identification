package de.uni_leipzig.informatik.pacosy.mitos.core.fastalignment.align;

public class Alignment {
    public int mScore;
    public int mPosSeqABegin;
    public int mPosSeqAEnd;
    public int mPosSeqBBegin;
    public int mPosSeqBEnd;
    public int matchCount;

    public Alignment(int score,
                     int posSeqABegin, int posSeqAEnd,
                     int posSeqBBegin, int posSeqBEnd,
                     int matchCount) {
        this.mScore = score;
        this.mPosSeqABegin = posSeqABegin;
        this.mPosSeqAEnd = posSeqAEnd;
        this.mPosSeqBBegin = posSeqBBegin;
        this.mPosSeqBEnd = posSeqBEnd;
        this.matchCount = matchCount;
    }
}