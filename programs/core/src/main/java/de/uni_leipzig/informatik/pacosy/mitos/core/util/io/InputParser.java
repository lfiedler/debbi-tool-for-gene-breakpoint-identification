package de.uni_leipzig.informatik.pacosy.mitos.core.util.io;

import org.apache.commons.lang.ArrayUtils;

import java.io.*;
import org.apache.commons.lang.ArrayUtils;

import java.io.*;
import java.util.ArrayList;

import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.Namespace;

public abstract class InputParser {

    protected ArgumentParser parser;

    protected abstract void setArguments();


    protected String[] parseFile(File file) {
        String [] fileArgs = null;
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while((line = br.readLine()) != null) {
                if(fileArgs == null) {
                    fileArgs = line.split(" ");
                }
                else {
                    fileArgs = (String[]) ArrayUtils.addAll(fileArgs,line.split(" "));
                }

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return fileArgs;
    }

    protected abstract void executeRoutines(Namespace res) ;

    protected void parseArgs(String[] args) {
        Namespace res = parser.parseArgsOrFail(args);
        if(res.getString("file") != null) {
            parseArgs(parseFile(res.<File>get("file")));
        }
        else {
            executeRoutines(res);
        }
    }

    protected InputParser(String[] args) {

        setArguments();
        parseArgs(args);

    }
}
