package de.uni_leipzig.informatik.pacosy.mitos.core.util.io;

import de.uni_leipzig.informatik.pacosy.mitos.core.breakpointDetection.DeBBI;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.Spark;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.TimeDT;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.Namespace;
import org.apache.commons.lang.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;

public class DeBBIInputParser extends InputParser {
    private static Logger LOGGER = LoggerFactory.getLogger(DeBBIInputParser.class);
    protected DeBBIInputParser(String[] args) {
        super(args);
    }

    protected void setArguments() {
        this.parser = ArgumentParsers.newFor("DeBI").
                build().description("Specify parameters for pipe line");

        parser.addArgument("--fasta-file").type(String.class).help("path to fasta file containing all genome sequences (at least two)").required(true);
        parser.addArgument("--result-directory").type(String.class).help("path where results should be stored.").required(true);
        parser.addArgument("--dislocation-breakpoints").type(Boolean.class).action(Arguments.storeTrue()).setDefault(false).help("identify dislocation breakpoints");
        parser.addArgument("--inversion-breakpoints").type(Boolean.class).action(Arguments.storeTrue()).setDefault(false).help("identify inversion breakpoints");

//        parser.addArgument("--id").type(String.class).setDefault(3).help("job id").required(true);
        parser.addArgument("--treat-linear").type(Boolean.class).setDefault(false).help("treat sequence as linear");
        parser.addArgument("--compute-local-evalue-parameter").type(Boolean.class).action(Arguments.storeTrue()).setDefault(false).help("compute E-value parameters for supplied sequences. Otherwise, default values are used.");
        parser.addArgument("--evalue-parameter-file").type(String.class)
                .help("path to file containing E-value parameters. File must consist of a single line with comma separated values for K, lambda, and H in this order.");

        parser.addArgument("--k").type(Integer.class).setDefault(9).choices(8,10,12,14,16).help("(k+1)-mer-length. If this value is not specified, it will be computed automatically.");
        parser.addArgument("--min-distance").type(Integer.class).setDefault(9).choices(Arguments.range(1,50)).help("minimum single color branch length");
        parser.addArgument("--max-distance").type(Integer.class).setDefault(400).help("maximum single color branch length");


        //only dislocation
        parser.addArgument("--cluster-distance").type(Integer.class).setDefault(50).choices(Arguments.range(10,100)).help("cluster size (only for dislocation routine)");
        parser.addArgument("--min-genelength").type(Integer.class).setDefault(70).choices(Arguments.range(10,100)).help("length of the shortest gene (only for dislocation routine)");
        parser.addArgument("--E-value").type(Double.class).setDefault(1e-5).choices(Arguments.range(1e-10,1e-1))
                .help("Cutoff E-value for alignments. Select a larger value for poorly conserved species. Selecting a too-large value may produce many spurious predictions.");
        parser.addArgument("--min-identical").type(Integer.class).setDefault(20).choices(Arguments.range(0,100)).help("Minimum number of perfectly matching nucleotides in alignments. " +
                "Select a lower value for poorly conserved species. Selecting a too-low value may produce many spurious predictions.");

        //only inversion
        parser.addArgument("--max-genelength").type(Integer.class).setDefault(4000).help("length of the longest gene (only for inversion routine)");
        parser.addArgument("--min-IB-length").type(Integer.class).setDefault(20).choices(Arguments.range(1,100)).help("minimum size of an inversion block (only for inversion routine)");
        parser.addArgument("--global-score-rate").type(Double.class).setDefault(0.8).choices(Arguments.range(0.0,1.0))
                .help("cutoff global alignment rate: the higher, the better alignment quality must be to accept alignment (only for inversion routine)");


    }
    protected void executeRoutines(Namespace res)  {



        int k=-1;

        String path = res.getString("result_directory");
        if(res.getInt("min_distance") > res.getInt("max_distance")){
            LOGGER.error("Minimum single color branch length must be smaller than maximum single color branch length");
            System.exit(-1);
        }
        if(res.getBoolean("dislocation_breakpoints")) {
            LOGGER.info("Computing dislocation breakpoints");
            if(res.getString("evalue_parameter_file") != null) {
                String paramFile = res.getString("evalue_parameter_file");
//            System.out.println(paramFile);

                String param = FileIO.readStringFromFile(paramFile ,'#',false);
                new File(res.getString("result_directory")+"/SCORES").mkdirs();
                try {
                    FileIO.writeToFile(param,res.getString("result_directory")+"/SCORES"+"/local_parameters_o-2_e-2_match1_missmatch-2",
                            false,"\"K\",\"lambda\",\"H\"");
                } catch (IOException e) {
                    e.printStackTrace();
                    LOGGER.error("invalid evalue paramter file");
//                e.printStackTrace();
                    System.exit(-1);
                }

            }
            if(res.getString("evalue_parameter_file") == null && res.getBoolean("compute_local_evalue_parameter")){
                try {
                    if(res.getInt("k") != null) {
                        DeBBI.prepareBulgeGraphWithEvalueStat(new File(res.getString("fasta_file")),res.getString("result_directory"),true,res.getInt("k"));
                    }
                    else{
                        DeBBI.prepareBulgeGraphWithEvalueStat(new File(res.getString("fasta_file")),res.getString("result_directory"),true);
                    }


                } catch (IOException e) {
                    LOGGER.error("invalid fasta file");
//                e.printStackTrace();
                    System.exit(-1);

                }
            }
            else {
                try {
                    if(res.getInt("k") != null) {
                        DeBBI.prepareBulgeGraph(new File(res.getString("fasta_file")), res.getInt("k"), res.getString("result_directory"),true);
                    }
                    else{
                        DeBBI.prepareBulgeGraph(new File(res.getString("fasta_file")),res.getString("result_directory"),true);
                    }


                } catch (IOException e) {
                    LOGGER.error("invalid fasta file");
//                e.printStackTrace();
                    System.exit(-1);
                }
            }


            File dir = new File(path);
            File [] files = dir.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.startsWith("EDGES_");
                }
            });
            for (File edgeFile : files) {
                k = Integer.parseInt( edgeFile.getName().substring(edgeFile.getName().indexOf("_")+1));
            }
            LOGGER.info("Using:\nk = "+ k+"\n" +
                    "min-distance = "+res.getInt("min_distance")+ "\n" +
                    "max-distance = "+res.getInt("max_distance")+ "\n" +
                    "min_identical = "+res.getInt("min_identical")+ "\n" +
                    "min_genelength = "+res.getInt("min_genelength")+ "\n" +
                    "cluster_distance = "+res.getInt("cluster_distance")+ "\n" +
                    "treat_linear = "+res.getBoolean("treat_linear")+ "\n"
            );

            try {
                DeBBI.identifyBreakPoints(
                        SparkComputer.readORC(path+"/EDGES_"+k),
                        SparkComputer.readORC(path+"/SEQUENCES"),
                        SparkComputer.readORC(path+"/RECORD_DATA"),
                        path+"/DISLOCATION",
                        path+"/SCORES",res.getInt("min_genelength"),k,
                        res.getInt("min_distance"),
                        res.getInt("max_distance"),
                        res.getDouble("E_value"),
                        res.getInt("min_identical"),
                        res.getInt("cluster_distance"),
                        !res.getBoolean("treat_linear")
                );
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if(res.getBoolean("inversion_breakpoints")) {
            LOGGER.info("Computing inversion breakpoints");
            try {
                DeBBI.prepareInverseBulgeGraphWithoutEvalueScores(new File(res.getString("fasta_file")),res.getString("result_directory"));
            } catch (IOException e) {
                LOGGER.error("invalid fasta file");
//                e.printStackTrace();
                System.exit(-1);
            }



            File dir = new File(path);
            if(k < 0) {

                File [] files = dir.listFiles(new FilenameFilter() {
                    @Override
                    public boolean accept(File dir, String name) {
                        return name.startsWith("EDGES_");
                    }
                });
                for (File edgeFile : files) {
                    k = Integer.parseInt( edgeFile.getName().substring(edgeFile.getName().indexOf("_")+1));
                }
            }

            LOGGER.info("Using:\nk = "+ k+"\n" +
                    "min-distance = "+res.getInt("min_distance")+ "\n" +
                    "max-distance = "+res.getInt("max_distance")+ "\n" +
                    "min_identical = "+res.getInt("min_identical")+ "\n" +
                    "max_genelength = "+res.getInt("max_genelength")+ "\n" +
                    "min_IB_length = "+res.getInt("min_IB_length")+ "\n" +
                    "global_score_rate = "+res.getDouble("global_score_rate")+ "\n" +
                    "treat_linear = "+res.getBoolean("treat_linear")+ "\n"
            );

            try {
                DeBBI.identifyInversionBreakPoints(SparkComputer.readORC(dir +"/INV_EDGES_"+k),
                        SparkComputer.readORC(dir +"/SEQUENCES"),
                        SparkComputer.readORC(dir+"/RECORD_DATA"),
                        res.getInt("min_distance"),res.getInt("max_distance"),
                        res.getInt("min_IB_length"),res.getInt("max_genelength"),
                        res.getDouble("global_score_rate"),res.getInt("min_identical"),k,
                        dir+"/INVERSION",!res.getBoolean("treat_linear")
                );
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        Spark.clearDirectory(path+"/EDGES_"+k);
        Spark.clearDirectory(path+"/INV_EDGES_"+k);
        Spark.clearDirectory(path+"/SEQUENCES");
        Spark.clearDirectory(path+"/RECORD_DATA");
        if(!res.getBoolean("inversion_breakpoints")&&!res.getBoolean("dislocation_breakpoints")) {
            System.out.println("Specifiy at least one of: "+"--inversion-breakpoints --dislocation-breakpoints"  );
        }


    }


    public static void main(String[] args) {



       new DeBBIInputParser(args);



        System.exit(0);
    }

}
