package de.uni_leipzig.informatik.pacosy.mitos.core.breakpointDetection;

import de.uni_leipzig.informatik.pacosy.mitos.core.fastalignment.PairwiseAligner;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphs.Graph;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.ColumnFunctions;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.Spark;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.ColumnIdentifier;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.TimeDT;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.Auxiliary;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.DeBBIInputParser;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.FileIO;
import org.apache.commons.lang.time.StopWatch;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.expressions.Window;
import org.apache.spark.sql.expressions.WindowSpec;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.biojava.nbio.alignment.Alignments;
import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.io.FastaReaderHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.Int;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static de.uni_leipzig.informatik.pacosy.mitos.core.fastalignment.PairwiseAligner.getShuffledScores;
import static de.uni_leipzig.informatik.pacosy.mitos.core.graphs.Graph.extractBreakPointsPerGeneGroupSynthetic;
import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.IntegerType;
import static org.apache.spark.sql.types.DataTypes.StringType;

public class DeBBI {
    private static Logger LOGGER = LoggerFactory.getLogger(DeBBI.class);
    private static String globalEvalPath = ProjectDirectoryManager.getSTATISTICS_DIR()+"/evalParameters";

    public static void identifyBreakPoints(Dataset<Row> edges, Dataset<Row> sequences, Dataset<Row> recordData,
                                           String path, String evalPath,
                                           int minGeneLength, int k ,
                                           int minDistance, int maxDistance, double eValueThreshold, int minIdentical, int clusterDistance, boolean circular) throws IOException {


        if(new File(evalPath+"/local_parameters_o-2_e-2_match1_missmatch-2").exists()) {
            LOGGER.debug("Using supplied eValue param file");
            PairwiseAligner.setEvalueParameters(evalPath+"/local_parameters_o-2_e-2_match1_missmatch-2");
        }
        else{
            PairwiseAligner.setEvalueParameters(globalEvalPath);
        }


        {
            LOGGER.debug("Computing candidates");

            if(circular) {
                SparkComputer.persistDataFrameORC(detectBreakPointCandidates(edges,
                                recordData,minDistance,maxDistance,minGeneLength,path),
                        path+"/CANDIDATES"
                );
            }
            else {
                SparkComputer.persistDataFrameORC(detectBreakPointCandidates(edges,
                                recordData,minDistance,maxDistance,minGeneLength,path)
                        .filter(col(ColumnIdentifier.POSITION+"5").leq(col(ColumnIdentifier.POSITION+"6")).
                                        and(col(ColumnIdentifier.POSITION+"1").leq(col(ColumnIdentifier.POSITION+"4")))),
                        path+"/CANDIDATES"
                );
            }

            Spark.clearDirectory(path+"/EP");
            Spark.clearDirectory(path+"/SP");
//            Spark.clearDirectory(path+"/SP1");
            Spark.clearDirectory(path+"/SP2");
            Spark.clearDirectory(path+"/SP3");



        }

        {

            LOGGER.debug("Checking validity");
            identifyBPBulges(SparkComputer.readORC(path+"/CANDIDATES"),
                    edges,
                    sequences.drop(ColumnIdentifier.SEQUENCE_NAME),
                    path+"/BREAKPOINTS",
                    k,90,200,1,1,eValueThreshold,minIdentical);

        }

        {
            LOGGER.debug("SHIFTING BULGES");
            SparkComputer.writeToCSV(reshiftBulgePositions(
                            SparkComputer.readORC(path+"/BREAKPOINTS/FINAL").
                                    join(recordData.select(col(ColumnIdentifier.RECORD_ID).as("r"),col(ColumnIdentifier.LENGTH).as(ColumnIdentifier.LENGTH)),
                                            col("r").equalTo(col(ColumnIdentifier.RECORD_ID))).drop("r")
                                    .join(recordData.select(col(ColumnIdentifier.RECORD_ID).as("r"),col(ColumnIdentifier.LENGTH).as(ColumnIdentifier.LENGTH+"2")),
                                            col("r").equalTo(col(ColumnIdentifier.RECORD_ID+"2"))
                                    ).drop("r"),k)
                    ,
                    path+"/BREAKPOINTS/FINAL_SHIFTED");
        }


        {
            LOGGER.debug("Clustering ");

            clusterData(path,clusterDistance);

            if(Spark.checkExisting(path+"/breakpoints")) {
                SparkComputer.writeToCSVTab(
                        removeOverlaps(
                                Spark.getInstance().
                                        read().format("csv") .option("inferSchema", "true").option("sep","\t")
                                        .option("header", "true").
                                        load(path+"/breakpoints")
                                        .join(sequences.select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.SEQUENCE_NAME),col(ColumnIdentifier.RECORD_ID).equalTo(col("bp_seq_id")))
                                        .withColumn("bp_seq_id",col(ColumnIdentifier.SEQUENCE_NAME)).drop(ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID).drop(ColumnIdentifier.RECORD_ID,ColumnIdentifier.SEQUENCE_NAME)
                                        .join(sequences.select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.SEQUENCE_NAME),col(ColumnIdentifier.RECORD_ID).equalTo(col("compare_seq_id")))
                                        .withColumn("compare_seq_id",col(ColumnIdentifier.SEQUENCE_NAME)).drop(ColumnIdentifier.RECORD_ID,ColumnIdentifier.SEQUENCE_NAME),
                                clusterDistance,maxDistance,path)
                                .drop(ColumnIdentifier.RECORDS,"m")
                                .orderBy("bp_seq_id","bp_seq_p1"),
                        path+"/TMP");
                String filename = Arrays.stream(new File(path+"/TMP").listFiles()).filter(f -> f.getName().endsWith(".csv"))
                        .map(f -> f.getAbsolutePath()).collect(Collectors.toList()).get(0);



                Files.move(new File(filename).toPath(),new File(path+"/../breakpoints").toPath());
            }
            else {
                FileIO.writeToFile("bp_seq_id\tcompare_seq_id\tbp_seq_p1\tbp_seq_p2\tcompare_seq_p1\tcompare_seq_p2\tbp_seq_length\tcompare_seq_length",path+"/../breakpoints",false,"");
            }



        }


        Spark.clearDirectory(path);


    }

    public static void identifyBPBulges(Dataset<Row> candidateBP, Dataset<Row> edges, Dataset<Row> sequences, String breakPointPath, int k,
                                        int searchDistanceGlobal, int searchDistanceLocal, int distanceThreshold, int minPathLength, double evalueThreshold,int minIdentical) {

        String[] colNames = candidateBP.columns();

        // compute  sequence similarity at the bulge ends
        LOGGER.debug("Computing scores at branchends");
        String directory = breakPointPath+"/SCORES_";



        {
            Dataset<Row> path =
                    Spark.checkPointORC(
                            compareSequenceSimilarityAtBranchEnds(candidateBP,sequences,k,searchDistanceGlobal, evalueThreshold,minIdentical)
                                    .select(SparkComputer.getColumns(colNames)).
                                    withColumn(ColumnIdentifier.TOPOLOGY+"2",lit(true))
                                    .withColumn(ColumnIdentifier.INDEX,concat_ws("",
                                            col(ColumnIdentifier.RECORD_ID),col(ColumnIdentifier.RECORD_ID+"2"),
                                            col(ColumnIdentifier.POSITION+"1"),
                                            col(ColumnIdentifier.POSITION+"4"),
                                            col(ColumnIdentifier.POSITION+"5"),
                                            col(ColumnIdentifier.POSITION+"6")))
                            ,
                            directory+"BRANCHENDS");

            LOGGER.debug("Computing scores at top branch");
            evaluateUpperBranch(path,edges,sequences,breakPointPath,k,searchDistanceLocal,distanceThreshold,minPathLength,evalueThreshold,minIdentical);



        }
        Dataset<Row> scores_joined =SparkComputer.readORC(breakPointPath+"/SCORES_JOINED");

        {

            LOGGER.debug("Computing scores at bottom branch");
            SparkComputer.persistDataFrameORC(collapseEdges(computeMappingKmers(
                            scores_joined.
                                    withColumn(ColumnIdentifier.DISTANCE+"R1",
                                            pmod(col(ColumnIdentifier.POSITION+"6").minus(col(ColumnIdentifier.POSITION+"5")),
                                                    col(ColumnIdentifier.LENGTH))).
                                    filter(col(ColumnIdentifier.DISTANCE+"R1").geq(20)).
                                    select(
                                            ColumnIdentifier.INDEX,
                                            ColumnIdentifier.RECORD_ID+"2" ,ColumnIdentifier.RECORD_ID,
                                            ColumnIdentifier.POSITION+"1",
                                            ColumnIdentifier.POSITION+"2",
                                            ColumnIdentifier.POSITION+"3",
                                            ColumnIdentifier.POSITION+"4",
                                            ColumnIdentifier.POSITION+"5",
                                            ColumnIdentifier.POSITION+"6",
                                            ColumnIdentifier.LENGTH,
                                            ColumnIdentifier.LENGTH+"2").distinct().
                                    withColumn(ColumnIdentifier.STRAND,lit(true)).withColumn(ColumnIdentifier.STRAND+"2",lit(true)).
                                    withColumn(ColumnIdentifier.TOPOLOGY,lit(true)).withColumn(ColumnIdentifier.TOPOLOGY+"2",lit(true))
                            ,edges,ColumnIdentifier.RECORD_ID,ColumnIdentifier.LENGTH,
                            ColumnIdentifier.STRAND,ColumnIdentifier.TOPOLOGY,
                            ColumnIdentifier.INDEX,ColumnIdentifier.POSITION+"5",ColumnIdentifier.POSITION+"6"),
                    ColumnIdentifier.LENGTH,ColumnIdentifier.STRAND
            ),breakPointPath+"/collapsedPaths56");

            Spark.clearDirectory(Spark.SPARK_CHECKPOINT_DIR+"/path");

            extractValidPaths(SparkComputer.readORC(breakPointPath+"/collapsedPaths56"),
                    1,breakPointPath+"/validPaths56",1,ColumnIdentifier.LENGTH+"2",ColumnIdentifier.STRAND+"2");


            LOGGER.debug("Computing final candidates");
            SparkComputer.persistDataFrameORC(
                    scores_joined.select(ColumnIdentifier.INDEX,"origin",ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"2",
                                    ColumnIdentifier.POSITION+"1",ColumnIdentifier.POSITION+"4",ColumnIdentifier.POSITION+"5",ColumnIdentifier.POSITION+"6").
                    distinct()
                            .join(compareSequenceSimilarityAlongPathNoShift(SparkComputer.readORC(breakPointPath+"/validPaths56"),
                                            sequences,k,
                                            ColumnIdentifier.RECORD_ID,ColumnIdentifier.LENGTH, ColumnIdentifier.POSITION+"5")
                                            .filter(col(ColumnIdentifier.ALIGN_RESULT+"."+ColumnIdentifier.EVALUE).leq(evalueThreshold).
                                                    and(col(ColumnIdentifier.ALIGN_RESULT+"."+ColumnIdentifier.IDENTICAL).geq(20)))
                                            .select(col(ColumnIdentifier.INDEX).as("i")).distinct(),
                                    col(ColumnIdentifier.INDEX).equalTo(col("i")),
                                    SparkComputer.JOIN_TYPES.LEFT_ANTI)

                    ,
                    breakPointPath+"/FINAL"

            );


        }



    }

    /*** Evaluate Branches ***/

    public static Dataset<Row> compareSequenceSimilarityAtBranchEnds(Dataset<Row> path, Dataset<Row> sequences, int k, int searchDistance,
                                                                     double evalueThreshold,int minIdentical)  {

        String[] colNames = path.columns();


        String align_quality_udf = PairwiseAligner.ALIGN_LOCAL_RETAIN_QUALITY_FAST;

        // fetch all distinct subsequencesof r2 on the left flank
        Dataset<Row> segmentsR2 =
                PairwiseAligner.addSubsequencesToLeft(sequences,
                        path
                                .select(ColumnIdentifier.RECORD_ID+"2",ColumnIdentifier.POSITION+"1",ColumnIdentifier.LENGTH+"2").distinct()
                                .withColumn("pR2L",pmod(col(ColumnIdentifier.POSITION+"1").minus(1),col(ColumnIdentifier.LENGTH+"2")))
                        , k, searchDistance,col("pR2L"),
                        col(ColumnIdentifier.RECORD_ID+"2"))
                        .withColumnRenamed(ColumnIdentifier.SUBSEQUENCE,"LR2").drop(ColumnIdentifier.LENGTH+"2");


        // fetch all distinct subsequences of r1 on the left flank
        Dataset<Row> segmentsR1 =
                PairwiseAligner.addSubsequencesToLeft(sequences,
                                path
                                        .select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.POSITION+"5",ColumnIdentifier.LENGTH).distinct()
                                        .withColumn("pR1L",pmod(col(ColumnIdentifier.POSITION+"5").minus(1),col(ColumnIdentifier.LENGTH)))
                                , k, searchDistance,col("pR1L"),col(ColumnIdentifier.RECORD_ID)).
                        withColumnRenamed(ColumnIdentifier.SUBSEQUENCE,"LR1").drop(ColumnIdentifier.LENGTH);



        // join the subsequences back to the input bulges
        Dataset<Row> segmentsL = path.join(SparkComputer.appendIdentifier(segmentsR2,"j",ColumnIdentifier.RECORD_ID+"2",ColumnIdentifier.POSITION+"1"),
                        col(ColumnIdentifier.RECORD_ID+"2").equalTo(col(ColumnIdentifier.RECORD_ID+"2j"))
                .and(col(ColumnIdentifier.POSITION+"1").equalTo(col(ColumnIdentifier.POSITION+"1j"))))
                .drop(ColumnIdentifier.RECORD_ID+"2j",ColumnIdentifier.POSITION+"1j")
                .join(SparkComputer.appendIdentifier(segmentsR1,"j",ColumnIdentifier.RECORD_ID,ColumnIdentifier.POSITION+"5"),
                        col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"j"))
                                .and(col(ColumnIdentifier.POSITION+"5").equalTo(col(ColumnIdentifier.POSITION+"5j"))))
                .drop(ColumnIdentifier.RECORD_ID+"j",ColumnIdentifier.POSITION+"5j").persist();


        Dataset<Row> scoresL =
                segmentsL.select("LR1","LR2").distinct().
                        // query is branch with recordId, target branch with recordId2
                                filter(callUDF(align_quality_udf,col("LR1"),col("LR2"),lit(evalueThreshold),lit(minIdentical)));



        // only keep bulges of sufficient quality
        segmentsL = segmentsL.join(scoresL.select(col("LR1").as("LR1j"),col("LR2").as("LR2j")),
                        col("LR1").equalTo(col("LR1j")).and(col("LR2").equalTo(col("LR2j"))))
                .select(SparkComputer.getColumns(colNames)).persist();



        // fetch all distinct subsequencesof r2 on the right flank
        segmentsR2 =
                PairwiseAligner.addSubsequencesToRight(sequences,
                        segmentsL
                                        .select(ColumnIdentifier.RECORD_ID+"2",ColumnIdentifier.POSITION+"4",ColumnIdentifier.LENGTH+"2").distinct()
                                        .withColumn("pR2R", pmod(col(ColumnIdentifier.POSITION+"4").minus(k-1),col(ColumnIdentifier.LENGTH+"2"))),
                                searchDistance,
                                col("pR2R"),
                                col(ColumnIdentifier.RECORD_ID+"2")).withColumnRenamed(ColumnIdentifier.SUBSEQUENCE,"LR2")
                        .drop(ColumnIdentifier.LENGTH+"2");



        // fetch all distinct subsequencesof r1 on the right flank
        segmentsR1 = PairwiseAligner.addSubsequencesToRight(sequences,
                        segmentsL
                        .select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.POSITION+"6",ColumnIdentifier.LENGTH).distinct()
                        .withColumn("pR1R",pmod(col(ColumnIdentifier.POSITION+"6").minus(k-1),col(ColumnIdentifier.LENGTH))),
                        searchDistance,
                        col("pR1R"),
                        col(ColumnIdentifier.RECORD_ID)).
                withColumnRenamed(ColumnIdentifier.SUBSEQUENCE,"LR1").drop(ColumnIdentifier.LENGTH);;

        // join the subsequences back to the input bulges
       segmentsL = segmentsL.join(SparkComputer.appendIdentifier(segmentsR2,"j",ColumnIdentifier.RECORD_ID+"2",ColumnIdentifier.POSITION+"4"),
                        col(ColumnIdentifier.RECORD_ID+"2").equalTo(col(ColumnIdentifier.RECORD_ID+"2j"))
                                .and(col(ColumnIdentifier.POSITION+"4").equalTo(col(ColumnIdentifier.POSITION+"4j"))))
                .drop(ColumnIdentifier.RECORD_ID+"2j",ColumnIdentifier.POSITION+"4j")
                .join(SparkComputer.appendIdentifier(segmentsR1,"j",ColumnIdentifier.RECORD_ID,ColumnIdentifier.POSITION+"6"),
                        col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"j"))
                                .and(col(ColumnIdentifier.POSITION+"6").equalTo(col(ColumnIdentifier.POSITION+"6j"))))
                .drop(ColumnIdentifier.RECORD_ID+"j",ColumnIdentifier.POSITION+"6j").persist();
        scoresL =
                segmentsL.select("LR1","LR2").distinct().
                        // query is branch with recordId, target branch with recordId2
                filter(callUDF(align_quality_udf,col("LR1"),col("LR2"),lit(evalueThreshold),lit(minIdentical)));


        // only keep bulges of sufficient quality
        segmentsL = segmentsL.join(scoresL.select(col("LR1").as("LR1j"),col("LR2").as("LR2j")),
                        col("LR1").equalTo(col("LR1j")).and(col("LR2").equalTo(col("LR2j"))))
                .select(SparkComputer.getColumns(colNames));



        return segmentsL;
    }

    /**
     *
     * @param path
     * @param edges
     * @param recordId Columnname from NOT mapping genome
     * @param length Columnname of length from NOT mapping genome
     * @param strand Columnname of strand from NOT mapping genome
     * @param topology Columnname of topology from NOT mapping genome
     * @param rowNumber
     * @param p1
     * @param p2
     * @return
     */
    public static Dataset<Row> computeMappingKmers(Dataset<Row> path, Dataset<Row> edges, String recordId, String length,String strand, String topology, String rowNumber, String p1, String p2) {
        String recordIdOther = counterPart(recordId);
        String lengthOther = counterPart(length);
        String strandOther = counterPart(strand);
        String topologyOther = counterPart(topology);
        path = Spark.checkPoint(path.
                withColumn(ColumnIdentifier.RANGE,
                        ColumnFunctions.computeIntermediatePositions(p1,p2,
                                length,
                                topology,strand,null)).
                filter(not(isnull(col(ColumnIdentifier.RANGE)))).
                withColumn(ColumnIdentifier.POSITION_DIFF,size(col(ColumnIdentifier.RANGE)).minus(1)).
                // keep an index of the positions in range, otherwise their order (which may go beyond periodic boundaries or is reverse for negative strand) is not preserved!
                        select(col("*"),posexplode(col(ColumnIdentifier.RANGE)).as(new String[]{ColumnIdentifier.POSITION_INDEX,ColumnIdentifier.POSITION+"R"})).
                // retrieve (k+1)-mers associated with positions
                        join(SparkComputer.appendIdentifier(edges,"E"),
                        col(recordId).equalTo(col(ColumnIdentifier.RECORD_ID+"E")).
                                and(col(ColumnIdentifier.POSITION+"R").equalTo(col(ColumnIdentifier.POSITION+"E"))).
                                and(col(strand).equalTo(col(ColumnIdentifier.STRAND+"E")))
                ).drop(ColumnIdentifier.RECORD_ID+"E",ColumnIdentifier.STRAND+"E",ColumnIdentifier.POSITION+"E").
                withColumnRenamed(ColumnIdentifier.SRC+"E",ColumnIdentifier.SRC).
                withColumnRenamed(ColumnIdentifier.DST+"E",ColumnIdentifier.DST).
                select(rowNumber,
                        ColumnIdentifier.POSITION+"1",
                        ColumnIdentifier.POSITION+"2",
                        ColumnIdentifier.POSITION+"3",
                        ColumnIdentifier.POSITION+"4",
                        ColumnIdentifier.POSITION+"5",
                        ColumnIdentifier.POSITION+"6",
                        ColumnIdentifier.POSITION_INDEX,
                        ColumnIdentifier.POSITION_DIFF,
                        ColumnIdentifier.STRAND,
                        ColumnIdentifier.STRAND+"2",
                        ColumnIdentifier.TOPOLOGY,
                        ColumnIdentifier.TOPOLOGY+"2",
                        ColumnIdentifier.LENGTH,
                        ColumnIdentifier.LENGTH+"2",
                        ColumnIdentifier.SRC,ColumnIdentifier.DST,
                        ColumnIdentifier.POSITION+"R",
                        ColumnIdentifier.RECORD_ID+"2" ,ColumnIdentifier.RECORD_ID).distinct(),"path");

//        path2 = SparkComputer.readORC(breakPointPath+"/data");




        path = path.
                // select all (k+1)-mers found for a recordId2 between path2 and path3, that are also annotated for recordId
                        join(
                        SparkComputer.appendIdentifier(edges,"'"),
                        col(recordIdOther).equalTo(col(ColumnIdentifier.RECORD_ID+"'")).
                                and(col(ColumnIdentifier.SRC).equalTo(col(ColumnIdentifier.SRC+"'"))).
                                and(col(ColumnIdentifier.DST).equalTo(col(ColumnIdentifier.DST+"'"))).
                                and(col(strandOther).equalTo(col(ColumnIdentifier.STRAND+"'")))
                ).drop(ColumnIdentifier.RECORD_ID+"'",ColumnIdentifier.SRC+"'",ColumnIdentifier.DST+"'",ColumnIdentifier.STRAND+"'");

        return path;
    }

    /**
     *
     * @param path
     * @param edges
     * @param recordId Columnname from NOT mapping genome
     * @param length Columnname of length from NOT mapping genome
     * @param strand Columnname of strand from NOT mapping genome
     * @param topology Columnname of topology from NOT mapping genome
     * @param rowNumber
     * @return
     */
    public static Dataset<Row> computeMappingKmers(Dataset<Row> path, Dataset<Row> edges, String recordId, String length,String strand, String topology, String rowNumber) {
        String recordIdOther = counterPart(recordId);
        String lengthOther = counterPart(length);
        String strandOther = counterPart(strand);
        String topologyOther = counterPart(topology);
        path = Spark.checkPoint(path.
                filter(not(isnull(col(ColumnIdentifier.RANGE)))).
                withColumn(ColumnIdentifier.POSITION_DIFF,size(col(ColumnIdentifier.RANGE)).minus(1)).
                // keep an index of the positions in range, otherwise their order (which may go beyond periodic boundaries or is reverse for negative strand) is not preserved!
                        select(col("*"),posexplode(col(ColumnIdentifier.RANGE)).as(new String[]{ColumnIdentifier.POSITION_INDEX,ColumnIdentifier.POSITION+"R"})).
                // retrieve (k+1)-mers associated with positions
                        join(SparkComputer.appendIdentifier(edges,"E"),
                        col(recordId).equalTo(col(ColumnIdentifier.RECORD_ID+"E")).
                                and(col(ColumnIdentifier.POSITION+"R").equalTo(col(ColumnIdentifier.POSITION+"E"))).
                                and(col(strand).equalTo(col(ColumnIdentifier.STRAND+"E")))
                ).drop(ColumnIdentifier.RECORD_ID+"E",ColumnIdentifier.STRAND+"E",ColumnIdentifier.POSITION+"E").
                withColumnRenamed(ColumnIdentifier.SRC+"E",ColumnIdentifier.SRC).
                withColumnRenamed(ColumnIdentifier.DST+"E",ColumnIdentifier.DST).
                select(rowNumber,
                        ColumnIdentifier.POSITION+"1",
                        ColumnIdentifier.POSITION+"2",
                        ColumnIdentifier.POSITION+"3",
                        ColumnIdentifier.POSITION+"4",
                        ColumnIdentifier.POSITION+"5",
                        ColumnIdentifier.POSITION+"6",
                        ColumnIdentifier.POSITION_INDEX,
                        ColumnIdentifier.POSITION_DIFF,
                        ColumnIdentifier.STRAND,
                        ColumnIdentifier.STRAND+"2",
                        ColumnIdentifier.TOPOLOGY,
                        ColumnIdentifier.TOPOLOGY+"2",
                        ColumnIdentifier.LENGTH,
                        ColumnIdentifier.LENGTH+"2",
                        ColumnIdentifier.SRC,ColumnIdentifier.DST,
                        ColumnIdentifier.POSITION+"R",
                        ColumnIdentifier.RECORD_ID+"2" ,ColumnIdentifier.RECORD_ID).distinct(),"path");

//        path2 = SparkComputer.readORC(breakPointPath+"/data");




        path = path.
                // select all (k+1)-mers found for a recordId2 between path2 and path3, that are also annotated for recordId
                        join(
                        SparkComputer.appendIdentifier(edges,"'"),
                        col(recordIdOther).equalTo(col(ColumnIdentifier.RECORD_ID+"'")).
                                and(col(ColumnIdentifier.SRC).equalTo(col(ColumnIdentifier.SRC+"'"))).
                                and(col(ColumnIdentifier.DST).equalTo(col(ColumnIdentifier.DST+"'"))).
                                and(col(strandOther).equalTo(col(ColumnIdentifier.STRAND+"'")))
                ).drop(ColumnIdentifier.RECORD_ID+"'",ColumnIdentifier.SRC+"'",ColumnIdentifier.DST+"'",ColumnIdentifier.STRAND+"'");

        return path;
    }

    private static String counterPart(String column) {
        return column.endsWith("2") ? column.substring(0,column.length()-1) : column +"2";
    }

    /**
     * Given dataset with mapping edges collapse coinciding edges with respect to the reference recordId2 (order given by positionIndex)
     * to a single path represented as first and last corresponding positionindex (positionIndex, positionIndexcon)
     * and first and last positon of the mapping recordId (p1,p2)
     * paths extending over periodic boundaries cannot be collapsed like this -> this is a tradeoff we have to make these will be handled later
     * @param edges
     * @param length Column name of length from mapping genome
     * @param strand Column name of strand from mapping genome
     * @return
     */
    public static Dataset<Row> collapseEdges(Dataset<Row> edges, String length,String strand)
    {
        WindowSpec positionIndexWindow =
                Window.partitionBy(
                                ColumnIdentifier.RECORD_ID+"2",
                                ColumnIdentifier.RECORD_ID,
                                ColumnIdentifier.POSITION+"1",
//                                ColumnIdentifier.POSITION+"2",
//                                ColumnIdentifier.POSITION+"3",
                                ColumnIdentifier.POSITION+"4",
                                ColumnIdentifier.POSITION+"5",
                                ColumnIdentifier.POSITION+"6",
//                                ColumnIdentifier.STRAND,ColumnIdentifier.STRAND+"2",
                                ColumnIdentifier.POSITION_INDEX).
                        orderBy(ColumnIdentifier.POSITION+"T");

        WindowSpec positionIndexWindowUnbounded =
                positionIndexWindow.
                        rowsBetween(Window.unboundedPreceding(),Window.unboundedFollowing());

        WindowSpec neighborCheckWindow =
                Window.partitionBy(
//                                ColumnIdentifier.INDEX,
                                ColumnIdentifier.RECORD_ID+"2",
                                ColumnIdentifier.RECORD_ID,
                                ColumnIdentifier.POSITION+"1",
//                                ColumnIdentifier.POSITION+"2",
//                                ColumnIdentifier.POSITION+"3",
                                ColumnIdentifier.POSITION+"4",
                                ColumnIdentifier.POSITION+"5",
                                ColumnIdentifier.POSITION+"6"
//                                ColumnIdentifier.STRAND,ColumnIdentifier.STRAND+"2"
                        ).
                        orderBy(ColumnIdentifier.POSITION_INDEX);
        WindowSpec rangeWindow =
                Window.partitionBy(
//                                ColumnIdentifier.INDEX,
                                ColumnIdentifier.RECORD_ID+"2",
                                ColumnIdentifier.RECORD_ID,
                                ColumnIdentifier.POSITION+"1",
//                                ColumnIdentifier.POSITION+"2",
//                                ColumnIdentifier.POSITION+"3",
                                ColumnIdentifier.POSITION+"4",
                                ColumnIdentifier.POSITION+"5",
                                ColumnIdentifier.POSITION+"6",
//                                ColumnIdentifier.STRAND,ColumnIdentifier.STRAND+"2",
                                ColumnIdentifier.DIFF_COLUMN_IDENTIFIER,
                                ColumnIdentifier.DIFF_COLUMN_IDENTIFIER+"2",
                                ColumnIdentifier.T1
                        ).
                        orderBy(ColumnIdentifier.POSITION_INDEX).
                        rowsBetween(Window.unboundedPreceding(), Window.unboundedFollowing());
        WindowSpec duplicateRemoveWindow =
                Window.partitionBy(
//                                ColumnIdentifier.INDEX,
                                ColumnIdentifier.RECORD_ID+"2",
                                ColumnIdentifier.RECORD_ID,
                                ColumnIdentifier.POSITION+"1",
//                                ColumnIdentifier.POSITION+"2",
//                                ColumnIdentifier.POSITION+"3",
                                ColumnIdentifier.POSITION+"4",
                                ColumnIdentifier.POSITION+"5",
                                ColumnIdentifier.POSITION+"6",
//                                ColumnIdentifier.STRAND,ColumnIdentifier.STRAND+"2",
                                ColumnIdentifier.END).
                        orderBy(ColumnIdentifier.START);


//        String lengthOther = counterPart(length);
//        String strandOther = counterPart(strand);

        return edges.
                withColumnRenamed(ColumnIdentifier.POSITION+"'",ColumnIdentifier.POSITION+"T").
                withColumn(ColumnIdentifier.POSITION+"T2",
                        ColumnFunctions.getRightNeighbour(ColumnIdentifier.POSITION+"T",length,strand)).
                withColumn(ColumnIdentifier.POSITIONS,collect_list(col(ColumnIdentifier.POSITION+"T")).over(positionIndexWindowUnbounded)).
                withColumn(ColumnIdentifier.POSITIONS+"2",collect_list(col(ColumnIdentifier.POSITION+"T2")).over(positionIndexWindowUnbounded)).
                withColumn(ColumnIdentifier.RANK, dense_rank().over(positionIndexWindow)).filter(col(ColumnIdentifier.RANK).equalTo(1)).
                drop(ColumnIdentifier.RANK).
                withColumn(ColumnIdentifier.NEXT,lead(col(ColumnIdentifier.POSITIONS),1).over(neighborCheckWindow)).
                withColumn(ColumnIdentifier.MATCH_FLAG,when((col(ColumnIdentifier.NEXT).equalTo(col(ColumnIdentifier.POSITIONS2))).isNull(),
                        false).otherwise(col(ColumnIdentifier.NEXT).equalTo(col(ColumnIdentifier.POSITIONS2)))).
                withColumn(ColumnIdentifier.MATCH_FLAG+"2",lag(col(ColumnIdentifier.MATCH_FLAG),1,false).over(neighborCheckWindow)).
                filter(not(col(ColumnIdentifier.MATCH_FLAG).equalTo(false).and(col(ColumnIdentifier.MATCH_FLAG+"2").equalTo(true)))).

                withColumn(ColumnIdentifier.T1,col(ColumnIdentifier.MATCH_FLAG).equalTo(true).or(col(ColumnIdentifier.MATCH_FLAG+"2").equalTo(true))).
                withColumn(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER,
                        col(ColumnIdentifier.POSITION_INDEX).minus(row_number().over(neighborCheckWindow))).
                withColumn(ColumnIdentifier.DIFF_COLUMN_IDENTIFIER+"2",
                        element_at(col(ColumnIdentifier.POSITIONS),1).minus(row_number().over(neighborCheckWindow))).

                withColumn(ColumnIdentifier.START,when(col(ColumnIdentifier.MATCH_FLAG).equalTo(true),
                        first(col(ColumnIdentifier.POSITION_INDEX)).over(rangeWindow)).otherwise(col(ColumnIdentifier.POSITION_INDEX))).
                withColumn(ColumnIdentifier.END,when(col(ColumnIdentifier.MATCH_FLAG).equalTo(true),
                        last(col(ColumnIdentifier.POSITION_INDEX)).over(rangeWindow).plus(1)).otherwise(col(ColumnIdentifier.POSITION_INDEX))).

                withColumn(ColumnIdentifier.POSITIONS+"1",when(col(ColumnIdentifier.MATCH_FLAG).equalTo(true),
                        first(col(ColumnIdentifier.POSITIONS)).over(rangeWindow)).otherwise(col(ColumnIdentifier.POSITIONS))).
                withColumn(ColumnIdentifier.POSITIONS+"2",when(col(ColumnIdentifier.MATCH_FLAG).equalTo(true),
                        last(col(ColumnIdentifier.POSITIONS+"2")).over(rangeWindow)).otherwise(col(ColumnIdentifier.POSITIONS))).
                select(ColumnIdentifier.INDEX,
                        ColumnIdentifier.POSITION+"1",
                        ColumnIdentifier.POSITION+"2",
                        ColumnIdentifier.POSITION+"3",
                        ColumnIdentifier.POSITION+"4",
                        ColumnIdentifier.POSITION+"5",
                        ColumnIdentifier.POSITION+"6",
                        ColumnIdentifier.POSITION_DIFF,
                        ColumnIdentifier.RECORD_ID+"2",
                        ColumnIdentifier.RECORD_ID,
                        ColumnIdentifier.LENGTH,ColumnIdentifier.LENGTH+"2",
                        ColumnIdentifier.STRAND,ColumnIdentifier.STRAND+"2",
                        ColumnIdentifier.TOPOLOGY,ColumnIdentifier.TOPOLOGY+"2",
                        ColumnIdentifier.START,ColumnIdentifier.END,
                        ColumnIdentifier.POSITIONS+"1",ColumnIdentifier.POSITIONS2
                ).distinct().
                withColumn(ColumnIdentifier.RANK, dense_rank().over(duplicateRemoveWindow)).filter(col(ColumnIdentifier.RANK).equalTo(1)).
                drop(ColumnIdentifier.RANK).
                withColumn(ColumnIdentifier.RANGE_LENGTH,ColumnFunctions.getLinearRangeLength(col(ColumnIdentifier.START),col(ColumnIdentifier.END))).
                select(ColumnIdentifier.INDEX,
                        ColumnIdentifier.POSITION+"1",
                        ColumnIdentifier.POSITION+"2",
                        ColumnIdentifier.POSITION+"3",
                        ColumnIdentifier.POSITION+"4",
                        ColumnIdentifier.POSITION+"5",
                        ColumnIdentifier.POSITION+"6",
                        ColumnIdentifier.POSITION_DIFF,
                        ColumnIdentifier.RECORD_ID+"2",
                        ColumnIdentifier.RECORD_ID,
                        ColumnIdentifier.LENGTH,ColumnIdentifier.LENGTH+"2",
                        ColumnIdentifier.STRAND,ColumnIdentifier.STRAND+"2",
                        ColumnIdentifier.TOPOLOGY,ColumnIdentifier.TOPOLOGY+"2",
                        ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.RANGE_LENGTH,
                        ColumnIdentifier.POSITIONS+"1",ColumnIdentifier.POSITIONS2);
    }

    /**
     *
     * @param paths
     * @param distanceThreshold
     * @param validPath
     * @param minPathLength
     * @param length Column name of length from mapping genome
     * @param strand Column name of length from mapping genome
     */
    public static void extractValidPaths(Dataset<Row> paths, int distanceThreshold, String validPath, int minPathLength, String length,String strand) {
        int checkPointInterval = 1;
        String positionIdentifier = "p";
        String con1 = "con";
        String con2 = "con2";
        String candidtateIdentifier = "candidate";
        String pathIdentifier = "path";
        String neighbor = "n";
        String neighbor2 = "n2";
        String candidatesPath = validPath+"/candidates";
        Dataset<Row> validPaths;
        LOGGER.debug("Extracting valid paths");
//        WindowSpec w = Window.
//                partitionBy(ColumnIdentifier.INDEX,
//                        ColumnIdentifier.RECORD_ID+"2",
//                        ColumnIdentifier.RECORD_ID,
//                        ColumnIdentifier.POSITION+"1",
//                        ColumnIdentifier.POSITION+"2",
//                        ColumnIdentifier.POSITION+"3",
//                        ColumnIdentifier.POSITION+"4").orderBy(ColumnIdentifier.START);

        paths = paths.
                withColumn("m",map_from_arrays(col(ColumnIdentifier.POSITIONS+"1"),col(ColumnIdentifier.POSITIONS+"2"))).
                select(col("*"),explode(col("m")).as(new String[]{ColumnIdentifier.P1,ColumnIdentifier.P2})).
                drop("m").
                withColumnRenamed(ColumnIdentifier.START,ColumnIdentifier.POSITION_INDEX).
                withColumnRenamed(ColumnIdentifier.END,ColumnIdentifier.POSITION_INDEX+con1);
        paths = Spark.checkPoint(paths,"p1");


        // create adjacence dataset -> connect edges e1, e2 if positionindex(e2) - positionindex(e1) < distanceThreshold (with resepect to recordId2) and
        // p1(e2)- p1(e1) < distancethreshold (with respect to recordId)
        paths =  paths.
                join(SparkComputer.appendIdentifier(
                                paths, neighbor),
                        col(ColumnIdentifier.INDEX).equalTo(col(ColumnIdentifier.INDEX+neighbor)).
//                                and(col(ColumnIdentifier.RECORD_ID+"2").equalTo(col(ColumnIdentifier.RECORD_ID+"2"+neighbor))).
//                                and(col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+neighbor))).
//                                and(col(ColumnIdentifier.POSITION+"1").equalTo(col(ColumnIdentifier.POSITION+"1"+neighbor))).
//                                and(col(ColumnIdentifier.POSITION+"2").equalTo(col(ColumnIdentifier.POSITION+"2"+neighbor))).
//                                and(col(ColumnIdentifier.POSITION+"3").equalTo(col(ColumnIdentifier.POSITION+"3"+neighbor))).
//                                and(col(ColumnIdentifier.POSITION+"4").equalTo(col(ColumnIdentifier.POSITION+"4"+neighbor))).
//                                and(col(strand).equalTo(col(strand+neighbor))).
        and((col(ColumnIdentifier.POSITION_INDEX+neighbor).minus(col(ColumnIdentifier.POSITION_INDEX+con1)).minus(1)).between(0,distanceThreshold)).
                                and(ColumnFunctions.computeDistanceExcludingBoundaryPositions(
                                        ColumnIdentifier.P2,
                                        ColumnIdentifier.P1+neighbor,
                                        length,
                                        ColumnIdentifier.TOPOLOGY,
                                        strand).between(0,distanceThreshold))).
                withColumn(ColumnIdentifier.RANGE_LENGTH,
                        ColumnFunctions.getLinearRangeLength(ColumnIdentifier.POSITION_INDEX,ColumnIdentifier.POSITION_INDEX+con1+neighbor)).
                select(
                        ColumnIdentifier.INDEX,
                        ColumnIdentifier.RECORD_ID,
                        ColumnIdentifier.RECORD_ID+"2",
                        ColumnIdentifier.POSITION+"1",
                        ColumnIdentifier.POSITION+"2",
                        ColumnIdentifier.POSITION+"3",
                        ColumnIdentifier.POSITION+"4",
                        ColumnIdentifier.POSITION+"5",
                        ColumnIdentifier.POSITION+"6",
                        ColumnIdentifier.STRAND,
                        ColumnIdentifier.STRAND+"2",
                        ColumnIdentifier.TOPOLOGY,
                        ColumnIdentifier.TOPOLOGY+"2",
                        ColumnIdentifier.LENGTH,
                        ColumnIdentifier.LENGTH+"2",
                        ColumnIdentifier.POSITION_DIFF,
                        ColumnIdentifier.POSITION_INDEX,
                        ColumnIdentifier.POSITION_INDEX+con1,
                        ColumnIdentifier.RANGE_LENGTH,
                        ColumnIdentifier.P1,
                        ColumnIdentifier.P2,
                        ColumnIdentifier.POSITION_INDEX+neighbor,
                        ColumnIdentifier.POSITION_INDEX+con1+neighbor,
                        ColumnIdentifier.P1+neighbor,
                        ColumnIdentifier.P2+neighbor);

        paths = Spark.checkPoint(paths,"p3");

        // persist singleton paths of sufficient length
        Dataset<Row> p1 = SparkComputer.read(Spark.SPARK_CHECKPOINT_DIR+"/p1");
        SparkComputer.persistDataFrameORC(p1.
                        select(ColumnIdentifier.INDEX,
                                ColumnIdentifier.RECORD_ID,
                                ColumnIdentifier.RECORD_ID+"2",
                                ColumnIdentifier.POSITION+"1",
                                ColumnIdentifier.POSITION+"2",
                                ColumnIdentifier.POSITION+"3",
                                ColumnIdentifier.POSITION+"4",
                                ColumnIdentifier.POSITION+"5",
                                ColumnIdentifier.POSITION+"6",
                                ColumnIdentifier.STRAND,
                                ColumnIdentifier.STRAND+"2",
                                ColumnIdentifier.TOPOLOGY,
                                ColumnIdentifier.TOPOLOGY+"2",
                                ColumnIdentifier.LENGTH,
                                ColumnIdentifier.LENGTH+"2",
                                ColumnIdentifier.POSITION_DIFF,
                                ColumnIdentifier.POSITION_INDEX,
                                ColumnIdentifier.POSITION_INDEX+con1,
                                ColumnIdentifier.P1,
                                ColumnIdentifier.P2).
                        filter(ColumnFunctions.getLinearRangeLength(ColumnIdentifier.POSITION_INDEX,ColumnIdentifier.POSITION_INDEX+con1).geq(minPathLength)).
                        except(
                                paths.select(ColumnIdentifier.INDEX,
                                                ColumnIdentifier.RECORD_ID,
                                                ColumnIdentifier.RECORD_ID+"2",
                                                ColumnIdentifier.POSITION+"1",
                                                ColumnIdentifier.POSITION+"2",
                                                ColumnIdentifier.POSITION+"3",
                                                ColumnIdentifier.POSITION+"4",
                                                ColumnIdentifier.POSITION+"5",
                                                ColumnIdentifier.POSITION+"6",
                                                ColumnIdentifier.STRAND,
                                                ColumnIdentifier.STRAND+"2",
                                                ColumnIdentifier.TOPOLOGY,
                                                ColumnIdentifier.TOPOLOGY+"2",
                                                ColumnIdentifier.LENGTH,
                                                ColumnIdentifier.LENGTH+"2",
                                                ColumnIdentifier.POSITION_DIFF,
                                                ColumnIdentifier.POSITION_INDEX,
                                                ColumnIdentifier.POSITION_INDEX+con1,
                                                ColumnIdentifier.P1,
                                                ColumnIdentifier.P2).distinct().
                                        union(paths.
                                                select(ColumnIdentifier.INDEX,
                                                        ColumnIdentifier.RECORD_ID,
                                                        ColumnIdentifier.RECORD_ID+"2",
                                                        ColumnIdentifier.POSITION+"1",
                                                        ColumnIdentifier.POSITION+"2",
                                                        ColumnIdentifier.POSITION+"3",
                                                        ColumnIdentifier.POSITION+"4",
                                                        ColumnIdentifier.POSITION+"5",
                                                        ColumnIdentifier.POSITION+"6",
                                                        ColumnIdentifier.STRAND,
                                                        ColumnIdentifier.STRAND+"2",
                                                        ColumnIdentifier.TOPOLOGY,
                                                        ColumnIdentifier.TOPOLOGY+"2",
                                                        ColumnIdentifier.LENGTH,
                                                        ColumnIdentifier.LENGTH+"2",
                                                        ColumnIdentifier.POSITION_DIFF,

                                                        ColumnIdentifier.POSITION_INDEX+neighbor,
                                                        ColumnIdentifier.POSITION_INDEX+con1+neighbor,
                                                        ColumnIdentifier.P1+neighbor,
                                                        ColumnIdentifier.P2+neighbor).
                                                distinct().
                                                withColumnRenamed(ColumnIdentifier.POSITION_INDEX+neighbor,ColumnIdentifier.POSITION_INDEX).
                                                withColumnRenamed(ColumnIdentifier.POSITION_INDEX+con1+neighbor,ColumnIdentifier.POSITION_INDEX+con1).
                                                withColumnRenamed(ColumnIdentifier.P1+neighbor,ColumnIdentifier.P1).
                                                withColumnRenamed(ColumnIdentifier.P2+neighbor,ColumnIdentifier.P2)
                                        )
                        )
                        .withColumn(ColumnIdentifier.DISTANCE,ColumnFunctions.getLinearRangeLength(ColumnIdentifier.POSITION_INDEX,ColumnIdentifier.POSITION_INDEX+con1))
                ,candidatesPath);






        // of the remaining paths select startPath -> no predecessor vertex
        Dataset<Row> startPath = paths.
                join(SparkComputer.appendIdentifier(paths,con2),
                        col(ColumnIdentifier.INDEX).equalTo(col(ColumnIdentifier.INDEX+con2)).

                                        and(col(ColumnIdentifier.POSITION_INDEX).equalTo(col(ColumnIdentifier.POSITION_INDEX+neighbor+con2))).
                                and(col(ColumnIdentifier.POSITION_INDEX+con1).equalTo(col(ColumnIdentifier.POSITION_INDEX+ con1+neighbor+con2))).
                                and(col(ColumnIdentifier.P1).equalTo(col(ColumnIdentifier.P1+neighbor+con2))).
                                and(col(ColumnIdentifier.P2).equalTo(col(ColumnIdentifier.P2+neighbor+con2)))
                        ,
                        SparkComputer.JOIN_TYPES.LEFT_ANTI).
                select(
                        ColumnIdentifier.INDEX,
                        ColumnIdentifier.RECORD_ID,
                        ColumnIdentifier.RECORD_ID+"2",
                        ColumnIdentifier.POSITION+"1",
                        ColumnIdentifier.POSITION+"2",
                        ColumnIdentifier.POSITION+"3",
                        ColumnIdentifier.POSITION+"4",
                        ColumnIdentifier.POSITION+"5",
                        ColumnIdentifier.POSITION+"6",
                        ColumnIdentifier.STRAND,
                        ColumnIdentifier.STRAND+"2",
                        ColumnIdentifier.TOPOLOGY,
                        ColumnIdentifier.TOPOLOGY+"2",
                        ColumnIdentifier.LENGTH,
                        ColumnIdentifier.LENGTH+"2",
                        ColumnIdentifier.POSITION_DIFF,
                        ColumnIdentifier.POSITION_INDEX,ColumnIdentifier.POSITION_INDEX+con1,
                        ColumnIdentifier.POSITION_INDEX+neighbor,ColumnIdentifier.POSITION_INDEX+con1+neighbor,
                        ColumnIdentifier.P1,ColumnIdentifier.P2,ColumnIdentifier.P1+neighbor,ColumnIdentifier.P2+neighbor).persist();



        int iteration = 0;


        // for all remaining paths we need to check if we can merge them to a single spanning path
        while (true) {
            // remove all paths that have been visited before
            paths = paths.join(SparkComputer.appendIdentifier(startPath,pathIdentifier),
                    col(ColumnIdentifier.INDEX).equalTo(col(ColumnIdentifier.INDEX+pathIdentifier)).
//                            and(col(ColumnIdentifier.RECORD_ID+"2").equalTo(col(ColumnIdentifier.RECORD_ID+"2"+pathIdentifier))).
//                            and(col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+pathIdentifier))).
//                            and(col(ColumnIdentifier.POSITION+"1").equalTo(col(ColumnIdentifier.POSITION+"1"+pathIdentifier))).
//                            and(col(ColumnIdentifier.POSITION+"2").equalTo(col(ColumnIdentifier.POSITION+"2"+pathIdentifier))).
//                            and(col(ColumnIdentifier.POSITION+"3").equalTo(col(ColumnIdentifier.POSITION+"3"+pathIdentifier))).
//                            and(col(ColumnIdentifier.POSITION+"4").equalTo(col(ColumnIdentifier.POSITION+"4"+pathIdentifier))).
        and(col(ColumnIdentifier.P2+neighbor).equalTo(col(ColumnIdentifier.P2+neighbor+pathIdentifier))).
                            and(col(ColumnIdentifier.POSITION_INDEX+con1+neighbor).equalTo(col(ColumnIdentifier.POSITION_INDEX+ con1+neighbor+pathIdentifier)))
//                            and(col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+pathIdentifier))).
//                            and(col(ColumnIdentifier.STRAND+"2").equalTo(col(ColumnIdentifier.STRAND+"2"+pathIdentifier)))
                    ,
                    SparkComputer.JOIN_TYPES.LEFT_ANTI);



            startPath = startPath.join(SparkComputer.appendIdentifier(paths,pathIdentifier),
                    col(ColumnIdentifier.INDEX).equalTo(col(ColumnIdentifier.INDEX+pathIdentifier)).
//                            and(col(ColumnIdentifier.RECORD_ID+"2").equalTo(col(ColumnIdentifier.RECORD_ID+"2"+pathIdentifier))).
//                            and(col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+pathIdentifier))).
//                            and(col(ColumnIdentifier.POSITION+"1").equalTo(col(ColumnIdentifier.POSITION+"1"+pathIdentifier))).
//                            and(col(ColumnIdentifier.POSITION+"2").equalTo(col(ColumnIdentifier.POSITION+"2"+pathIdentifier))).
//                            and(col(ColumnIdentifier.POSITION+"3").equalTo(col(ColumnIdentifier.POSITION+"3"+pathIdentifier))).
//                            and(col(ColumnIdentifier.POSITION+"4").equalTo(col(ColumnIdentifier.POSITION+"4"+pathIdentifier))).
        and(col(ColumnIdentifier.P2+neighbor).equalTo(col(ColumnIdentifier.P2+pathIdentifier))).
                            and(col(ColumnIdentifier.P1+neighbor).equalTo(col(ColumnIdentifier.P1+pathIdentifier))).
                            and(col(ColumnIdentifier.POSITION_INDEX+neighbor).equalTo(col(ColumnIdentifier.POSITION_INDEX+pathIdentifier))).
                            and(col(ColumnIdentifier.POSITION_INDEX+con1+neighbor).equalTo(col(ColumnIdentifier.POSITION_INDEX+ con1+pathIdentifier)))
//                            and(col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+pathIdentifier))).
//                            and(col(ColumnIdentifier.STRAND+"2").equalTo(col(ColumnIdentifier.STRAND+"2"+pathIdentifier)))
                    ,
                    SparkComputer.JOIN_TYPES.LEFT).
//                    withColumn(ColumnIdentifier.P1+neighbor,col(ColumnIdentifier.P1+neighbor+pathIdentifier)).
//                    withColumn(ColumnIdentifier.P2+neighbor,col(ColumnIdentifier.P2+neighbor+pathIdentifier)).
//                    withColumn(ColumnIdentifier.POSITION_INDEX+neighbor,col(ColumnIdentifier.POSITION_INDEX+neighbor+pathIdentifier)).
//                    withColumn(ColumnIdentifier.POSITION_INDEX+con1+neighbor,col(ColumnIdentifier.POSITION_INDEX+con1+neighbor+pathIdentifier)).
//                    withColumn(ColumnIdentifier.PATH,concat(col(ColumnIdentifier.PATH),array(col(ColumnIdentifier.P2)))).
        select(
        ColumnIdentifier.INDEX,
        ColumnIdentifier.RECORD_ID,
        ColumnIdentifier.RECORD_ID+"2",
        ColumnIdentifier.POSITION+"1",
        ColumnIdentifier.POSITION+"2",
        ColumnIdentifier.POSITION+"3",
        ColumnIdentifier.POSITION+"4",
        ColumnIdentifier.POSITION+"5",
        ColumnIdentifier.POSITION+"6",
        ColumnIdentifier.STRAND,
        ColumnIdentifier.STRAND+"2",
        ColumnIdentifier.TOPOLOGY,
        ColumnIdentifier.TOPOLOGY+"2",
        ColumnIdentifier.LENGTH,
        ColumnIdentifier.LENGTH+"2",
        ColumnIdentifier.POSITION_DIFF,
        ColumnIdentifier.POSITION_INDEX,ColumnIdentifier.POSITION_INDEX+con1,
        ColumnIdentifier.POSITION_INDEX+neighbor,ColumnIdentifier.POSITION_INDEX+con1+neighbor,
        ColumnIdentifier.POSITION_INDEX+neighbor+pathIdentifier,
        ColumnIdentifier.POSITION_INDEX+con1+neighbor+pathIdentifier,
        ColumnIdentifier.P1,ColumnIdentifier.P2,
        ColumnIdentifier.P1+neighbor,ColumnIdentifier.P2+neighbor,
        ColumnIdentifier.P1+neighbor+pathIdentifier,
        ColumnIdentifier.P2+neighbor+pathIdentifier).distinct().persist();


            SparkComputer.persistDataFrameORCAppend(startPath.filter(isnull(col("p1"+neighbor+pathIdentifier))).
                            withColumn(ColumnIdentifier.P2,col(ColumnIdentifier.P2+neighbor)).
                            withColumn(ColumnIdentifier.POSITION_INDEX+con1,col(ColumnIdentifier.POSITION_INDEX+con1+neighbor)).
                            withColumn(ColumnIdentifier.DISTANCE,ColumnFunctions.getLinearRangeLength(ColumnIdentifier.POSITION_INDEX,ColumnIdentifier.POSITION_INDEX+con1)).
                            filter(col(ColumnIdentifier.DISTANCE).geq(minPathLength)).
                            select(ColumnIdentifier.INDEX,
                                    ColumnIdentifier.RECORD_ID,
                                    ColumnIdentifier.RECORD_ID+"2",
                                    ColumnIdentifier.POSITION+"1",
                                    ColumnIdentifier.POSITION+"2",
                                    ColumnIdentifier.POSITION+"3",
                                    ColumnIdentifier.POSITION+"4",
                                    ColumnIdentifier.POSITION+"5",
                                    ColumnIdentifier.POSITION+"6",
                                    ColumnIdentifier.STRAND,
                                    ColumnIdentifier.STRAND+"2",
                                    ColumnIdentifier.TOPOLOGY,
                                    ColumnIdentifier.TOPOLOGY+"2",
                                    ColumnIdentifier.LENGTH,
                                    ColumnIdentifier.LENGTH+"2",
                                    ColumnIdentifier.POSITION_DIFF,

                                    ColumnIdentifier.POSITION_INDEX,
                                    ColumnIdentifier.POSITION_INDEX+con1,
                                    ColumnIdentifier.P1,
                                    ColumnIdentifier.P2,ColumnIdentifier.DISTANCE).
                            distinct()
                    ,candidatesPath);


            startPath = startPath.filter(not(isnull(col("p1"+neighbor+pathIdentifier)))).
                    withColumn(ColumnIdentifier.P1+neighbor,col(ColumnIdentifier.P1+neighbor+pathIdentifier)).
                    withColumn(ColumnIdentifier.P2+neighbor,col(ColumnIdentifier.P2+neighbor+pathIdentifier)).
                    withColumn(ColumnIdentifier.POSITION_INDEX+neighbor,col(ColumnIdentifier.POSITION_INDEX+neighbor+pathIdentifier)).
                    withColumn(ColumnIdentifier.POSITION_INDEX+con1+neighbor,col(ColumnIdentifier.POSITION_INDEX+con1+neighbor+pathIdentifier)).
                    drop(ColumnIdentifier.POSITION_INDEX+neighbor+pathIdentifier,
                            ColumnIdentifier.POSITION_INDEX+con1+neighbor+pathIdentifier,
                            ColumnIdentifier.P1+neighbor+pathIdentifier,
                            ColumnIdentifier.P2+neighbor+pathIdentifier).persist();


            if(startPath.isEmpty()){
                break;
            }


            if(iteration % checkPointInterval == 0) {

                startPath = Spark.checkPoint2(startPath,iteration ,checkPointInterval,"BP/start");
                paths = Spark.checkPoint(paths,iteration,checkPointInterval,"/BP/paths");
                System.gc();
            }
            iteration++;

        }
        Spark.clearDirectory(Spark.SPARK_CHECKPOINT_DIR+"/BP");
        Dataset<Row> candidates = SparkComputer.readORC(candidatesPath);
        candidates = candidates.
        withColumn(ColumnIdentifier.RANK,dense_rank().over(Window.partitionBy(ColumnIdentifier.INDEX,ColumnIdentifier.POSITION_INDEX).orderBy(desc(ColumnIdentifier.POSITION_INDEX+con1)))).
                filter(col(ColumnIdentifier.RANK).equalTo(1)).drop(ColumnIdentifier.RANK).
                distinct();
        SparkComputer.persistDataFrameORCAppend(candidates,validPath);
        Spark.clearCheckPointDir(Spark.SPARK_CHECKPOINT_DIR+"/p1");
        Spark.clearCheckPointDir(Spark.SPARK_CHECKPOINT_DIR+"/p3");

    }


    public static void evaluateUpperBranch(Dataset<Row> path, Dataset<Row> edges, Dataset<Row> sequences, String breakPointPath, int k,
                                       int searchDistance, int distanceThreshold, int minPathLength, double eValueThreshold,int minIdentical) {


    Dataset<Row> pathBoth=
            Spark.checkPoint(path.
                    filter(col(ColumnIdentifier.DISTANCE+"R2").leq(2*searchDistance)).
                    withColumn(ColumnIdentifier.RANGE,ColumnFunctions.computeIntermediatePositions(
                            ColumnIdentifier.POSITION+"1",ColumnIdentifier.POSITION+"4",ColumnIdentifier.LENGTH+"2",
                            ColumnIdentifier.TOPOLOGY,ColumnIdentifier.STRAND+"2",null)),"pBoth");

    SparkComputer.persistDataFrameORC(collapseEdges(
            computeMappingKmers(pathBoth,edges,ColumnIdentifier.RECORD_ID+"2",ColumnIdentifier.LENGTH+"2",
                    ColumnIdentifier.STRAND+"2",ColumnIdentifier.TOPOLOGY+"2",
                    ColumnIdentifier.INDEX),
            ColumnIdentifier.LENGTH,ColumnIdentifier.STRAND),breakPointPath+"/collapsedPaths23Both");
    Spark.clearDirectory(Spark.SPARK_CHECKPOINT_DIR+"/pBoth");
    Spark.clearDirectory(Spark.SPARK_CHECKPOINT_DIR+"/path");
    extractValidPaths(SparkComputer.readORC(breakPointPath+"/collapsedPaths23Both"),
            distanceThreshold,breakPointPath+"/validPaths23Both",minPathLength,ColumnIdentifier.LENGTH,ColumnIdentifier.STRAND);



    path = path.
            filter(col(ColumnIdentifier.DISTANCE+"R2").gt(2*searchDistance)).persist();

    Dataset<Row> pathLeft =    Spark.checkPoint(path.
            withColumn(ColumnIdentifier.P2,
                    pmod(col(ColumnIdentifier.POSITION+"2").plus(searchDistance),col(ColumnIdentifier.LENGTH+"2"))).
            withColumn(ColumnIdentifier.RANGE,ColumnFunctions.computeIntermediatePositions(
                    ColumnIdentifier.POSITION+"1",ColumnIdentifier.P2,ColumnIdentifier.LENGTH+"2",
                    ColumnIdentifier.TOPOLOGY,ColumnIdentifier.STRAND+"2",null)).
            drop(ColumnIdentifier.P2),"pLeft");
    SparkComputer.persistDataFrameORC(collapseEdges(computeMappingKmers(pathLeft,edges,ColumnIdentifier.RECORD_ID+"2",ColumnIdentifier.LENGTH+"2",
                    ColumnIdentifier.STRAND+"2",ColumnIdentifier.TOPOLOGY+"2",
                    ColumnIdentifier.INDEX),
            ColumnIdentifier.LENGTH,ColumnIdentifier.STRAND),breakPointPath+"/collapsedPaths23Left");
    Spark.clearDirectory(Spark.SPARK_CHECKPOINT_DIR+"/pLeft");
    Spark.clearDirectory(Spark.SPARK_CHECKPOINT_DIR+"/path");
    extractValidPaths(SparkComputer.readORC(breakPointPath+"/collapsedPaths23Left"),
            distanceThreshold,breakPointPath+"/validPaths23Left",minPathLength,ColumnIdentifier.LENGTH,ColumnIdentifier.STRAND);


    Dataset<Row> pathRight =    Spark.checkPoint(path.
            withColumn(ColumnIdentifier.RANGE,
                    ColumnFunctions.computeIntermediatePositions(
                            pmod(col(ColumnIdentifier.POSITION+"3").minus(searchDistance),col(ColumnIdentifier.LENGTH+"2")),
                            col(ColumnIdentifier.POSITION+"4"),col(ColumnIdentifier.LENGTH+"2"),
                            col(ColumnIdentifier.TOPOLOGY),col(ColumnIdentifier.STRAND+"2"),null)),"pRight");
    SparkComputer.persistDataFrameORC(collapseEdges(computeMappingKmers(pathRight,edges,ColumnIdentifier.RECORD_ID+"2",ColumnIdentifier.LENGTH+"2",
                    ColumnIdentifier.STRAND+"2",ColumnIdentifier.TOPOLOGY+"2",
                    ColumnIdentifier.INDEX),
            ColumnIdentifier.LENGTH,ColumnIdentifier.STRAND),breakPointPath+"/collapsedPaths23Right");
    Spark.clearDirectory(Spark.SPARK_CHECKPOINT_DIR+"/pRight");
    Spark.clearDirectory(Spark.SPARK_CHECKPOINT_DIR+"/path");
    extractValidPaths(SparkComputer.readORC(breakPointPath+"/collapsedPaths23Right"),
            distanceThreshold,breakPointPath+"/validPaths23Right",minPathLength,ColumnIdentifier.LENGTH,ColumnIdentifier.STRAND);

//    SparkComputer.persistDataFrameORC(    SparkComputer.readORC(breakPointPath+"/validPaths23Left").groupBy(length(col(ColumnIdentifier.RANGE))).count().union(pathRight.groupBy(length(col(ColumnIdentifier.RANGE))).count())
//            .union(pathBoth.groupBy(length(col(ColumnIdentifier.RANGE))).count()),breakPointPath+"/alignmentlengths");


    String directory = breakPointPath+"/SCORES_";
    SparkComputer.persistDataFrameORC(
            ColumnFunctions.computeOverlap(
                    compareSequenceSimilarityAlongPathNoShift(SparkComputer.readORC(breakPointPath+"/validPaths23Both"),
                            sequences,k,
                            ColumnIdentifier.RECORD_ID+"2",ColumnIdentifier.LENGTH+"2", ColumnIdentifier.POSITION+"1")
                            // only retain alignments of sufficient quality
                            .filter(col(ColumnIdentifier.ALIGN_RESULT+"."+ColumnIdentifier.EVALUE).leq(eValueThreshold).
                            and(col(ColumnIdentifier.ALIGN_RESULT+"."+ColumnIdentifier.IDENTICAL).geq(minIdentical)))
                    ,
                    col("pQ1"),col("pQ2"),
                    col(ColumnIdentifier.POSITION+"5"),
                    col(ColumnIdentifier.POSITION+"6"),
                    col(ColumnIdentifier.LENGTH),ColumnIdentifier.OVERLAP),
            directory+"BOTH");
    SparkComputer.persistDataFrameORC(
            ColumnFunctions.computeOverlap(
                    compareSequenceSimilarityAlongPathNoShift(SparkComputer.readORC(breakPointPath+"/validPaths23Left"),
                            sequences,k,
                            ColumnIdentifier.RECORD_ID+"2",ColumnIdentifier.LENGTH+"2", ColumnIdentifier.POSITION+"1")

                            // only retain alignments of sufficient quality
                            .filter(col(ColumnIdentifier.ALIGN_RESULT+"."+ColumnIdentifier.EVALUE).leq(eValueThreshold).
                                    and(col(ColumnIdentifier.ALIGN_RESULT+"."+ColumnIdentifier.IDENTICAL).geq(minIdentical))),
                    col("pQ1"),col("pQ2"),
                    col(ColumnIdentifier.POSITION+"5"),
                    col(ColumnIdentifier.POSITION+"6"),
                    col(ColumnIdentifier.LENGTH),ColumnIdentifier.OVERLAP),
            directory+"LEFT");
    SparkComputer.persistDataFrameORC(
            ColumnFunctions.computeOverlap(
                    compareSequenceSimilarityAlongPathNoShift(SparkComputer.readORC(breakPointPath+"/validPaths23Right").
                                    withColumn("pmid",pmod(col(ColumnIdentifier.POSITION+"3").minus(searchDistance),col(ColumnIdentifier.LENGTH+"2"))),
                            sequences,k,
                            ColumnIdentifier.RECORD_ID+"2",ColumnIdentifier.LENGTH+"2", "pmid")
                            // only retain alignments of sufficient quality
                            .filter(col(ColumnIdentifier.ALIGN_RESULT+"."+ColumnIdentifier.EVALUE).leq(eValueThreshold).
                                    and(col(ColumnIdentifier.ALIGN_RESULT+"."+ColumnIdentifier.IDENTICAL).geq(minIdentical)))
                    ,
                    col("pQ1"),col("pQ2"),
                    col(ColumnIdentifier.POSITION+"5"),
                    col(ColumnIdentifier.POSITION+"6"),
                    col(ColumnIdentifier.LENGTH),ColumnIdentifier.OVERLAP),
            directory+"RIGHT");

    SparkComputer.persistDataFrameORC(computeCombinedScores(breakPointPath,minIdentical), directory+"JOINED");


}


    public static Dataset<Row> computeCombinedScores(String breakPointPath, int minIdentical) {

//        Dataset<Row> scores_both = filterScores(SparkComputer.readORC(breakPointPath+"/SCORES_BOTH"),eValueThreshold,minIdentical);
//
//        Dataset<Row> scores_left = filterScores(SparkComputer.readORC(breakPointPath+"/SCORES_LEFT"),eValueThreshold,minIdentical);
//
//        Dataset<Row> scores_right = filterScores(SparkComputer.readORC(breakPointPath+"/SCORES_RIGHT"),eValueThreshold,minIdentical);

        Dataset<Row> scores_both = SparkComputer.readORC(breakPointPath+"/SCORES_BOTH");

        Dataset<Row> scores_left = SparkComputer.readORC(breakPointPath+"/SCORES_LEFT");

        Dataset<Row> scores_right = SparkComputer.readORC(breakPointPath+"/SCORES_RIGHT");

        // determine which alignments on top branch are of sufficient quality AND overlap with the bottom branch-> these must be removed
        Dataset<Row> branchMatches = scores_both.filter((col(ColumnIdentifier.OVERLAP).geq(0.1))).select(ColumnIdentifier.INDEX).distinct().
                union(scores_right.filter((col(ColumnIdentifier.OVERLAP).geq(0.1))).select(ColumnIdentifier.INDEX).distinct()).
                union(scores_left.filter((col(ColumnIdentifier.OVERLAP).geq(0.1))).select(ColumnIdentifier.INDEX).distinct()).distinct().
                withColumnRenamed(ColumnIdentifier.INDEX,"i").persist();


        scores_both = computeFlankOverlaps(scores_both.join(branchMatches,col(ColumnIdentifier.INDEX).equalTo(col("i")),SparkComputer.JOIN_TYPES.LEFT_ANTI),20);

        scores_left = computeFlankOverlaps(scores_left.join(branchMatches,col(ColumnIdentifier.INDEX).equalTo(col("i")),SparkComputer.JOIN_TYPES.LEFT_ANTI),20);

        scores_right = computeFlankOverlaps(scores_right.join(branchMatches,col(ColumnIdentifier.INDEX).equalTo(col("i")),SparkComputer.JOIN_TYPES.LEFT_ANTI),20);

        // remove all branches with significant score for upper branch that map to more than 15% to bottom branch -> these are no random matches
        // of the remaining branches only keep sufficiently high scores
        double threshold = 0.15;
        branchMatches = scores_both.filter(((col("oLR1Left").geq(threshold)).or(col("oLR1Right").geq(threshold))))
                .select(ColumnIdentifier.INDEX).distinct().
                union(scores_right.filter(((col("oLR1Left").geq(threshold)).or(col("oLR1Right").geq(threshold))))
                        .select(ColumnIdentifier.INDEX).distinct()).
                union(scores_left.filter(((col("oLR1Left").geq(threshold)).or(col("oLR1Right").geq(threshold))))
                        .select(ColumnIdentifier.INDEX).distinct()).distinct().
                withColumnRenamed(ColumnIdentifier.INDEX,"i").persist();

        scores_both = scores_both.join(branchMatches,col(ColumnIdentifier.INDEX).equalTo(col("i")),SparkComputer.JOIN_TYPES.LEFT_ANTI);

        scores_left = scores_left.join(branchMatches,col(ColumnIdentifier.INDEX).equalTo(col("i")),SparkComputer.JOIN_TYPES.LEFT_ANTI);

        scores_right = scores_right.join(branchMatches,col(ColumnIdentifier.INDEX).equalTo(col("i")),SparkComputer.JOIN_TYPES.LEFT_ANTI);

        branchMatches = branchMatches.unpersist();


        Dataset<Row> scoresJoined =
                scores_left.join(SparkComputer.appendIdentifier(scores_right,"right"),
                                col(ColumnIdentifier.INDEX).equalTo(col(ColumnIdentifier.INDEX+"right"))).
        select(ColumnIdentifier.INDEX,ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"2",ColumnIdentifier.POSITION+"1",
        ColumnIdentifier.POSITION+"2",ColumnIdentifier.POSITION+"3",ColumnIdentifier.POSITION+"4",ColumnIdentifier.POSITION+"5",ColumnIdentifier.POSITION+"6",
        ColumnIdentifier.LENGTH,ColumnIdentifier.LENGTH+"2",

        "pQ1","pQ2","pT1","pT2",
        "pQ1right","pQ2right","pT1right","pT2right");



        scores_both = scores_both.

        withColumn(ColumnIdentifier.ALIGN_RESULT+"right",col(ColumnIdentifier.ALIGN_RESULT)).
                withColumn("pQ1right",lit(null)).withColumn("pQ2right",lit(null)).
                withColumn("pT1right",lit(null)).withColumn("pT2right",lit(null)).
                withColumn(ColumnIdentifier.OVERLAP+"right",col(ColumnIdentifier.OVERLAP));

        Dataset<Row> allScores =
                scores_both.withColumn("origin",lit("both")).
                        select(ColumnIdentifier.INDEX,"origin",ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"2",ColumnIdentifier.POSITION+"1",
                        ColumnIdentifier.POSITION+"2",ColumnIdentifier.POSITION+"3",ColumnIdentifier.POSITION+"4",ColumnIdentifier.POSITION+"5",ColumnIdentifier.POSITION+"6",
                        ColumnIdentifier.LENGTH,ColumnIdentifier.LENGTH+"2",
                        "pQ1","pQ2","pT1","pT2",
                        "pQ1right","pQ2right","pT1right","pT2right").
                        union(
                                scoresJoined.
                                        withColumn("origin",lit("combined")).
                                        select(ColumnIdentifier.INDEX,"origin",ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"2",ColumnIdentifier.POSITION+"1",
                                                ColumnIdentifier.POSITION+"2",ColumnIdentifier.POSITION+"3",ColumnIdentifier.POSITION+"4",ColumnIdentifier.POSITION+"5",ColumnIdentifier.POSITION+"6",
                                                ColumnIdentifier.LENGTH,ColumnIdentifier.LENGTH+"2",
                                                //        ColumnIdentifier.EVALUE,ColumnIdentifier.EVALUE+"right",
                                                //        ColumnIdentifier.ALIGN_RESULT,ColumnIdentifier.ALIGN_RESULT+"right","e","eright",
                                                "pQ1","pQ2","pT1","pT2",
                                                "pQ1right","pQ2right","pT1right","pT2right")
                        );

        return allScores;
    }


    /**
     * compute overlap of top branch with synteny blocks on both flanks with respect to both genomes, only consider minIdentical nucleotides of these flanks
     * @param scores
     * @param minIdentical
     * @return
     */
    public static Dataset<Row> computeFlankOverlaps(Dataset<Row> scores, int minIdentical ) {
//        return                 ColumnFunctions.computeOverlap(
//                ColumnFunctions.computeOverlap(
//                        ColumnFunctions.computeOverlap(
//                                ColumnFunctions.computeOverlap(
//
//                                        scores
//                                        ,col("pQ1"),col("pQ2"),
//                                        pmod(col(ColumnIdentifier.POSITION+"5").minus(minIdentical),col(ColumnIdentifier.LENGTH)),
//                                        col(ColumnIdentifier.POSITION+"5"),
//                                        col(ColumnIdentifier.LENGTH),"oLR1Left",false),
//                                col("pQ1"),col("pQ2"),
//                                col(ColumnIdentifier.POSITION+"6"),pmod(col(ColumnIdentifier.POSITION+"6").plus(minIdentical),col(ColumnIdentifier.LENGTH)),
//                                col(ColumnIdentifier.LENGTH),"oLR1Right",false),
//                        col("pT1"),col("pT2"),
//                        pmod(col(ColumnIdentifier.POSITION+"1").minus(minIdentical),col(ColumnIdentifier.LENGTH+"2")),col(ColumnIdentifier.POSITION+"1"),
//                        col(ColumnIdentifier.LENGTH+"2"),"oLR2Left",false),
//                col("pT1"),col("pT2"),
//                col(ColumnIdentifier.POSITION+"4"),
//                pmod(col(ColumnIdentifier.POSITION+"4").plus(minIdentical),col(ColumnIdentifier.LENGTH+"2")),
//                col(ColumnIdentifier.LENGTH+"2"),"oLR2Right",false);
        return
                        ColumnFunctions.computeOverlap(
                                ColumnFunctions.computeOverlap(

                                        scores
                                        ,col("pQ1"),col("pQ2"),
                                        pmod(col(ColumnIdentifier.POSITION+"5").minus(minIdentical),col(ColumnIdentifier.LENGTH)),
                                        col(ColumnIdentifier.POSITION+"5"),
                                        col(ColumnIdentifier.LENGTH),"oLR1Left",false),
                                col("pQ1"),col("pQ2"),
                                col(ColumnIdentifier.POSITION+"6"),pmod(col(ColumnIdentifier.POSITION+"6").plus(minIdentical),col(ColumnIdentifier.LENGTH)),
                                col(ColumnIdentifier.LENGTH),"oLR1Right",false);

    }

    /**
     *
     * @param path
     * @param sequences
     * @param k
     * @param recordId of the NOT mapping genome (reference)
     * @param length of the NOT mapping genome (reference)
     * @param referencePosition of the NOT mapping genome (reference)
     * @return
     */
    public static Dataset<Row> compareSequenceSimilarityAlongPathNoShift(Dataset<Row> path, Dataset<Row> sequences, int k, String recordId, String length, String referencePosition) {
        String lengthOther = counterPart(length);
        String recordIdOther = counterPart(recordId);

        return         PairwiseAligner.addSubsequencesToRight(sequences,
                        PairwiseAligner.addSubsequencesToRight(sequences,
                                        path.
                                                withColumn(ColumnIdentifier.RANK,dense_rank().over(Window.partitionBy(ColumnIdentifier.INDEX).orderBy(desc(ColumnIdentifier.DISTANCE)))).
                                                filter(col(ColumnIdentifier.RANK).equalTo(1)).drop(ColumnIdentifier.RANK).
                                                withColumn(ColumnIdentifier.P1+"reference",pmod(col(referencePosition).plus(1),col(length))).
                                                withColumn(ColumnIdentifier.P1+"old",col(ColumnIdentifier.P1)).
//                                                withColumn(ColumnIdentifier.P2+"old",col(ColumnIdentifier.P2)).
        withColumn(ColumnIdentifier.P1,pmod(col(ColumnIdentifier.P1).minus(col(ColumnIdentifier.POSITION_INDEX)),col(lengthOther))).
//                                                withColumn(ColumnIdentifier.P2,pmod(col(ColumnIdentifier.P1).plus(col(ColumnIdentifier.POSITION_DIFF).minus(k-1)),col(lengthOther))).
        drop(ColumnIdentifier.POSITION_INDEX,ColumnIdentifier.POSITION_INDEX+"con",ColumnIdentifier.DISTANCE,ColumnIdentifier.P2).distinct(),
                                        col(ColumnIdentifier.POSITION_DIFF).minus(k),col(ColumnIdentifier.P1),col(recordIdOther)).
                                withColumnRenamed(ColumnIdentifier.SUBSEQUENCE,"S")
                        ,col(ColumnIdentifier.POSITION_DIFF).minus(k),col(ColumnIdentifier.P1+"reference")
//                        pmod(col(referencePosition),col(length)),col(recordId)).
                        ,col(recordId)).
                withColumnRenamed(ColumnIdentifier.SUBSEQUENCE,"Sreference").
                distinct().
                withColumn(ColumnIdentifier.ALIGN_RESULT,callUDF(PairwiseAligner.ALIGN_LOCAL_FAST_UDF,col("S"),col("Sreference"))).
//                withColumn(ColumnIdentifier.RANK,dense_rank().over(Window.partitionBy(ColumnIdentifier.INDEX).
//                        orderBy(ColumnIdentifier.ALIGN_RESULT+"."+ColumnIdentifier.EVALUE))).
//                filter(col(ColumnIdentifier.RANK).equalTo(1)).drop(ColumnIdentifier.RANK).
                // align positions
                        withColumn("pQ1",col(ColumnIdentifier.ALIGN_RESULT+"."+"pQ1")).
                withColumn("pQ2",col(ColumnIdentifier.ALIGN_RESULT+"."+"pQ2")).
                withColumn("pQ1",pmod(col(ColumnIdentifier.P1).plus(col("pQ1")).minus(1),col(lengthOther))).
                withColumn("pQ2",pmod(col(ColumnIdentifier.P1).plus(col("pQ2")).minus(1),col(lengthOther))).

                // reference align positions
                        withColumn("pT1",col(ColumnIdentifier.ALIGN_RESULT+"."+"pT1")).
                withColumn("pT2",col(ColumnIdentifier.ALIGN_RESULT+"."+"pT2")).
                withColumn("pT1",pmod(col(ColumnIdentifier.P1+"reference").plus(col("pT1")).minus(1),col(length))).
                withColumn("pT2",pmod(col(ColumnIdentifier.P1+"reference").plus(col("pT2")).minus(1),col(length)));

//                withColumn(ColumnIdentifier.SCORE,round(col(ColumnIdentifier.SCORE),2));
    }

    /*** Clustering ***/

    public static void clusterData(String path, int clusterDistance) {
        String runCmnd = " Rscript --vanilla "+ ProjectDirectoryManager.getSCRIPTS_DIR()+"/breakpointClusteringNoEvalues.R "+ path+"/BREAKPOINTS/FINAL_SHIFTED " + path+ " " + clusterDistance;

        try {
            Process process = Runtime.getRuntime().exec(runCmnd);
//            Auxiliary.printResults(process);
            process.waitFor();
            if(process.exitValue() != 0) {
                System.exit(-1);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static Dataset<Row> removeOverlaps(Dataset<Row> breakpoints, int threshold, int branchlength, String path) {

        String i1 = "bp_seq_id";
        String i2 = "compare_seq_id";
        String s1 = "bp_seq_p1";
        String e1 = "bp_seq_p2";
        String s2 = "compare_seq_p1";
        String e2 = "compare_seq_p2";
        String l1 = "bp_seq_length";
        String l2 = "compare_seq_length";

        String randomAcessor = UUID.randomUUID().toString();
        String c1 = path+"/"+ randomAcessor+ "can1";
        String c2 = path+"/"+ randomAcessor+ "can2";
        String c3 = path+"/"+ randomAcessor+ "can3";
        String c4 = path+"/"+ randomAcessor+ "can4";
        String c5 = path+"/"+ randomAcessor+ "can5";


        breakpoints = breakpoints
                .withColumn(ColumnIdentifier.RECORDS,concat_ws("_",
                when(col(i1).lt(col(i2)),col(i1)).otherwise(col(i2)),
                when(col(i2).lt(col(i1)),col(i2)).otherwise(col(i1))));



        // remove overlaps
        breakpoints = Spark.checkPointORC(
                breakpoints             .   join(
                SparkComputer.appendIdentifier(
                        ColumnFunctions.computeOverlapPositions2(
                                        breakpoints.join(SparkComputer.appendIdentifier(breakpoints,"n"),
                                                        col(i1).equalTo(col(i1+"n"))
                                                                .and(col(i2).equalTo(col(i2+"n")))
                                                                .and(not(col(s1).equalTo(col(s1+"n"))
                                                                        .and(col(e1).equalTo(col(e1+"n")))
                                                                        .and(col(s2).equalTo(col(s2+"n")))
                                                                        .and(col(e2).equalTo(col(e2+"n")))
                                                                )),
                                                        SparkComputer.JOIN_TYPES.INNER
                                                ).withColumn(ColumnIdentifier.RANGE_LENGTH+"1", ColumnFunctions.getCyclicRangeLength(col(s1),col(e1),col(l1)))
                                                .withColumn(ColumnIdentifier.RANGE_LENGTH+"2",ColumnFunctions.getCyclicRangeLength(col(s1+"n"),col(e1+"n"),col(l1)))

                                        ,
                                        col(s1),col(e1),
                                        col(s1+"n"),col(e1+"n"),
                                        col(ColumnIdentifier.RANGE_LENGTH+"1"),
                                        col(ColumnIdentifier.RANGE_LENGTH+"2"),
                                        col(l1),ColumnIdentifier.OVERLAP
                                )
                                .filter(not(isnull(col(ColumnIdentifier.OVERLAP))))
                                .withColumn(ColumnIdentifier.RANGE_LENGTH+"3",
                                        ColumnFunctions.getCyclicRangeLength(element_at(col(ColumnIdentifier.OVERLAP),1),
                                                element_at(col(ColumnIdentifier.OVERLAP),2),col(l1)))
                                .withColumn(ColumnIdentifier.RATIO,col(ColumnIdentifier.RANGE_LENGTH+"3").divide(col(ColumnIdentifier.RANGE_LENGTH+"1")))
                                .withColumn(ColumnIdentifier.RATIO+"2",col(ColumnIdentifier.RANGE_LENGTH+"3").divide(col(ColumnIdentifier.RANGE_LENGTH+"2")))
                                .filter(col(ColumnIdentifier.RATIO).geq(0.90).or(col(ColumnIdentifier.RATIO+"2").geq(0.90))
//                        .and(not(col(ColumnIdentifier.RATIO).equalTo(1).and(col(ColumnIdentifier.RATIO+"2").equalTo(1))))
                                )
                                .filter(col(ColumnIdentifier.RATIO).lt(col(ColumnIdentifier.RATIO+"2")).
                                        and(col(ColumnIdentifier.RANGE_LENGTH+"1").divide(col(ColumnIdentifier.RANGE_LENGTH+"2")).leq(3) ))
                                .select(i1,i2,s1,s2,e1,e2),
                        "n"
                ),
                col(i1).equalTo(col(i1+"n"))
                        .and(col(i2).equalTo(col(i2+"n")))
                        .and(col(s2).equalTo(col(s2+"n")))
                        .and(col(e2).equalTo(col(e2+"n")))
                        .and(col(s1).equalTo(col(s1+"n")))
                        .and(col(e1).equalTo(col(e1+"n"))),
                SparkComputer.JOIN_TYPES.LEFT_ANTI
        ),c1).persist();





        WindowSpec w = Window.partitionBy(ColumnIdentifier.RECORDS,i1).orderBy("p");
        WindowSpec w2 = Window.partitionBy(ColumnIdentifier.RECORDS,i1).orderBy(desc("p"));

        Dataset<Row> can = Spark.checkPointORC(breakpoints.select(col(ColumnIdentifier.RECORDS),
                        col(i1),col(l1),
                        col(i2),col(l2),
                        col(s1).as("p"),col(s2).as("p2")).
                union(breakpoints.select(col(ColumnIdentifier.RECORDS),col(i1),col(l1),
                        col(i2),col(l2),
                        col(e1).as("p"),col(e2).as("p2")))
                .union(breakpoints.select(col(ColumnIdentifier.RECORDS),col(i2).as(i1),col(l2).as(l1),
                        col(i1).as(i2),col(l1).as(l2),
                        col(s2).as("p"),col(s1).as("p2")))
                .union(breakpoints.select(col(ColumnIdentifier.RECORDS),col(i2).as(i1),col(l2).as(l1),
                        col(i1).as(i2),col(l1).as(l2),
                        col(e2).as("p"),col(e1).as("p2")))
                .distinct(),c3)
//                .withColumn("prev",lag(col("p"),1).over(w) )
//                .withColumn("suc",lead(col("p"),1).over(w))
//                .withColumn("prev2",lag(col("p2"),1).over(w))
//                .withColumn("suc2",lead(col("p2"),1).over(w))
//                .withColumn(ColumnIdentifier.RANK,dense_rank().over(w))
//                .withColumn(ColumnIdentifier.RANK+"2",dense_rank().over(w2))
                .persist();


        Dataset<Row> canP = can.select(col(ColumnIdentifier.RECORDS),
                        col(i1),col(l1),
                        col(i2),col(l2),
                        col("p")

                ).distinct()
                .withColumn("prev",lag(col("p"),1).over(w) )
                .withColumn("suc",lead(col("p"),1).over(w))
                .withColumn(ColumnIdentifier.RANK,dense_rank().over(w))
                .withColumn(ColumnIdentifier.RANK+"2",dense_rank().over(w2)).persist();



        String[] colsCan = canP.columns();



        canP = Spark.checkPointORC(canP.filter(not(isnull(col("prev")).or(isnull(col("suc"))) ))
                .union(
                    canP.filter(isnull(col("prev"))).join(SparkComputer.appendIdentifier(canP.filter(col(ColumnIdentifier.RANK+"2").equalTo(1)),"n"),
                                col(ColumnIdentifier.RECORDS).equalTo(col(ColumnIdentifier.RECORDS+"n"))
                                        .and(col(i1).equalTo(col(i1+"n")))
                        )
                        .withColumn("prev",col("pn"))
                        .select(SparkComputer.getColumns(colsCan)))
                .union(
                        canP.filter(isnull(col("suc"))).join(SparkComputer.appendIdentifier(canP.filter(col(ColumnIdentifier.RANK).equalTo(1)),"n"),
                                col(ColumnIdentifier.RECORDS).equalTo(col(ColumnIdentifier.RECORDS+"n"))
                                        .and(col(i1).equalTo(col(i1+"n")))
                        )
                        .withColumn("suc",col("pn")).select(SparkComputer.getColumns(colsCan))).drop(ColumnIdentifier.RANK,ColumnIdentifier.RANK+"2"),c4).persist();



        Dataset<Row> marked =  breakpoints.join(SparkComputer.appendIdentifier(breakpoints,"n"),
                        col(i1).equalTo(col(i2+"n"))
                                .and(col(i2).equalTo(col(i1+"n")))
                                .and(pmod(col(s2).minus(col(s1+"n")),col(l2)).leq(threshold).
                                        or(pmod(col(s1+"n").minus(col(s2)),col(l2)).leq(threshold))),
                        SparkComputer.JOIN_TYPES.LEFT_ANTI
                ).join(SparkComputer.appendIdentifier(breakpoints,"n"),
                        col(i1).equalTo(col(i2+"n"))
                                .and(col(i2).equalTo(col(i1+"n")))
                                .and(pmod(col(s2).minus(col(e1+"n")),col(l2)).leq(threshold).
                                        or(pmod(col(e1+"n").minus(col(s2)),col(l2)).leq(threshold))),
                        SparkComputer.JOIN_TYPES.LEFT_ANTI
                ).select(col(ColumnIdentifier.RECORDS),col(i1).as(i2),col(i2).as(i1),col(s2).as("c1")).
                union(
                        breakpoints.join(SparkComputer.appendIdentifier(breakpoints,"n"),
                                col(i1).equalTo(col(i2+"n"))
                                        .and(col(i2).equalTo(col(i1+"n")))
                                        .and(pmod(col(e2).minus(col(s1+"n")),col(l2)).leq(threshold).
                                                or(pmod(col(s1+"n").minus(col(e2)),col(l2)).leq(threshold))),
                                SparkComputer.JOIN_TYPES.LEFT_ANTI
                        ).join(SparkComputer.appendIdentifier(breakpoints,"n"),
                                col(i1).equalTo(col(i2+"n"))
                                        .and(col(i2).equalTo(col(i1+"n")))
                                        .and(pmod(col(e2).minus(col(e1+"n")),col(l2)).leq(threshold).
                                                or(pmod(col(e1+"n").minus(col(e2)),col(l2)).leq(threshold))),
                                SparkComputer.JOIN_TYPES.LEFT_ANTI).select(col(ColumnIdentifier.RECORDS),
                                col(i1).as(i2),col(i2).as(i1),col(e2).as("c1")));

        // only keep those where no data is available
        canP = canP
                .join(SparkComputer.appendIdentifier(marked.drop(i2),"n",ColumnIdentifier.RECORDS,i1),
                col(ColumnIdentifier.RECORDS).equalTo(col(ColumnIdentifier.RECORDS+"n"))
                        .and(col(i1).equalTo(col(i1+"n"))).and(col("p").equalTo(col("c1"))),
//                        SparkComputer.JOIN_TYPES.LEFT
                SparkComputer.JOIN_TYPES.INNER
        )                .drop(ColumnIdentifier.RECORDS+"n",i1+"n")
                .join(SparkComputer.appendIdentifier(can.select(ColumnIdentifier.RECORDS,i1,"p","p2"),"n"),
                        col(ColumnIdentifier.RECORDS).equalTo(col(ColumnIdentifier.RECORDS+"n"))
                                .and(col(i1).equalTo(col(i1+"n")))
                                .and(col("p").equalTo(col("pn"))))
                .withColumn("p2",col("p2n"))
                .drop(ColumnIdentifier.RECORDS+"n",i1+"n","pn","p2n")
            .persist();


        canP = Spark.checkPoint(
                canP
                        .filter(ColumnFunctions.getCyclicRangeLength(col("prev"),col("p"),col(l1)).leq(branchlength))
                        .join(SparkComputer.appendIdentifier(can,"n"),
                                col(ColumnIdentifier.RECORDS).equalTo(col(ColumnIdentifier.RECORDS+"n"))
                                        .and(col(i1).equalTo(col(i1+"n")))
                                        .and(col("prev").equalTo(col("pn"))))
                        .withColumn("prev2",col("p2n"))
                        .select(col(ColumnIdentifier.RECORDS),col(i1),col(i2),col("prev").as(s1),col("p").as(e1),
                                col("prev2").as(s2),col("p2").as(e2),col(l1),col(l2)).
                        union(        canP
                                .filter(ColumnFunctions.getCyclicRangeLength(col("p"),col("suc"),col(l1)).leq(branchlength))
                                .join(SparkComputer.appendIdentifier(can,"n"),
                                        col(ColumnIdentifier.RECORDS).equalTo(col(ColumnIdentifier.RECORDS+"n"))
                                                .and(col(i1).equalTo(col(i1+"n")))
                                                .and(col("suc").equalTo(col("pn"))))
                                .withColumn("suc2",col("p2n"))
                                .select(col(ColumnIdentifier.RECORDS),
                                        col(i1),col(i2),col("p").as(s1),col("suc").as(e1),
                                        col("p2").as(s2),col("suc2").as(e2),col(l1),col(l2))).distinct(),c5);


        breakpoints = breakpoints.withColumn("m",lit(false));
        String[] cols = breakpoints.columns();
        breakpoints = breakpoints.union(canP.withColumn("m",lit(true)).select(SparkComputer.getColumns(cols))).persist();

        breakpoints = breakpoints
                        .
                join(
                        SparkComputer.appendIdentifier(
                                ColumnFunctions.computeOverlapPositions2(
                                                breakpoints.join(SparkComputer.appendIdentifier(breakpoints,"n"),
                                                                col(i1).equalTo(col(i1+"n"))
                                                                        .and(col(i2).equalTo(col(i2+"n")))
                                                                        .and(not(col(s1).equalTo(col(s1+"n"))
                                                                                .and(col(e1).equalTo(col(e1+"n")))
                                                                                .and(col(s2).equalTo(col(s2+"n")))
                                                                                .and(col(e2).equalTo(col(e2+"n")))
                                                                        )),
                                                                SparkComputer.JOIN_TYPES.INNER
                                                        ).withColumn(ColumnIdentifier.RANGE_LENGTH+"1", ColumnFunctions.getCyclicRangeLength(col(s1),col(e1),col(l1)))
                                                        .withColumn(ColumnIdentifier.RANGE_LENGTH+"2",ColumnFunctions.getCyclicRangeLength(col(s1+"n"),col(e1+"n"),col(l1)))

                                                ,
                                                col(s1),col(e1),
                                                col(s1+"n"),col(e1+"n"),
                                                col(ColumnIdentifier.RANGE_LENGTH+"1"),
                                                col(ColumnIdentifier.RANGE_LENGTH+"2"),
                                                col(l1),ColumnIdentifier.OVERLAP
                                        )
                                        .filter(not(isnull(col(ColumnIdentifier.OVERLAP))))
                                        .withColumn(ColumnIdentifier.RANGE_LENGTH+"3",
                                                ColumnFunctions.getCyclicRangeLength(element_at(col(ColumnIdentifier.OVERLAP),1),
                                                        element_at(col(ColumnIdentifier.OVERLAP),2),col(l1)))
                                        .withColumn(ColumnIdentifier.RATIO,col(ColumnIdentifier.RANGE_LENGTH+"3").divide(col(ColumnIdentifier.RANGE_LENGTH+"1")))
                                        .withColumn(ColumnIdentifier.RATIO+"2",col(ColumnIdentifier.RANGE_LENGTH+"3").divide(col(ColumnIdentifier.RANGE_LENGTH+"2")))
                                        .filter(col(ColumnIdentifier.RATIO).geq(0.90).or(col(ColumnIdentifier.RATIO+"2").geq(0.90))
//                        .and(not(col(ColumnIdentifier.RATIO).equalTo(1).and(col(ColumnIdentifier.RATIO+"2").equalTo(1))))
                                        )
                                        .filter(col(ColumnIdentifier.RATIO).lt(col(ColumnIdentifier.RATIO+"2")).
                                                and(col(ColumnIdentifier.RANGE_LENGTH+"1").divide(col(ColumnIdentifier.RANGE_LENGTH+"2")).leq(3) ))
                                        .select(i1,i2,s1,s2,e1,e2),
                                "n"
                        ),
                        col(i1).equalTo(col(i1+"n"))
                                .and(col(i2).equalTo(col(i2+"n")))
                                .and(col(s2).equalTo(col(s2+"n")))
                                .and(col(e2).equalTo(col(e2+"n")))
                                .and(col(s1).equalTo(col(s1+"n")))
                                .and(col(e1).equalTo(col(e1+"n"))),
                        SparkComputer.JOIN_TYPES.LEFT_ANTI
                ).persist();

        String[] cols2 = breakpoints.columns();



        breakpoints = breakpoints.except(
        breakpoints
                .withColumn("ma",lag(col("m"),1).over(Window.partitionBy(ColumnIdentifier.RECORDS,i1).orderBy(s1)))
                .withColumn("a1",lag(col(e1),1).over(Window.partitionBy(ColumnIdentifier.RECORDS,i1).orderBy(s1)))
                .withColumn("a2",lag(col(e2),1).over(Window.partitionBy(ColumnIdentifier.RECORDS,i1).orderBy(s1)))
                .withColumn(ColumnIdentifier.RANGE_LENGTH+"1",ColumnFunctions.getCyclicRangeLength(col(s1),col(e1),col(l1)))
                .withColumn(ColumnIdentifier.RANGE_LENGTH+"2",ColumnFunctions.getCyclicRangeLength(col(s2),col(e2),col(l1)))
                .filter(not(col("ma")).and(col("m")).and(col("a1").equalTo(col(s1))).and(col("a2").equalTo(col(s2)))
                        .and(abs(col(ColumnIdentifier.RANGE_LENGTH+"1").minus(col(ColumnIdentifier.RANGE_LENGTH+"2"))).leq(10))
                ).select(SparkComputer.getColumns(cols2)));

        return breakpoints;
    }

    /*** Compute IP and TP +**/
    /**
     * Compute breakpoint candidates with rownumber for each individual candidate
     * @param edges
     * @param recordData
     * @param minDistance
     * @param maxDistance
     * @param minGeneLength
     * @param path
     * @return
     */
    public static Dataset<Row> detectBreakPointCandidates(Dataset<Row> edges, Dataset<Row> recordData, int minDistance,
                                                          int maxDistance, int minGeneLength,String path) {


        Dataset<Row> startPoints = detectBreakPointCandidatesStart(edges,recordData,minDistance,maxDistance,minGeneLength,path);//SparkComputer.readORC(path+"/SP2");


        startPoints =
                Spark.checkPointORC(
                        startPoints.
                                withColumnRenamed("p1",ColumnIdentifier.POSITION+"5").
                                withColumnRenamed("p2",ColumnIdentifier.POSITION+"1").
                                withColumnRenamed("p3",ColumnIdentifier.POSITION+"6").
                                withColumnRenamed("p4",ColumnIdentifier.POSITION+"4").
                                withColumnRenamed("p5",ColumnIdentifier.POSITION+"3").
                                withColumnRenamed("p6",ColumnIdentifier.POSITION+"2").
                                filter(ColumnFunctions.computeDistanceExcludingBoundaryPositions(
                                                ColumnIdentifier.POSITION+"3",ColumnIdentifier.POSITION+"4",
                                                ColumnIdentifier.LENGTH+"2",ColumnIdentifier.TOPOLOGY,ColumnIdentifier.STRAND+"2").
                                        lt(
                                                least(
                                                        ColumnFunctions.computeDistanceExcludingBoundaryPositions(
                                                                ColumnIdentifier.POSITION+"3",ColumnIdentifier.POSITION+"1",
                                                                ColumnIdentifier.LENGTH+"2",ColumnIdentifier.TOPOLOGY,ColumnIdentifier.STRAND+"2"),
                                                        ColumnFunctions.computeDistanceExcludingBoundaryPositions(
                                                                ColumnIdentifier.POSITION+"3",ColumnIdentifier.POSITION+"2",
                                                                ColumnIdentifier.LENGTH+"2",ColumnIdentifier.TOPOLOGY,ColumnIdentifier.STRAND+"2"))).
                                        and(
                                                ColumnFunctions.computeDistanceExcludingBoundaryPositions(
                                                                ColumnIdentifier.POSITION+"1",ColumnIdentifier.POSITION+"2",
                                                                ColumnIdentifier.LENGTH+"2",ColumnIdentifier.TOPOLOGY,ColumnIdentifier.STRAND+"2").
                                                        lt(
                                                                least(
                                                                        ColumnFunctions.computeDistanceExcludingBoundaryPositions(
                                                                                ColumnIdentifier.POSITION+"1",ColumnIdentifier.POSITION+"3",
                                                                                ColumnIdentifier.LENGTH+"2",ColumnIdentifier.TOPOLOGY,ColumnIdentifier.STRAND+"2"),
                                                                        ColumnFunctions.computeDistanceExcludingBoundaryPositions(
                                                                                ColumnIdentifier.POSITION+"1",ColumnIdentifier.POSITION+"4",
                                                                                ColumnIdentifier.LENGTH+"2",ColumnIdentifier.TOPOLOGY,ColumnIdentifier.STRAND+"2"))))),
                        path+"/SP3");


        startPoints =
                startPoints.

        withColumn(ColumnIdentifier.DISTANCE+"R2",ColumnFunctions.computeDistanceExcludingBoundaryPositions(
        ColumnIdentifier.POSITION+"2",ColumnIdentifier.POSITION+"3",
        ColumnIdentifier.LENGTH+"2",ColumnIdentifier.TOPOLOGY,ColumnIdentifier.STRAND+"2")).
                        withColumn(ColumnIdentifier.RANK,dense_rank().over(Window.partitionBy(
                                ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"2",
                                ColumnIdentifier.POSITION+"5",ColumnIdentifier.POSITION+"6",
                                ColumnIdentifier.POSITION+"1",ColumnIdentifier.POSITION+"4").orderBy(desc(ColumnIdentifier.DISTANCE+"R2")))).
                        filter(col(ColumnIdentifier.RANK).equalTo(1)).drop(ColumnIdentifier.RANK);

//        startPoints = Spark.checkPointORC(startPoints,path+"/SP3");
//        SparkComputer.persistDataFrameORC(startPoints,path+"/SP3");

//        sP.unpersist();
//        System.gc();

//        Spark.clearCheckPointDir(Spark.SPARK_CHECKPOINT_DIR+"/SP3");
//        Dataset<Row> unsuitedBranches =
//                startPoints.withColumn(ColumnIdentifier.RANGE,
//                        ColumnFunctions.computeIntermediatePositions(ColumnIdentifier.POSITION+"5",ColumnIdentifier.POSITION+"6",
//                                ColumnIdentifier.LENGTH,ColumnIdentifier.TOPOLOGY,ColumnIdentifier.STRAND,null)).
//                        withColumn(ColumnIdentifier.POSITION+"R",explode(col(ColumnIdentifier.RANGE))).
//                        withColumn(ColumnIdentifier.K_PLUS_ONE_MER_COUNT,size(col(ColumnIdentifier.RANGE))).
//                        drop(ColumnIdentifier.RANGE).
//                        join(SparkComputer.appendIdentifier(edges,"E"),
//                                col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"E")).
//                                        and(col(ColumnIdentifier.POSITION+"R").equalTo(col(ColumnIdentifier.POSITION+"E"))).
//                                        and(col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+"E")))).
//                        drop(ColumnIdentifier.RECORD_ID+"E",ColumnIdentifier.STRAND+"E",ColumnIdentifier.POSITION+"E").
//                        withColumnRenamed(ColumnIdentifier.SRC+"E",ColumnIdentifier.SRC).
//                        withColumnRenamed(ColumnIdentifier.DST+"E",ColumnIdentifier.DST).
//                        join(SparkComputer.appendIdentifier(edges,"E2"),
//                                col(ColumnIdentifier.RECORD_ID+"2").equalTo(col(ColumnIdentifier.RECORD_ID+"E2")).
//                                        and(col(ColumnIdentifier.SRC).equalTo(col(ColumnIdentifier.SRC+"E2"))).
//                                        and(col(ColumnIdentifier.DST).equalTo(col(ColumnIdentifier.DST+"E2"))).
//                                        and(col(ColumnIdentifier.STRAND+"2").equalTo(col(ColumnIdentifier.STRAND+"E2")))).
//
//                        groupBy(ColumnIdentifier.ROW_NUMBER,ColumnIdentifier.K_PLUS_ONE_MER_COUNT).
//                        agg(countDistinct(col(ColumnIdentifier.SRC),col(ColumnIdentifier.DST)).as(ColumnIdentifier.MATCH_AMOUNT)).
//                        withColumn(ColumnIdentifier.RATIO,col(ColumnIdentifier.MATCH_AMOUNT).divide(col(ColumnIdentifier.K_PLUS_ONE_MER_COUNT))).
//                        filter(col(ColumnIdentifier.RATIO).gt(0.1)).select(ColumnIdentifier.ROW_NUMBER);
//
////        unsuitedBranches = Spark.checkPointORC(unsuitedBranches,"UB");
//        startPoints =
//                startPoints.join(
//                        // remove all repeat paths
//                        SparkComputer.appendIdentifier(unsuitedBranches,"s"),
//                        col(ColumnIdentifier.ROW_NUMBER).equalTo(col(ColumnIdentifier.ROW_NUMBER+"s")),
//                        SparkComputer.JOIN_TYPES.LEFT_ANTI).
//                        distinct();
//        Spark.clearCheckPointDir(Spark.SPARK_CHECKPOINT_DIR+"/SP4");
//        Spark.clearCheckPointDir("US");
        return startPoints;

    }

    public static Dataset<Row> detectBreakPointCandidatesStart(Dataset<Row> edges, Dataset<Row> recordData, int minDistance, int maxDistance, int minGeneLength, String path) {
        String endIdentifier = "end";
        String endIdentifier2 = "end2";
        String startIdentifier = "start";
        String endBranch = "eB";
        String startBranch = "sB";
        String candidate = "candidate";
        String optIdentier = "opt";
        int minbranchdifference = 5;


        identifyBPStartAndEndPoints(edges,recordData,path+"/SP",path+"/EP");

        Dataset<Row> startPoints = SparkComputer.readORC(path+"/SP");
        Dataset<Row> endPoints = SparkComputer.readORC(path+"/EP");


        startPoints = startPoints.withColumn(ColumnIdentifier.TOPOLOGY,lit(true)).
                // find first and last positions of all shorter paths between startvertex and endvertex of recordId
                        join(
                        SparkComputer.appendIdentifier(endPoints.drop(ColumnIdentifier.LENGTH),endIdentifier).
                                withColumnRenamed(ColumnIdentifier.POSITION+endIdentifier,ColumnIdentifier.POSITION),
                        col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+endIdentifier)).
                                and(col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+endIdentifier)))).
                withColumn(ColumnIdentifier.DISTANCE+"R1",ColumnFunctions.computeDistanceExcludingBoundaryPositions(
                        ColumnIdentifier.POSITION+"2",ColumnIdentifier.POSITION,
                        ColumnIdentifier.LENGTH,ColumnIdentifier.TOPOLOGY,ColumnIdentifier.STRAND)).
                // make sure path is not too long
                        filter(col(ColumnIdentifier.DISTANCE+"R1").between(minDistance,maxDistance)).
                drop(ColumnIdentifier.STRAND+endIdentifier,ColumnIdentifier.RECORD_ID+endIdentifier).

                //of identified endvertex, select edge to previous vertex not on above path with recordId2 !=recordId
                        join(
                        SparkComputer.appendIdentifier(endPoints,endIdentifier2),
                        col(ColumnIdentifier.START_TRAVERSAL_VERTEX+endIdentifier).equalTo(col(ColumnIdentifier.START_TRAVERSAL_VERTEX+endIdentifier2)).
                                and(col(ColumnIdentifier.START_VERTEX+endIdentifier).equalTo(col(ColumnIdentifier.START_VERTEX+endIdentifier2))).
                                and(col(ColumnIdentifier.PREV_VERTEX+endIdentifier).notEqual(col(ColumnIdentifier.PREV_VERTEX+endIdentifier2))).
                                and(col(ColumnIdentifier.RECORD_ID).notEqual(col(ColumnIdentifier.RECORD_ID+endIdentifier2)))
                ).drop(ColumnIdentifier.START_TRAVERSAL_VERTEX+endIdentifier2,ColumnIdentifier.START_VERTEX+endIdentifier2).
                withColumnRenamed(ColumnIdentifier.LENGTH+endIdentifier2,ColumnIdentifier.LENGTH+"2").
                withColumnRenamed(ColumnIdentifier.RECORD_ID+endIdentifier2,ColumnIdentifier.RECORD_ID+"2").
                withColumnRenamed(ColumnIdentifier.STRAND+endIdentifier2,ColumnIdentifier.STRAND+"2").
                // only keep the identified connection to previous vertex, if recordId2 is also annotated on succeeding vertex to startvertex not on this path
                        join(
                        SparkComputer.appendIdentifier(startPoints.drop(ColumnIdentifier.LENGTH,ColumnIdentifier.STRAND),startIdentifier),
                        col(ColumnIdentifier.PREV_VERTEX).equalTo(col(ColumnIdentifier.PREV_VERTEX+startIdentifier)).
                                and(col(ColumnIdentifier.START_VERTEX).equalTo(col(ColumnIdentifier.START_VERTEX+startIdentifier))).
                                and(col(ColumnIdentifier.START_TRAVERSAL_VERTEX).notEqual(col(ColumnIdentifier.START_TRAVERSAL_VERTEX+startIdentifier))).
                                and(col(ColumnIdentifier.RECORD_ID+"2").equalTo(col(ColumnIdentifier.RECORD_ID+startIdentifier)))
                ).drop(ColumnIdentifier.PREV_VERTEX+startIdentifier,ColumnIdentifier.START_VERTEX+startIdentifier,ColumnIdentifier.RECORD_ID+startIdentifier).
                withColumn(ColumnIdentifier.DISTANCE+"R2",ColumnFunctions.computeDistanceExcludingBoundaryPositions(
                        ColumnIdentifier.POSITION+"2"+startIdentifier,ColumnIdentifier.POSITION+endIdentifier2,
                        ColumnIdentifier.LENGTH+"2",ColumnIdentifier.TOPOLOGY,ColumnIdentifier.STRAND+"2")).

                filter(col(ColumnIdentifier.DISTANCE+"R2").geq(col(ColumnIdentifier.DISTANCE+"R1").plus(minbranchdifference)).
                        and(col(ColumnIdentifier.DISTANCE+"R2").gt(minGeneLength))).
                select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND,ColumnIdentifier.LENGTH,ColumnIdentifier.TOPOLOGY,
                        ColumnIdentifier.RECORD_ID+"2",ColumnIdentifier.STRAND+"2",ColumnIdentifier.LENGTH+"2",
                        ColumnIdentifier.DISTANCE+"R1",ColumnIdentifier.DISTANCE+"R2",
                        ColumnIdentifier.POSITION+"2",ColumnIdentifier.POSITION,
                        ColumnIdentifier.POSITION+endIdentifier2,ColumnIdentifier.POSITION+"2"+startIdentifier).
                distinct().persist();

        return
                        startPoints.
                                // right upper branch -> pos3
                                        join(SparkComputer.appendIdentifier(startPoints.drop(ColumnIdentifier.LENGTH,ColumnIdentifier.TOPOLOGY),endBranch),
                                        col(ColumnIdentifier.RECORD_ID+"2").equalTo(col(ColumnIdentifier.RECORD_ID+endBranch)).
                                                and(col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"2"+endBranch))).
                                                and(col(ColumnIdentifier.POSITION+endIdentifier2).equalTo(col(ColumnIdentifier.POSITION+endBranch))).
                                                and(col(ColumnIdentifier.POSITION).equalTo(col(ColumnIdentifier.POSITION+endIdentifier2+endBranch)))
                                ).drop(ColumnIdentifier.RECORD_ID+endBranch,ColumnIdentifier.PREV_VERTEX+endIdentifier+endBranch,ColumnIdentifier.START_VERTEX+endIdentifier+endBranch,
                                        ColumnIdentifier.START_TRAVERSAL_VERTEX+endIdentifier+endBranch,ColumnIdentifier.POSITION+endBranch,ColumnIdentifier.POSITION+endIdentifier2+endBranch).
                                // left upper branch -> pos2
                                        join(SparkComputer.appendIdentifier(startPoints.drop(ColumnIdentifier.LENGTH,ColumnIdentifier.TOPOLOGY),startBranch),
                                        col(ColumnIdentifier.RECORD_ID+"2").equalTo(col(ColumnIdentifier.RECORD_ID+startBranch)).
                                                and(col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"2"+startBranch))).
                                                and(col(ColumnIdentifier.POSITION+"2"+startIdentifier).equalTo(col(ColumnIdentifier.POSITION+"2"+startBranch))).
                                                and(col(ColumnIdentifier.POSITION+"2").equalTo(col(ColumnIdentifier.POSITION+"2"+startIdentifier+startBranch)))
                                ).
                                withColumnRenamed(ColumnIdentifier.POSITION+"2","p1").
                                withColumnRenamed(ColumnIdentifier.POSITION+"2"+startIdentifier,"p2").
                                withColumnRenamed(ColumnIdentifier.POSITION,"p3").
                                withColumnRenamed(ColumnIdentifier.POSITION+endIdentifier2,"p4").
                                withColumnRenamed(ColumnIdentifier.POSITION+"2"+endBranch,"p5").
                                withColumnRenamed(ColumnIdentifier.POSITION+startBranch,"p6").
                                select(
                                        ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"2",
                                        ColumnIdentifier.STRAND,ColumnIdentifier.LENGTH,
                                        ColumnIdentifier.STRAND+"2",ColumnIdentifier.LENGTH+"2",
                                        ColumnIdentifier.TOPOLOGY,
                                        ColumnIdentifier.DISTANCE+"R1",ColumnIdentifier.DISTANCE+"R2",
                                        "p1","p3",
                                        "p2","p6",
                                        "p5","p4").distinct();

    }

    public static void identifyBPStartAndEndPoints(Dataset<Row> edges, Dataset<Row> recordData, String startPath, String endPath) {

        edges = edges.join(recordData.select(col(ColumnIdentifier.RECORD_ID).as("r"),col(ColumnIdentifier.LENGTH)),
                col(ColumnIdentifier.RECORD_ID).equalTo(col("r"))).drop("r");


        Dataset<Row> startTripel = edges.
                drop(ColumnIdentifier.CATEGORIES,ColumnIdentifier.GENES).
                filter(col(ColumnIdentifier.SRC).notEqual(col(ColumnIdentifier.DST)) ).
                withColumnRenamed(ColumnIdentifier.SRC,ColumnIdentifier.PREV_VERTEX).
                withColumnRenamed(ColumnIdentifier.DST,ColumnIdentifier.START_VERTEX).
                join(SparkComputer.appendIdentifier(edges.drop(ColumnIdentifier.LENGTH) ,"2"),
                        col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"2")).
                                and(col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+"2"))).
                                and(col(ColumnIdentifier.POSITION+"2").
                                        equalTo(ColumnFunctions.getRightNeighbour(ColumnIdentifier.POSITION,ColumnIdentifier.LENGTH,ColumnIdentifier.STRAND))).
                                and(col(ColumnIdentifier.DST+"2").notEqual(col(ColumnIdentifier.PREV_VERTEX)))
                ).filter(col(ColumnIdentifier.SRC+"2").notEqual(col(ColumnIdentifier.DST+"2"))).
                drop(ColumnIdentifier.SRC+"2",ColumnIdentifier.RECORD_ID+"2",ColumnIdentifier.STRAND+"2").
                withColumnRenamed(ColumnIdentifier.DST+"2",ColumnIdentifier.START_TRAVERSAL_VERTEX).persist();

        Dataset<Row> startPoints =startTripel.
                groupBy(ColumnIdentifier.PREV_VERTEX,ColumnIdentifier.START_VERTEX).
                agg(countDistinct(col(ColumnIdentifier.RECORD_ID)).as(ColumnIdentifier.RECORDS_IN),
                        countDistinct(ColumnIdentifier.START_TRAVERSAL_VERTEX).as(ColumnIdentifier.COUNT)).
                filter(col(ColumnIdentifier.COUNT).gt(1).and(col(ColumnIdentifier.RECORDS_IN).gt(1))).
                drop(ColumnIdentifier.COUNT,ColumnIdentifier.RECORDS_IN).
                join(SparkComputer.appendIdentifier(startTripel,"s",ColumnIdentifier.START_VERTEX,ColumnIdentifier.PREV_VERTEX,ColumnIdentifier.START_TRAVERSAL_VERTEX),
                        col(ColumnIdentifier.START_VERTEX).equalTo(col(ColumnIdentifier.START_VERTEX+"s")).
                                and(col(ColumnIdentifier.PREV_VERTEX).equalTo(col(ColumnIdentifier.PREV_VERTEX+"s")))).
                drop(ColumnIdentifier.START_VERTEX +"s",ColumnIdentifier.PREV_VERTEX+"s",ColumnIdentifier.POSITION,ColumnIdentifier.COUNT,ColumnIdentifier.RECORDS_OUT).
//                withColumnRenamed(ColumnIdentifier.RECORDS_OUT+"s",ColumnIdentifier.RECORDS_OUT).
        withColumnRenamed(ColumnIdentifier.START_TRAVERSAL_VERTEX+"s",ColumnIdentifier.START_TRAVERSAL_VERTEX);

        SparkComputer.persistDataFrameORC(startPoints,startPath);

        WindowSpec w = Window.partitionBy(ColumnIdentifier.START_VERTEX,ColumnIdentifier.START_TRAVERSAL_VERTEX).
                rowsBetween(Window.unboundedPreceding(),Window.unboundedFollowing());
//        Dataset<Row> endPoints = startTripel.
//                withColumn(ColumnIdentifier.COUNT,approx_count_distinct(col(ColumnIdentifier.RECORD_ID),0.01).over(w))
//                .filter(col(ColumnIdentifier.COUNT).gt(1)).drop(ColumnIdentifier.COUNT)
//                .withColumn(ColumnIdentifier.COUNT,approx_count_distinct(col(ColumnIdentifier.PREV_VERTEX),0.01).over(w))
//                .filter(col(ColumnIdentifier.COUNT).gt(1)).drop(ColumnIdentifier.COUNT)
//                .drop(ColumnIdentifier.START_VERTEX ,ColumnIdentifier.START_TRAVERSAL_VERTEX,ColumnIdentifier.POSITION+"2",
//                        ColumnIdentifier.COUNT,ColumnIdentifier.RECORDS_IN);

        Dataset<Row> endPoints = startTripel.
                groupBy(ColumnIdentifier.START_VERTEX,ColumnIdentifier.START_TRAVERSAL_VERTEX).
                agg(countDistinct(col(ColumnIdentifier.RECORD_ID)).as(ColumnIdentifier.RECORDS_OUT),
                        countDistinct(ColumnIdentifier.PREV_VERTEX).as(ColumnIdentifier.COUNT)).
                filter(col(ColumnIdentifier.COUNT).gt(1).and(col(ColumnIdentifier.RECORDS_OUT).gt(1))).
                drop(ColumnIdentifier.COUNT,ColumnIdentifier.RECORDS_OUT).
                join(SparkComputer.appendIdentifier(startTripel,"s",ColumnIdentifier.START_VERTEX,ColumnIdentifier.PREV_VERTEX,ColumnIdentifier.START_TRAVERSAL_VERTEX),
                        col(ColumnIdentifier.START_VERTEX).
                                equalTo(col(ColumnIdentifier.START_VERTEX+"s")).
                                and(col(ColumnIdentifier.START_TRAVERSAL_VERTEX).equalTo(col(ColumnIdentifier.START_TRAVERSAL_VERTEX+"s")))).
                drop(ColumnIdentifier.START_VERTEX +"s",ColumnIdentifier.START_TRAVERSAL_VERTEX+"s",ColumnIdentifier.POSITION+"2",
                        ColumnIdentifier.COUNT,ColumnIdentifier.RECORDS_IN).
//                withColumnRenamed(ColumnIdentifier.RECORDS_IN+"2",ColumnIdentifier.RECORDS_IN).
        withColumnRenamed(ColumnIdentifier.PREV_VERTEX+"s",ColumnIdentifier.PREV_VERTEX);

        SparkComputer.persistDataFrameORC(endPoints,endPath);
        startTripel = startTripel.unpersist();
    }

    public static Dataset<Row> reshiftBulgePositions(Dataset<Row> candidates, int k) {
        return candidates
                .withColumn(ColumnIdentifier.POSITION+"5",pmod(col(ColumnIdentifier.POSITION+"5").minus(1),col(ColumnIdentifier.LENGTH)))
                .withColumn(ColumnIdentifier.POSITION+"1",pmod(col(ColumnIdentifier.POSITION+"1").minus(1),col(ColumnIdentifier.LENGTH+"2")))
                .withColumn(ColumnIdentifier.POSITION+"6",pmod(col(ColumnIdentifier.POSITION+"6").minus(k-1),col(ColumnIdentifier.LENGTH)))
                .withColumn(ColumnIdentifier.POSITION+"4",pmod(col(ColumnIdentifier.POSITION+"4").minus(k-1),col(ColumnIdentifier.LENGTH+"2")));
    }

    /*** Evaluation **/

    public static void annotateCollinearBlocks(String path) {
        String i1 = "bp_seq_id";
        String i2 = "compare_seq_id";
        String s1 = "bp_seq_p1";
        String e1 = "bp_seq_p2";
        String s2 = "compare_seq_p1";
        String e2 = "compare_seq_p2";
        String l1 = "bp_seq_length";
        String l2 = "compare_seq_length";

        Dataset<Row> breakpoints = Spark.getInstance().
                read().format("csv") .option("inferSchema", "true").option("sep","\t")
                .option("header", "true").load(path+ "/breakpoints");

        breakpoints = breakpoints
                .withColumn("r1",when(col(i1).lt(col(i2)),col(i1)).otherwise(col(i2)))
                .withColumn(ColumnIdentifier.RECORDS,concat_ws("_",
                when(col(i1).lt(col(i2)),col(i1)).otherwise(col(i2)),
                when(col(i2).lt(col(i1)),col(i2)).otherwise(col(i1))))
                .withColumn("cbs",col(e1))
                .withColumn("cbe",lead(col(s1),1)
                        .over(Window.partitionBy(ColumnIdentifier.RECORDS,i1)
                .orderBy(s1)))
                .withColumn(ColumnIdentifier.RANK,dense_rank().over(Window.partitionBy(ColumnIdentifier.RECORDS,i1).orderBy(s1)))

                        .withColumn("cbs2",col(e2))
                .withColumn("cbe2",lead(col(s2),1)
                        .over(Window.partitionBy(ColumnIdentifier.RECORDS,i1).orderBy(s1)))
                        .orderBy(i1,s1);

        String[] cols = breakpoints.columns();


        Dataset<Row> collinearBlocks = breakpoints.filter(not(isnull(col("cbe")))).union(
                        breakpoints.filter(isnull(col("cbe"))).join(SparkComputer.appendIdentifier(breakpoints.filter(col(ColumnIdentifier.RANK).equalTo(1)),"n"),
                                        col(ColumnIdentifier.RECORDS).equalTo(col(ColumnIdentifier.RECORDS+"n"))
                                                .and(col(i1).equalTo(col(i1+"n")))
                                )
                                .withColumn("cbe",col(s1+"n"))
                                .withColumn("cbe2",col(s2+"n"))
                                .select(SparkComputer.getColumns(cols))
        );

//        SparkComputer.persistDataFrameORC(collinearBlocks.select(i1,i2,l1,l2,"cbs","cbe","cbs2","cbe2"),
//                path+"/COLLINEAR_BLOCKS");

        Dataset<Row> genes1 = collinearBlocks
                .filter(col(i1).equalTo(col("r1")))
                .select(col(i1).as(ColumnIdentifier.RECORD_ID),col(l1).as(ColumnIdentifier.LENGTH),
                        col("cbs").as(ColumnIdentifier.START),col("cbe").as(ColumnIdentifier.END),
                        concat_ws("g",col(ColumnIdentifier.RANK)).as(ColumnIdentifier.PROPERTY))
                .union(collinearBlocks
                        .filter(col(i1).equalTo(col("r1")))
                        .select(col(i2).as(ColumnIdentifier.RECORD_ID),col(l2).as(ColumnIdentifier.LENGTH),
                                col("cbs2").as(ColumnIdentifier.START),col("cbe2").as(ColumnIdentifier.END),
                                concat_ws("g",col(ColumnIdentifier.RANK)).as(ColumnIdentifier.PROPERTY))
                );



        SparkComputer.persistOverwriteDataFrameORC(genes1
                ,
                path+"/GM_r1");
        genes1 = genes1.filter(col(ColumnIdentifier.START).gt(col(ColumnIdentifier.END)) ).withColumn(ColumnIdentifier.END,col(ColumnIdentifier.LENGTH))
                .union(genes1.filter(col(ColumnIdentifier.START).gt(col(ColumnIdentifier.END)) ).withColumn(ColumnIdentifier.START,lit(0))).
                union(genes1.filter(col(ColumnIdentifier.START).leq(col(ColumnIdentifier.END))));


        SparkComputer.persistOverwriteDataFrameORC(genes1
                ,
                path+"/GM_r1_linear");

        Dataset<Row> genes = collinearBlocks
                .filter(col(i1).notEqual(col("r1")))
                .select(col(i1).as(ColumnIdentifier.RECORD_ID),col(l1).as(ColumnIdentifier.LENGTH),
                        col("cbs").as(ColumnIdentifier.START),col("cbe").as(ColumnIdentifier.END),
                        concat_ws("g",col(ColumnIdentifier.RANK)).as(ColumnIdentifier.PROPERTY))
                .union(collinearBlocks
                        .filter(col(i1).notEqual(col("r1")))
                        .select(col(i2).as(ColumnIdentifier.RECORD_ID),col(l2).as(ColumnIdentifier.LENGTH),
                                col("cbs2").as(ColumnIdentifier.START),col("cbe2").as(ColumnIdentifier.END),
                                concat_ws("g",col(ColumnIdentifier.RANK)).as(ColumnIdentifier.PROPERTY))
                );
        SparkComputer.persistOverwriteDataFrameORC(genes

                ,
                path+"/GM_r2");
        genes = genes.filter(col(ColumnIdentifier.START).gt(col(ColumnIdentifier.END)) ).withColumn(ColumnIdentifier.END,col(ColumnIdentifier.LENGTH))
                        .union(genes.filter(col(ColumnIdentifier.START).gt(col(ColumnIdentifier.END)) ).withColumn(ColumnIdentifier.START,lit(0))).
                union(genes.filter(col(ColumnIdentifier.START).leq(col(ColumnIdentifier.END))));


        SparkComputer.persistOverwriteDataFrameORC(genes

                ,
                path+"/GM_r2_linear");
    }

    public static Dataset<Row> createCyclicPropertyRangesWithRecords(Dataset<Row> ranges){
        Dataset<Row> ends  = ranges.filter(
                col(ColumnIdentifier.SEQUENCE_LENGTH).equalTo(col(ColumnIdentifier.END).plus(1))
        );
        Dataset<Row> starts  = ranges.filter(
                col(ColumnIdentifier.START).equalTo(0)
        );


        Dataset<Row> cyclicRanges = ends.join(SparkComputer.appendIdentifier(starts,"2"),
                col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"2"))
                        .and(col(ColumnIdentifier.GENE).equalTo(col(ColumnIdentifier.GENE+"2")))
                        .and(col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+"2")))
        );


        ranges = ranges.join(
                SparkComputer.appendIdentifier(cyclicRanges.select(SparkComputer.getColumns(ranges.columns()))
                        ,"2"),
                col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"2"))
                        .and(col(ColumnIdentifier.PROPERTY).equalTo(col(ColumnIdentifier.PROPERTY+"2")))
                        .and(col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+"2")))
                        .and(col(ColumnIdentifier.START).equalTo(col(ColumnIdentifier.START+"2")))
                        .and(col(ColumnIdentifier.END).equalTo(col(ColumnIdentifier.END+"2")))
                ,
                SparkComputer.JOIN_TYPES.LEFT_ANTI
        );


        ranges = ranges.join(
                cyclicRanges.select(ColumnIdentifier.RECORD_ID+"2",ColumnIdentifier.PROPERTY+"2",ColumnIdentifier.STRAND+"2",ColumnIdentifier.START+"2",ColumnIdentifier.END+"2"),
                col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"2"))
                        .and(col(ColumnIdentifier.PROPERTY).equalTo(col(ColumnIdentifier.PROPERTY+"2")))
                        .and(col(ColumnIdentifier.STRAND).equalTo(col(ColumnIdentifier.STRAND+"2")))
                        .and(col(ColumnIdentifier.START).equalTo(col(ColumnIdentifier.START+"2")))
                        .and(col(ColumnIdentifier.END).equalTo(col(ColumnIdentifier.END+"2")))
                ,
                SparkComputer.JOIN_TYPES.LEFT_ANTI
        );


        return ranges.

                union(
                        cyclicRanges.withColumn(ColumnIdentifier.END,col(ColumnIdentifier.END+"2"))
                                .withColumn(ColumnIdentifier.LENGTH,ColumnFunctions.getCyclicRangeLength(col(ColumnIdentifier.START),col(ColumnIdentifier.END),col(ColumnIdentifier.SEQUENCE_LENGTH)))
                                .select(SparkComputer.getColumns(ranges.columns()))
                );
    }

    public static Dataset<Row> createComputedBreakPointPositionMapping( Dataset<Row> genomeMapping, Dataset<Row> computedBreakpoints, int referenceRecord, Dataset<Row> compareRecords) {
        computedBreakpoints = computedBreakpoints
                .withColumn("reference",when(col(ColumnIdentifier.RECORD_ID).equalTo(referenceRecord),true).otherwise(false))
                .select("reference",ColumnIdentifier.BREAKPOINT,ColumnIdentifier.BREAKPOINT+"2");

        Dataset<Row> result = computedBreakpoints.filter(col("reference")).join(genomeMapping.filter(col(ColumnIdentifier.RECORD_ID).equalTo(referenceRecord)),
                        col(ColumnIdentifier.BREAKPOINT).equalTo(col(ColumnIdentifier.GENE)))
                .select(col(ColumnIdentifier.BREAKPOINT),col(ColumnIdentifier.BREAKPOINT+"2"),
                        col(ColumnIdentifier.START).as(ColumnIdentifier.START+"bp1"),
                        col(ColumnIdentifier.END).as(ColumnIdentifier.END+"bp1"),
                        col(ColumnIdentifier.LENGTH).as(ColumnIdentifier.LENGTH+"bp1"))
                .join(genomeMapping.filter(col(ColumnIdentifier.RECORD_ID).equalTo(referenceRecord)),
                        col(ColumnIdentifier.BREAKPOINT+"2").equalTo(col(ColumnIdentifier.GENE)))
                .select(lit(referenceRecord).as(ColumnIdentifier.RECORD_ID),
                        col(ColumnIdentifier.BREAKPOINT),col(ColumnIdentifier.BREAKPOINT+"2"),
                        col(ColumnIdentifier.START+"bp1"),col(ColumnIdentifier.END+"bp1"),col(ColumnIdentifier.LENGTH+"bp1"),
                        col(ColumnIdentifier.START).as(ColumnIdentifier.START+"bp2"),
                        col(ColumnIdentifier.END).as(ColumnIdentifier.END+"bp2"),
                        col(ColumnIdentifier.LENGTH).as(ColumnIdentifier.LENGTH+"bp2"));

        genomeMapping = genomeMapping.join(compareRecords
                        .withColumnRenamed(ColumnIdentifier.RECORD_ID,"r"),col(ColumnIdentifier.RECORD_ID).equalTo(col("r")),
                SparkComputer.JOIN_TYPES.SEMI);

        return result.union(computedBreakpoints.filter(not(col("reference"))).
                join(genomeMapping,
                        col(ColumnIdentifier.BREAKPOINT).equalTo(col(ColumnIdentifier.GENE)))
                .select(col(ColumnIdentifier.RECORD_ID),
                        col(ColumnIdentifier.BREAKPOINT),col(ColumnIdentifier.BREAKPOINT+"2"),
                        col(ColumnIdentifier.START).as(ColumnIdentifier.START+"bp1"),
                        col(ColumnIdentifier.END).as(ColumnIdentifier.END+"bp1"),
                        col(ColumnIdentifier.LENGTH).as(ColumnIdentifier.LENGTH+"bp1"))
                .join(genomeMapping.withColumnRenamed(ColumnIdentifier.RECORD_ID,"r"),
                        col(ColumnIdentifier.RECORD_ID).equalTo(col("r"))
                                .and(col(ColumnIdentifier.BREAKPOINT+"2").equalTo(col(ColumnIdentifier.GENE))))
                .select(col(ColumnIdentifier.RECORD_ID),
                        col(ColumnIdentifier.BREAKPOINT),col(ColumnIdentifier.BREAKPOINT+"2"),
                        col(ColumnIdentifier.START+"bp1"),col(ColumnIdentifier.END+"bp1"),col(ColumnIdentifier.LENGTH+"bp1"),
                        col(ColumnIdentifier.START).as(ColumnIdentifier.START+"bp2"),
                        col(ColumnIdentifier.END).as(ColumnIdentifier.END+"bp2"),
                        col(ColumnIdentifier.LENGTH).as(ColumnIdentifier.LENGTH+"bp2")));
    }

    public static void measureSynTimesFull(String sequenceDir, String path, int k ) {
        Spark.getInstance();
//        for(String suffix: Arrays.asList("1","2","3","4","5","7_5","10")) {
//            for(int i: Arrays.asList(1,3,5,10)) {
        for(String suffix: Arrays.asList("1","2","3","4","5")) {
            for(int i: Arrays.asList(1,3,5,10)) {

                String dir = path+"/"+suffix+"_" + i +"/";
                Spark.clearDirectory(dir+"/DEBBI");
                System.out.println(suffix + " " + i);
//                Spark.renameDirectory(dir+"/DEBBI/breakpoints",dir+"/DEBBI/breakpoints2");
//                if(Spark.checkExisting(dir+"/DEBBI")) {
//                   continue;
//                }
                File f = new File(dir+"/DEBBI");


                try {
                    StopWatch stopWatch = new StopWatch();
                    stopWatch.start();
                    Dataset<Row> sequences = SparkComputer.readORC(sequenceDir+"/SEQUENCES"+suffix+"_"+i).withColumn(ColumnIdentifier.SEQUENCE_NAME,col(ColumnIdentifier.RECORD_ID));
                    prepareBulgeGraph(sequences,f.getAbsolutePath(),true);
                    identifyBreakPoints(
                            SparkComputer.readORC(f.getAbsolutePath()+"/EDGES_"+k),
                            sequences,
                            SparkComputer.readORC(f.getAbsolutePath()+"/RECORD_DATA"),
                            f.getAbsolutePath()+"/k"+k,
                            f.getAbsolutePath()+"/SCORES",50,k,1,200,1e-5,20,40,true);
                    LOGGER.debug(Spark.threads + ","+suffix + ", "+ i  +"\n");
                    LOGGER.debug("Complete run time: " + TimeDT.asMinutes(stopWatch.getTime()));
//                    FileIO.writeToFile(Spark.threads + ","+suffix + ","+ i +", " +","+TimeDT.asMinutes(stopWatch.getTime())+"\n",path+"/DebbiTimesFull2.csv",true,"");
                    stopWatch.reset();

                } catch (IOException e) {
                    e.printStackTrace();
                }


            }

        }
    }

    public static void measureSynTimesFullInversion(String path, int k ) {
        Spark.getInstance();
        for(String suffix: Arrays.asList("1","2","3","4","5","7_5","10")) {

            String dir = path+"/"+suffix+"/";
            Spark.clearDirectory(dir+"/k10");
            Spark.clearDirectory(dir+"/EDGES_10");
            Spark.clearDirectory(dir+"/RECORD_DATA");
            try {
                StopWatch stopWatch = new StopWatch();
                stopWatch.start();
                Dataset<Row> sequences = SparkComputer.readORC(dir+"/SEQUENCES");
                prepareInverseBulgeGraphWithoutEvalueScores(sequences,k,dir);
                LOGGER.debug("prep time: " + TimeDT.asMinutes(stopWatch.getTime()));
                stopWatch.reset();
                stopWatch.start();

                identifyInversionBreakPoints(SparkComputer.readORC(dir +"/EDGES_"+k),
                        sequences,
                        SparkComputer.readORC(dir+"/RECORD_DATA"),
                        1,300,15,3000,0.8,25,k,
                        dir+"/k_"+k,true
                );
                LOGGER.debug(Spark.threads + ","+suffix  +","+TimeDT.asMinutes(stopWatch.getTime())+"\n");
                LOGGER.debug("Complete run time: " + TimeDT.asMinutes(stopWatch.getTime()));
                FileIO.writeToFile(Spark.threads + ","+suffix  +","+TimeDT.asMinutes(stopWatch.getTime())+"\n",path+"/DebbiInversionTimesFull.csv",true,"");
                stopWatch.reset();

            } catch (IOException e) {
                e.printStackTrace();
            }



        }
    }

    public static void evaluateSynFull(String genomeMappingdir, String dir) {
        Spark.getInstance();
//        for(String suffix: Arrays.asList("1","2","3","4","5","7_5","10")) {
//
//            for(int i: Arrays.asList(1,3,5,10)) {
        for(String suffix: Arrays.asList("1","2","3","4","5")) {

            for(int i: Arrays.asList(1,3,5,10)) {
                System.out.println(suffix+" "+ i);
//                if(Spark.checkExisting(dir+"/"+suffix+"_"+ i+"/DEBBI"+"/CLUSTERED_ANNOTATED_50_2")) {
//                    System.out.println("exists");
//                    continue;
//                }

                Dataset<Row> ca = evaluateSyn(suffix,i,dir,SparkComputer.readORC(genomeMappingdir+"/GENOME_MAPPING"+suffix+"_"+i));
                System.out.println(ca.withColumn("d",greatest(col("dR1l"),col("dR1r"))).filter(col("d").leq(10))
                        .select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"2", ColumnIdentifier.BREAKPOINT,ColumnIdentifier.BREAKPOINT+"2").distinct().count());
                System.out.println(ca.withColumn("d",greatest(col("dR1l"),col("dR1r"))).filter(col("d").leq(20))
                        .select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"2", ColumnIdentifier.BREAKPOINT,ColumnIdentifier.BREAKPOINT+"2").distinct().count());
                System.out.println(ca.withColumn("d",greatest(col("dR1l"),col("dR1r"))).filter(col("d").leq(30))
                        .select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"2", ColumnIdentifier.BREAKPOINT,ColumnIdentifier.BREAKPOINT+"2").distinct().count());
                System.out.println(ca.withColumn("d",greatest(col("dR1l"),col("dR1r"))).filter(col("d").leq(40))
                        .select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"2", ColumnIdentifier.BREAKPOINT,ColumnIdentifier.BREAKPOINT+"2").distinct().count());
                System.out.println(ca.withColumn("d",greatest(col("dR1l"),col("dR1r"))).filter(col("d").leq(50))
                        .select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"2", ColumnIdentifier.BREAKPOINT,ColumnIdentifier.BREAKPOINT+"2").distinct().count());
//                System.out.println(ca.withColumn("d",greatest(col("dR1l"),col("dR1r"))).filter(col("d").leq(75))
//                        .select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"2", ColumnIdentifier.BREAKPOINT,ColumnIdentifier.BREAKPOINT+"2").distinct().count());
//                System.out.println(ca.withColumn("d",greatest(col("dR1l"),col("dR1r"))).filter(col("d").leq(100))
//                        .select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"2", ColumnIdentifier.BREAKPOINT,ColumnIdentifier.BREAKPOINT+"2").distinct().count());
//                System.out.println(ca.withColumn("d",greatest(col("dR1l"),col("dR1r"))).filter(col("d").leq(120))
//                        .select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"2", ColumnIdentifier.BREAKPOINT,ColumnIdentifier.BREAKPOINT+"2").distinct().count());
//
//                Dataset<Row> ca = evaluateSyn(10,suffix,i,dir,SparkComputer.readORC(genomeMappingdir+"/GENOME_MAPPING"+suffix+"_"+i));
////                Dataset<Row> ca = SparkComputer.readORC(dir+"/"+suffix+"_"+ i+"/DEBBI/k"+k+"/CLUSTERED_ANNOTATED");
//                SparkComputer.persistDataFrameORCAppend(ca.select(
//                        ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"2",
//                                ColumnIdentifier.SEQUENCE_LENGTH,ColumnIdentifier.SEQUENCE_LENGTH+"2",
//                                ColumnIdentifier.POSITION+"5",ColumnIdentifier.POSITION+"6",ColumnIdentifier.POSITION+"1",ColumnIdentifier.POSITION+"4",
//                                ColumnIdentifier.BREAKPOINT,ColumnIdentifier.BREAKPOINT+"2","dR1l","dR1r","dR2l","dR2r",ColumnIdentifier.RANK
//                                ).withColumn(ColumnIdentifier.SUBSTITUTION_RATE,lit(suffix)).withColumn(ColumnIdentifier.SHUFFLE_AMOUNT,lit(i)),
//                        dir+"/CLUSTERED_ANNOTATED_50_NEW");

                SparkComputer.persistDataFrameORCAppend(ca.select(
                                ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"2",
                                ColumnIdentifier.SEQUENCE_LENGTH,ColumnIdentifier.SEQUENCE_LENGTH+"2",
                                ColumnIdentifier.POSITION+"5",ColumnIdentifier.POSITION+"6",ColumnIdentifier.POSITION+"1",ColumnIdentifier.POSITION+"4",
                                ColumnIdentifier.BREAKPOINT,ColumnIdentifier.BREAKPOINT+"2","dR1l","dR1r",ColumnIdentifier.RANK
                        ).withColumn(ColumnIdentifier.SUBSTITUTION_RATE,lit(suffix)).withColumn(ColumnIdentifier.SHUFFLE_AMOUNT,lit(i)),
                        dir+"/CLUSTERED_ANNOTATED_NEW");
            }

        }
    }

    public static void evaluate(String dir, Dataset<Row> genomeMapping,Dataset<Row> sequences){

        evaluate(sequences.select(ColumnIdentifier.RECORD_ID).as(Encoders.INT()).first(),dir,genomeMapping,sequences);
    }

    public static void evaluate(int referenceRecord, String dir, Dataset<Row> genomeMapping,Dataset<Row> sequences){

        Dataset<Row> breakpoints = Spark.getInstance().read().format("csv").option("inferSchema","true").
                option("sep","\t").option("header","true").
                load(dir+"/breakpoints")
                .withColumnRenamed("bp_seq_id",ColumnIdentifier.RECORD_ID)
                .withColumnRenamed("compare_seq_id",ColumnIdentifier.RECORD_ID+"2")
                .withColumnRenamed("bp_seq_p1",ColumnIdentifier.POSITION+"5")
                .withColumnRenamed("bp_seq_p2",ColumnIdentifier.POSITION+"6")
                .withColumnRenamed("compare_seq_p1",ColumnIdentifier.POSITION+"1")
                .withColumnRenamed("compare_seq_p2",ColumnIdentifier.POSITION+"4")
                .withColumnRenamed("bp_seq_length",ColumnIdentifier.SEQUENCE_LENGTH)
                .withColumnRenamed("compare_seq_length",ColumnIdentifier.SEQUENCE_LENGTH+"2")
                ;

        if(breakpoints.count() == 0) {
            System.out.println("no breakpoints found");
            return;
        }
        Dataset<Row> records = sequences.select(ColumnIdentifier.RECORD_ID).distinct();


        genomeMapping = genomeMapping.join(records.withColumnRenamed(ColumnIdentifier.RECORD_ID,"r"),
                col("r").equalTo(col(ColumnIdentifier.RECORD_ID)),SparkComputer.JOIN_TYPES.SEMI);

        genomeMapping = createCyclicPropertyRangesWithRecords(genomeMapping);


        Dataset<Row> computedBreakpoints = extractBreakPointsPerGeneGroupSynthetic(genomeMapping);

        computedBreakpoints = createComputedBreakPointPositionMapping(genomeMapping,computedBreakpoints,
                referenceRecord,records.filter(col(ColumnIdentifier.RECORD_ID).notEqual(referenceRecord)));


        System.out.println(computedBreakpoints.count());
        SparkComputer.persistDataFrameORC(computedBreakpoints,dir+"/COMPUTED_BREAKPOINTS");
        //        String dir = "/home/lisa/Documents/MITOS_Project/MITOS_gdb/project/graphData/SELECTION/ARTROPODS/31_198_E5/16173_16202/k10_E6";







        SparkComputer.persistOverwriteDataFrameORC(breakpoints
                        .withColumnRenamed(ColumnIdentifier.RECORD_ID,"r")
//                .withColumnRenamed(ColumnIdentifier.RECORD_ID+"2","r2")
                        .join(computedBreakpoints,
                                col(ColumnIdentifier.RECORD_ID).equalTo(col("r")))
                        .withColumn("dR1l",
                                ColumnFunctions.computeSmallestDistance(
                                        col(ColumnIdentifier.POSITION+"5"),
                                        col("endbp1"),
                                        col(ColumnIdentifier.SEQUENCE_LENGTH),lit(true),lit(true)
                                ))
                        .withColumn("dR1r",
                                ColumnFunctions.computeSmallestDistance(
                                        col(ColumnIdentifier.POSITION+"6"),
                                        col("startbp2"),
                                        col(ColumnIdentifier.SEQUENCE_LENGTH),lit(true),lit(true)
                                ))
                        .withColumn("d",array_sort(array(abs(col("dR1l")),abs(col("dR1r")))))
                        .withColumn(ColumnIdentifier.RANK,dense_rank().over(Window
                                .partitionBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"2",
                                        ColumnIdentifier.POSITION+"5",ColumnIdentifier.POSITION+"6",ColumnIdentifier.POSITION+"1",ColumnIdentifier.POSITION+"4")
                                .orderBy(
                                        element_at(col("d"),2),element_at(col("d"),1)))).
//                        .withColumn("d",array_sort(array(abs(col("dR1l")),abs(col("dR1r")),
//                                abs(col("dR2l")),abs(col("dR2r")))))
//                        .withColumn(ColumnIdentifier.RANK,dense_rank().over(Window
//                                .partitionBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"2",
//                                        ColumnIdentifier.POSITION+"5",ColumnIdentifier.POSITION+"6",ColumnIdentifier.POSITION+"1",ColumnIdentifier.POSITION+"4")
//                                .orderBy(
//                                        element_at(col("d"),4),element_at(col("d"),3),
//                                        element_at(col("d"),2),element_at(col("d"),1)))).
        filter(col(ColumnIdentifier.RANK).leq(5))
//                        .withColumn("d",array_sort(array(abs(col("dR1l")),abs(col("dR1r"))
////                        abs(col("dR2l")),abs(col("dR2r")))
//                                )
//                        ))
//                        .withColumn(ColumnIdentifier.RANK,dense_rank().over(Window
//                                .partitionBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"2",
//                                        ColumnIdentifier.POSITION+"5",ColumnIdentifier.POSITION+"6",ColumnIdentifier.POSITION+"1",ColumnIdentifier.POSITION+"4")
//                                .orderBy(
//                                        element_at(col("d"),2),element_at(col("d"),1),
//                                        element_at(col("d"),3),element_at(col("d"),4)))).
//                filter(col(ColumnIdentifier.RANK).leq(5))
//                        filter(col(ColumnIdentifier.RANK).equalTo(1)).drop(ColumnIdentifier.RANK)
//                .withColumn("dR2l",
//                        ColumnFunctions.computeSmallestDistance(
//                                col(ColumnIdentifier.POSITION+"1"),
//                                col("endbp1R2"),
//                                col(ColumnIdentifier.SEQUENCE_LENGTH+"2"),lit(true),lit(true)
//                        ))
//                .withColumn("dR2r",
//                        ColumnFunctions.computeSmallestDistance(
//                                col(ColumnIdentifier.POSITION+"4"),
//                                col("startbp2R2"),
//                                col(ColumnIdentifier.SEQUENCE_LENGTH+"2"),lit(true),lit(true)
//                        ))
                        .orderBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.POSITION+"5",ColumnIdentifier.RANK),
                dir+"/CLUSTERED_ANNOTATED"
        );


        SparkComputer.readORC(dir+"/CLUSTERED_ANNOTATED").orderBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.POSITION+"5",ColumnIdentifier.RANK).show(500);

        SparkComputer.readORC(dir+"/CLUSTERED_ANNOTATED").filter(col(ColumnIdentifier.RANK).equalTo(1)).orderBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.BREAKPOINT).show(500);
    }
    public static Dataset<Row> evaluateSyn(String substitionRate, int shuffleAmount, String dir, Dataset<Row> genomeMapping){
        File f = new File(dir);

        dir = f.getAbsolutePath()+"/"+substitionRate+"_"+ shuffleAmount+"/DEBBI";

//        Spark.renameDirectory(dir+"/CLUSTERED",dir+"/CLUSTERED_70");
//        Spark.renameDirectory(dir+"/breakpoints",dir+"/breakpoints_70");
//        clusterData(dir,50);

        genomeMapping = createCyclicPropertyRangesWithRecords(genomeMapping);

//

        Dataset<Row> computedBreakpoints = extractBreakPointsPerGeneGroupSynthetic(genomeMapping);

        computedBreakpoints =  computedBreakpoints.
                join(SparkComputer.appendIdentifier(genomeMapping,"bp1"),
                        col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"bp1")).
                                and(col(ColumnIdentifier.GENE+"bp1").equalTo(col(ColumnIdentifier.BREAKPOINT)))).
                drop(ColumnIdentifier.GENE+"bp1",ColumnIdentifier.STRAND+"bp1",ColumnIdentifier.SEQUENCE_LENGTH+"bp1",
                        ColumnIdentifier.PROPERTY+"bp1",ColumnIdentifier.RECORD_ID+"bp1",ColumnIdentifier.CATEGORY+"bp2").
                join(SparkComputer.appendIdentifier(genomeMapping,"bp2"),
                        col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"bp2")).
                                and(col(ColumnIdentifier.GENE+"bp2").equalTo(col(ColumnIdentifier.BREAKPOINT+"2")))).
                drop(ColumnIdentifier.GENE+"bp2",ColumnIdentifier.STRAND+"bp2",
                        ColumnIdentifier.PROPERTY+"bp2",ColumnIdentifier.RECORD_ID+"bp2",ColumnIdentifier.CATEGORY+"bp2").

                join(SparkComputer.appendIdentifier(genomeMapping,"bp1R2"),
                        col(ColumnIdentifier.RECORD_ID+"2").equalTo(col(ColumnIdentifier.RECORD_ID+"bp1R2")).
                                and(col(ColumnIdentifier.GENE+"bp1R2").equalTo(col(ColumnIdentifier.BREAKPOINT)))).
                drop(ColumnIdentifier.GENE+"bp1R2",ColumnIdentifier.STRAND+"bp1R2",ColumnIdentifier.SEQUENCE_LENGTH+"bp1R2",
                        ColumnIdentifier.PROPERTY+"bp1R2",ColumnIdentifier.RECORD_ID+"bp1R2",ColumnIdentifier.CATEGORY+"bp1R2").
                join(SparkComputer.appendIdentifier(genomeMapping,"bp2R2"),
                        col(ColumnIdentifier.RECORD_ID+"2").equalTo(col(ColumnIdentifier.RECORD_ID+"bp2R2")).
                                and(col(ColumnIdentifier.GENE+"bp2R2").equalTo(col(ColumnIdentifier.BREAKPOINT+"2")))).
                drop(ColumnIdentifier.GENE+"bp2R2",ColumnIdentifier.STRAND+"bp2R2",
                        ColumnIdentifier.PROPERTY+"bp2R2",ColumnIdentifier.RECORD_ID+"bp2R2",ColumnIdentifier.CATEGORY+"bp2R2");



        Dataset<Row> breakpoints = Spark.getInstance().read().format("csv").option("inferSchema","true").
                option("sep","\t").option("header","true").
                load(dir+"/breakpoints")
                .withColumnRenamed("bp_seq_id",ColumnIdentifier.RECORD_ID)
                .withColumnRenamed("compare_seq_id",ColumnIdentifier.RECORD_ID+"2")
                .withColumnRenamed("bp_seq_p1",ColumnIdentifier.POSITION+"5")
                .withColumnRenamed("bp_seq_p2",ColumnIdentifier.POSITION+"6")
                .withColumnRenamed("compare_seq_p1",ColumnIdentifier.POSITION+"1")
                .withColumnRenamed("compare_seq_p2",ColumnIdentifier.POSITION+"4")
                .withColumnRenamed("bp_seq_length",ColumnIdentifier.SEQUENCE_LENGTH)
                .withColumnRenamed("compare_seq_length",ColumnIdentifier.SEQUENCE_LENGTH+"2")
                ;
        SparkComputer.persistOverwriteDataFrameORC(
                computedBreakpoints.join(breakpoints
                                        .withColumnRenamed(ColumnIdentifier.RECORD_ID,"rec")
                                        .withColumnRenamed(ColumnIdentifier.RECORD_ID+"2","rec2")
                                ,
                                col(ColumnIdentifier.RECORD_ID).equalTo(col("rec")).and(col(ColumnIdentifier.RECORD_ID+"2").equalTo(col("rec2")))
                        )
                        .withColumn("dR1l",
                                ColumnFunctions.computeSmallestDistance(
                                        col(ColumnIdentifier.POSITION+"5"),
                                        col("endbp1"),
                                        col(ColumnIdentifier.SEQUENCE_LENGTH),lit(true),lit(true)
                                ))
                        .withColumn("dR1r",
                                ColumnFunctions.computeSmallestDistance(
                                        col(ColumnIdentifier.POSITION+"6"),
                                        col("startbp2"),
                                        col(ColumnIdentifier.SEQUENCE_LENGTH),lit(true),lit(true)
                                ))
                        .withColumn("d",array_sort(array(abs(col("dR1l")),abs(col("dR1r")))))
                        .withColumn(ColumnIdentifier.RANK,dense_rank().over(Window
                                .partitionBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"2",
                                        ColumnIdentifier.POSITION+"5",ColumnIdentifier.POSITION+"6",ColumnIdentifier.POSITION+"1",ColumnIdentifier.POSITION+"4")
                                .orderBy(
                                        element_at(col("d"),2),element_at(col("d"),1)))).
//                        .withColumn("d",array_sort(array(abs(col("dR1l")),abs(col("dR1r")),
//                                abs(col("dR2l")),abs(col("dR2r")))))
//                        .withColumn(ColumnIdentifier.RANK,dense_rank().over(Window
//                                .partitionBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"2",
//                                        ColumnIdentifier.POSITION+"5",ColumnIdentifier.POSITION+"6",ColumnIdentifier.POSITION+"1",ColumnIdentifier.POSITION+"4")
//                                .orderBy(
//                                        element_at(col("d"),4),element_at(col("d"),3),
//                                        element_at(col("d"),2),element_at(col("d"),1)))).
        filter(col(ColumnIdentifier.RANK).leq(20))
//                        .withColumn("d",array_sort(array(abs(col("dR1l")),abs(col("dR1r"))
////                        abs(col("dR2l")),abs(col("dR2r")))
//                                )
//                        ))
//                        .withColumn(ColumnIdentifier.RANK,dense_rank().over(Window
//                                .partitionBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"2",
//                                        ColumnIdentifier.POSITION+"5",ColumnIdentifier.POSITION+"6",ColumnIdentifier.POSITION+"1",ColumnIdentifier.POSITION+"4")
//                                .orderBy(
//                                        element_at(col("d"),2),element_at(col("d"),1),
//                                        element_at(col("d"),3),element_at(col("d"),4)))).
//                filter(col(ColumnIdentifier.RANK).leq(5))
//                        filter(col(ColumnIdentifier.RANK).equalTo(1)).drop(ColumnIdentifier.RANK)
//                .withColumn("dR2l",
//                        ColumnFunctions.computeSmallestDistance(
//                                col(ColumnIdentifier.POSITION+"1"),
//                                col("endbp1R2"),
//                                col(ColumnIdentifier.SEQUENCE_LENGTH+"2"),lit(true),lit(true)
//                        ))
//                .withColumn("dR2r",
//                        ColumnFunctions.computeSmallestDistance(
//                                col(ColumnIdentifier.POSITION+"4"),
//                                col("startbp2R2"),
//                                col(ColumnIdentifier.SEQUENCE_LENGTH+"2"),lit(true),lit(true)
//                        ))
                        .orderBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.POSITION+"5",ColumnIdentifier.RANK),
                dir+"/CLUSTERED_ANNOTATED"
        );


        return SparkComputer.readORC(dir+"/CLUSTERED_ANNOTATED");
    }

    /*** Prep ***/
    public static void prepareBulgeGraphWithEvalueStat(File fastafile,  String path, boolean cyclic, int k ) throws IOException {

        prepareBulgeGraph(fastafile,k,path,cyclic);
        computeEvalueParameter(path);
//        int open= -2;
//        int extend = -2;
//        short match = 1;
//        short mismatch = -2;
//
//        String eValueDir = path+"/SCORES";
//        try {
//            getShuffledScores(sequences,1000,20,eValueDir, Alignments.PairwiseSequenceAlignerType.LOCAL,open,extend,match,mismatch);
//        } catch (CompoundNotFoundException ex) {
//            ex.printStackTrace();
//        }
//
//        SparkComputer.writeToCSV(SparkComputer.readORC(eValueDir+"/local_SCORES_o-2_e-2_match1_missmatch-2_l1000"),
//                eValueDir+"/local_SCORES_o-2_e-2_match1_missmatch-2_l1000/CSV");
//
//        String runCmnd = " Rscript --vanilla "+ProjectDirectoryManager.getSCRIPTS_DIR()+"/computeEvalueParam.R "+ eValueDir+"/local_SCORES_o-2_e-2_match1_missmatch-2_l1000/CSV";
////        String runCmnd = " Rscript --vanilla "+ProjectDirectoryManager.getSCRIPTS_DIR()+"/computeEvalueParam.R /home/lisa/Documents";
////        String runCmnd = " Rscript --vanilla computeEvalueParam.R /home/lisa/Documents";
//        try {
//            Process process = Runtime.getRuntime().exec(runCmnd);
//            Auxiliary.printResults(process);
//            process.waitFor();
//            if(process.exitValue() != 0) {
//                System.exit(-1);
//            }
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        } catch (InterruptedException ex) {
//            ex.printStackTrace();
//        }
//
//        Path p = Paths.get(eValueDir+"/local_SCORES_o-2_e-2_match1_missmatch-2_l1000.csv");
//        Files.move(p,p.resolveSibling("local_parameters_o-2_e-2_match1_missmatch-2"));
    }

    public static void prepareBulgeGraphWithEvalueStat(File fastafile, String path, boolean cyclic) throws IOException {

        prepareBulgeGraph(fastafile,path,cyclic);
       computeEvalueParameter(path);
    }

    public static void prepareBulgeGraph(Dataset<Row> sequences,  int k, String path, boolean cyclic) throws IOException {
        SparkComputer.persistDataFrameORC(de.uni_leipzig.informatik.pacosy.mitos.core.graphs.Graph.createEdgesFromSequences(sequences, k,true,true).withColumn(ColumnIdentifier.STRAND, lit(true)),
                Arrays.asList("src", "dst", "recordId", ColumnIdentifier.POSITION), path + "/EDGES_" + k);

//        Dataset<Row> e = Graph.createEdgesFromSequences(sequences,k).withColumn(ColumnIdentifier.STRAND,lit(true));
//        SparkComputer.persistDataFrameORC(e, Arrays.asList("src","dst","recordId",ColumnIdentifier.POSITION),path+"/EDGES_"+k);

        if(!Spark.checkExisting(path+"/RECORD_DATA")){
            SparkComputer.persistDataFrameORC(sequences.select(col("*"),
                                    length(col(ColumnIdentifier.SEQUENCE)).as(ColumnIdentifier.LENGTH) )
                            .withColumn(ColumnIdentifier.TOPOLOGY,lit(cyclic))
                    ,

                    path+"/RECORD_DATA" );
        }
        if(!Spark.checkExisting(path+"/SEQUENCES")){
            SparkComputer.persistDataFrameORC(sequences,path+"/SEQUENCES");
        }


    }

    public static void prepareBulgeGraph(File fastafile,   int k, String path, boolean cyclic) throws IOException {
        Dataset<Row> sequences= readFastaFile(fastafile.getAbsolutePath());
        SparkComputer.persistDataFrameORC(de.uni_leipzig.informatik.pacosy.mitos.core.graphs.Graph.createEdgesFromSequences(sequences, k,true,true).withColumn(ColumnIdentifier.STRAND, lit(true)),
                Arrays.asList("src", "dst", "recordId", ColumnIdentifier.POSITION), path + "/EDGES_" + k);

//        Dataset<Row> e = Graph.createEdgesFromSequences(sequences,k).withColumn(ColumnIdentifier.STRAND,lit(true));
//        SparkComputer.persistDataFrameORC(e, Arrays.asList("src","dst","recordId",ColumnIdentifier.POSITION),path+"/EDGES_"+k);

        if(!Spark.checkExisting(path+"/RECORD_DATA")){
            SparkComputer.persistDataFrameORC(sequences.select(col("*"),
                                    length(col(ColumnIdentifier.SEQUENCE)).as(ColumnIdentifier.LENGTH) )
                            .withColumn(ColumnIdentifier.TOPOLOGY,lit(cyclic))
                    ,

                    path+"/RECORD_DATA" );
        }
        if(!Spark.checkExisting(path+"/SEQUENCES")){
            SparkComputer.persistDataFrameORC(sequences,path+"/SEQUENCES");
        }


    }
    public static void prepareBulgeGraph(Dataset<Row> sequences,   String path, boolean cyclic) throws IOException {
        int k = 8;
        Dataset<Row> edges;
        do {
            k+= 2;
            edges  = de.uni_leipzig.informatik.pacosy.mitos.core.graphs.Graph.createEdgesFromSequences(sequences, k,true,true).withColumn(ColumnIdentifier.STRAND, lit(true));
            LOGGER.debug(k+"");
        } while(checkDistinctCountRate(edges,0.85) );



        SparkComputer.persistDataFrameORC(edges,
                Arrays.asList("src", "dst", "recordId", ColumnIdentifier.POSITION)
                , path + "/EDGES_" + k);
//        Dataset<Row> e = Graph.createEdgesFromSequences(sequences,k).withColumn(ColumnIdentifier.STRAND,lit(true));
//        SparkComputer.persistDataFrameORC(e, Arrays.asList("src","dst","recordId",ColumnIdentifier.POSITION),path+"/EDGES_"+k);

        if(!Spark.checkExisting(path+"/RECORD_DATA")){
            SparkComputer.persistDataFrameORC(sequences.select(col("*"),
                                    length(col(ColumnIdentifier.SEQUENCE)).as(ColumnIdentifier.LENGTH) )
                            .withColumn(ColumnIdentifier.TOPOLOGY,lit(cyclic))
                    ,

                    path+"/RECORD_DATA" );
        }
        if(!Spark.checkExisting(path+"/SEQUENCES")){
            SparkComputer.persistDataFrameORC(sequences,path+"/SEQUENCES");
        }


    }


    public static void prepareBulgeGraph(File fastafile,  String path, boolean cyclic) throws IOException {



        Dataset<Row> sequenceDataset= readFastaFile(fastafile.getAbsolutePath());
        prepareBulgeGraph(sequenceDataset,path,cyclic);
    }


    public static void computeEvalueParameter(String path) throws IOException {
        int open= -2;
        int extend = -2;
        short match = 1;
        short mismatch = -2;

        String eValueDir = path+"/SCORES";

        try {
            getShuffledScores(SparkComputer.readORC(path+"/SEQUENCES"),1000,2,
                    eValueDir, Alignments.PairwiseSequenceAlignerType.LOCAL,open,extend,match,mismatch);
        } catch (CompoundNotFoundException ex) {
            ex.printStackTrace();
        }


        SparkComputer.writeToCSV(SparkComputer.readORC(eValueDir+"/local_SCORES_o-2_e-2_match1_missmatch-2_l1000"),
                eValueDir+"/local_SCORES_o-2_e-2_match1_missmatch-2_l1000/CSV");

        String runCmnd = " Rscript --vanilla "+ProjectDirectoryManager.getSCRIPTS_DIR()+"/computeEvalueParam.R "+ eValueDir+"/local_SCORES_o-2_e-2_match1_missmatch-2_l1000/CSV";
//        String runCmnd = " Rscript --vanilla "+ProjectDirectoryManager.getSCRIPTS_DIR()+"/computeEvalueParam.R /home/lisa/Documents";
//        String runCmnd = " Rscript --vanilla computeEvalueParam.R /home/lisa/Documents";
        try {
            Process process = Runtime.getRuntime().exec(runCmnd);
            Auxiliary.printResults(process);
            process.waitFor();
            if(process.exitValue() != 0) {
                System.exit(-1);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        Path p = Paths.get(eValueDir+"/local_SCORES_o-2_e-2_match1_missmatch-2_l1000.csv");
        Files.move(p,p.resolveSibling("local_parameters_o-2_e-2_match1_missmatch-2"));
    }


    /**
     * check if the ratio of distinct edges to all edges is smaller than supplied fraction
     * @param edges
     * @param fraction
     * @return true if ratio is smaller, false otherwise
     */
    public static boolean checkDistinctCountRate(Dataset<Row> edges, double fraction){
        long count  = edges.groupBy(ColumnIdentifier.RECORD_ID)
                    .agg(countDistinct(ColumnIdentifier.SRC).as(ColumnIdentifier.DISTINCT_KMER_COUNT),
                            count(col(ColumnIdentifier.SRC)).as(ColumnIdentifier.COUNT))
                    .withColumn(ColumnIdentifier.RATIO,col(ColumnIdentifier.DISTINCT_KMER_COUNT).divide(col(ColumnIdentifier.COUNT))).
                    filter(col(ColumnIdentifier.RATIO).lt(fraction)).count();
        return count > 0;
    }
    /*** Inversions ***/
    public static void prepareInverseBulgeGraphWithoutEvalueScores(Dataset<Row> sequences,   String path) throws IOException {
        int k = 8;
        Dataset<Row> edges;
        do {
            k+= 2;
            edges  = de.uni_leipzig.informatik.pacosy.mitos.core.graphs.Graph.createEdgesFromSequences(sequences, k,true,true).withColumn(ColumnIdentifier.STRAND, lit(true));
            LOGGER.debug(k+"");
//            System.out.println(k);
        } while(checkDistinctCountRate(edges,0.85) );




        SparkComputer.persistDataFrameORC(edges.withColumn(ColumnIdentifier.STRAND, lit(true)).
                        union(de.uni_leipzig.informatik.pacosy.mitos.core.graphs.Graph.createEdgesFromSequences(sequences, k,false,true).withColumn(ColumnIdentifier.STRAND, lit(false))),
                Arrays.asList("src", "dst", "recordId", ColumnIdentifier.POSITION), path + "/INV_EDGES_" + k);


        if(!Spark.checkExisting(path+"/RECORD_DATA" )) {
            SparkComputer.persistDataFrameORC(sequences.select(col("*"),
                                    length(col(ColumnIdentifier.SEQUENCE)).as(ColumnIdentifier.LENGTH) )
                            .withColumn(ColumnIdentifier.TOPOLOGY,lit(true))
                    ,

                    path+"/RECORD_DATA" );
        }


    }

    public static void prepareInverseBulgeGraphWithoutEvalueScores(Dataset<Row> sequences,  int k, String path) throws IOException {
        SparkComputer.persistDataFrameORC(de.uni_leipzig.informatik.pacosy.mitos.core.graphs.Graph.createEdgesFromSequences(sequences, k,true,true).withColumn(ColumnIdentifier.STRAND, lit(true)).
                        union(de.uni_leipzig.informatik.pacosy.mitos.core.graphs.Graph.createEdgesFromSequences(sequences, k,false,true).withColumn(ColumnIdentifier.STRAND, lit(false))),
                Arrays.asList("src", "dst", "recordId", ColumnIdentifier.POSITION), path + "/INV_EDGES_" + k);
//        Dataset<Row> e = Graph.createEdgesFromSequences(sequences, k).withColumn(ColumnIdentifier.STRAND, lit(true));
//        e = e.union(Graph.computeInverseStrandEdges(e, sequences.withColumn(ColumnIdentifier.LENGTH, length(col(ColumnIdentifier.SEQUENCE)))));
        SparkComputer.persistDataFrameORC(sequences.select(col("*"),
                                length(col(ColumnIdentifier.SEQUENCE)).as(ColumnIdentifier.LENGTH) )
                        .withColumn(ColumnIdentifier.TOPOLOGY,lit(true))
                ,

                path+"/RECORD_DATA" );

    }

    public static void prepareInverseBulgeGraphWithoutEvalueScores(File fastafile, String path) throws IOException {
        Dataset<Row> sequenceDataset= null;
        if(!Spark.checkExisting(path+"/SEQUENCES")) {
            sequenceDataset= readFastaFile(fastafile.getAbsolutePath());
            SparkComputer.persistDataFrameORC(sequenceDataset,path+"/SEQUENCES");
        }
        else {
            sequenceDataset = SparkComputer.readORC(path+"/SEQUENCES");
        }



        prepareInverseBulgeGraphWithoutEvalueScores(sequenceDataset,path);
    }


    public static Dataset<Row> readFastaFile2(String fastafile) throws IOException {
        List<Row> rows = new ArrayList<>();
        LinkedHashMap<String, DNASequence> sequences = null;
        StructType FASTA_STRUCT = new StructType(new StructField[]{
                new StructField(ColumnIdentifier.SEQUENCE_NAME, StringType,true, Metadata.empty()),
                new StructField(ColumnIdentifier.SEQUENCE, StringType,true, Metadata.empty()),
                new StructField(ColumnIdentifier.RECORD_ID, IntegerType,true, Metadata.empty()),
        });

        int counter = 1;
        try {
            sequences = FastaReaderHelper.readFastaDNASequence(new File(fastafile));
        } catch (IOException e) {
            e.printStackTrace();
        }
        for(Entry<String,DNASequence> entry: sequences.entrySet()) {
            rows.add(RowFactory.create(entry.getKey(),entry.getValue().getSequenceAsString(),counter));
            counter++;
//            System.out.println(entry.getKey());
//            System.out.println(entry.getValue().getSequenceAsString());
//            System.out.println("");
        }

        if(counter<= 1) {
            LOGGER.error("At least two sequences must be supplied");
            System.exit(-1);
        }

        Dataset<Row> sequenceDataset = Spark.getInstance().createDataFrame(rows,FASTA_STRUCT);

        return sequenceDataset;

    }

    public static Dataset<Row> readFastaFile(String fastafile) throws IOException {
//        Pattern fastaHeaderPattern = Pattern.compile("^>\\p{Blank}?([^\\p{Blank}]+)");
        Pattern fastaHeaderPattern = Pattern.compile("^>\\p{Blank}?([^\\p{Blank}]+).*$");

        List<Row> rows = new ArrayList<>();

        if(!new File(fastafile).exists()) {
            LOGGER.warn("File " + fastafile + " does not exist");
            return null;
        }
        BufferedReader br = new BufferedReader(new FileReader(fastafile));
        String line;
        StringBuilder sequence = new StringBuilder();
        Matcher fastaHeaderMatcher;
        StructType FASTA_STRUCT = new StructType(new StructField[]{
                new StructField(ColumnIdentifier.SEQUENCE_NAME, StringType,true, Metadata.empty()),
                new StructField(ColumnIdentifier.SEQUENCE, StringType,true, Metadata.empty()),
                new StructField(ColumnIdentifier.RECORD_ID, IntegerType,true, Metadata.empty()),
        });
        int lineCounter = 0;
        String recordName="";

        while((line = br.readLine()) != null)  {


            fastaHeaderMatcher = fastaHeaderPattern.matcher(line);
            if(fastaHeaderMatcher.matches()){
                if(lineCounter >0){
                    rows.add(RowFactory.create(recordName,sequence.toString(),lineCounter));
                }
                recordName = fastaHeaderMatcher.group(1);
//                System.out.println(recordName);

                lineCounter++;
                sequence.setLength(0);
                sequence = new StringBuilder();
            }
            else {
                sequence.append(line);
            }

        }
        rows.add(RowFactory.create(recordName,sequence.toString(),lineCounter));
        br.close();
        if(lineCounter < 2) {
            LOGGER.error("At least two genomes must be provided: " + lineCounter);
            System.exit(-1);
        }
        Dataset<Row> sequenceDataset = Spark.getInstance().createDataFrame(rows,FASTA_STRUCT);

        return sequenceDataset;

    }

    public static void identifyInverseBPStartAndEndPoints(Dataset<Row> edges, Dataset<Row> recordData, String startPath, String endPath) {

        edges = edges.join(recordData.select(col(ColumnIdentifier.RECORD_ID).as("r"),col(ColumnIdentifier.LENGTH)),
                col(ColumnIdentifier.RECORD_ID).equalTo(col("r"))).drop("r");


        Dataset<Row> startTripelPlus = edges.filter(col(ColumnIdentifier.STRAND)).
                drop(ColumnIdentifier.CATEGORIES,ColumnIdentifier.GENES).
                filter(col(ColumnIdentifier.SRC).notEqual(col(ColumnIdentifier.DST)) ).
                withColumnRenamed(ColumnIdentifier.SRC,ColumnIdentifier.PREV_VERTEX).
                withColumnRenamed(ColumnIdentifier.DST,ColumnIdentifier.START_VERTEX).
                join(SparkComputer.appendIdentifier(edges.filter(col(ColumnIdentifier.STRAND)).drop(ColumnIdentifier.LENGTH) ,"2"),
                        col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"2")).
                                and(col(ColumnIdentifier.POSITION+"2").
                                        equalTo(ColumnFunctions.getRightNeighbour(ColumnIdentifier.POSITION,ColumnIdentifier.LENGTH,ColumnIdentifier.STRAND))).
                                and(col(ColumnIdentifier.DST+"2").notEqual(col(ColumnIdentifier.PREV_VERTEX)))
                ).filter(col(ColumnIdentifier.SRC+"2").notEqual(col(ColumnIdentifier.DST+"2"))).
                drop(ColumnIdentifier.SRC+"2",ColumnIdentifier.RECORD_ID+"2",ColumnIdentifier.STRAND+"2").
                withColumnRenamed(ColumnIdentifier.DST+"2",ColumnIdentifier.START_TRAVERSAL_VERTEX).persist();

//        Dataset<Row> startPoints =startTripelPlus.
//                groupBy(ColumnIdentifier.PREV_VERTEX,ColumnIdentifier.START_VERTEX).
//                agg(countDistinct(col(ColumnIdentifier.RECORD_ID)).as(ColumnIdentifier.RECORDS_IN),
//                        countDistinct(ColumnIdentifier.START_TRAVERSAL_VERTEX).as(ColumnIdentifier.COUNT)).
//                filter(col(ColumnIdentifier.COUNT).gt(1).and(col(ColumnIdentifier.RECORDS_IN).gt(1))).
//                drop(ColumnIdentifier.COUNT,ColumnIdentifier.RECORDS_IN).
//                join(SparkComputer.appendIdentifier(startTripelPlus,"s",ColumnIdentifier.START_VERTEX,ColumnIdentifier.PREV_VERTEX,ColumnIdentifier.START_TRAVERSAL_VERTEX),
//                        col(ColumnIdentifier.START_VERTEX).equalTo(col(ColumnIdentifier.START_VERTEX+"s")).
//                                and(col(ColumnIdentifier.PREV_VERTEX).equalTo(col(ColumnIdentifier.PREV_VERTEX+"s")))).
//                drop(ColumnIdentifier.START_VERTEX +"s",ColumnIdentifier.PREV_VERTEX+"s",ColumnIdentifier.POSITION,ColumnIdentifier.COUNT,ColumnIdentifier.RECORDS_OUT).
////                withColumnRenamed(ColumnIdentifier.RECORDS_OUT+"s",ColumnIdentifier.RECORDS_OUT).
//        withColumnRenamed(ColumnIdentifier.START_TRAVERSAL_VERTEX+"s",ColumnIdentifier.START_TRAVERSAL_VERTEX);

        Dataset<Row> startPoints =startTripelPlus.
                groupBy(ColumnIdentifier.PREV_VERTEX,ColumnIdentifier.START_VERTEX).
                agg(countDistinct(col(ColumnIdentifier.RECORD_ID)).as(ColumnIdentifier.RECORDS_IN),
                        countDistinct(ColumnIdentifier.START_TRAVERSAL_VERTEX).as(ColumnIdentifier.COUNT)).
                filter(col(ColumnIdentifier.COUNT).gt(1).and(col(ColumnIdentifier.RECORDS_IN).gt(1))).
                drop(ColumnIdentifier.COUNT,ColumnIdentifier.RECORDS_IN).
                join(SparkComputer.appendIdentifier(startTripelPlus,"s",ColumnIdentifier.START_VERTEX,ColumnIdentifier.PREV_VERTEX,ColumnIdentifier.START_TRAVERSAL_VERTEX),
                        col(ColumnIdentifier.START_VERTEX).equalTo(col(ColumnIdentifier.START_VERTEX+"s")).
                                and(col(ColumnIdentifier.PREV_VERTEX).equalTo(col(ColumnIdentifier.PREV_VERTEX+"s")))).
                drop(ColumnIdentifier.START_VERTEX +"s",ColumnIdentifier.PREV_VERTEX+"s",ColumnIdentifier.POSITION,ColumnIdentifier.COUNT,ColumnIdentifier.RECORDS_OUT).
//                withColumnRenamed(ColumnIdentifier.RECORDS_OUT+"s",ColumnIdentifier.RECORDS_OUT).
        withColumnRenamed(ColumnIdentifier.START_TRAVERSAL_VERTEX+"s",ColumnIdentifier.START_TRAVERSAL_VERTEX);

        SparkComputer.persistDataFrameORC(startPoints,startPath);

        Dataset<Row> startTripelMinus = edges.filter(not(col(ColumnIdentifier.STRAND))).
                drop(ColumnIdentifier.CATEGORIES,ColumnIdentifier.GENES).
                filter(col(ColumnIdentifier.SRC).notEqual(col(ColumnIdentifier.DST)) ).
                withColumnRenamed(ColumnIdentifier.SRC,ColumnIdentifier.PREV_VERTEX).
                withColumnRenamed(ColumnIdentifier.DST,ColumnIdentifier.START_VERTEX).
                join(SparkComputer.appendIdentifier(edges.filter(not(col(ColumnIdentifier.STRAND)))
                                .drop(ColumnIdentifier.LENGTH) ,"2"),
                        col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"2")).
                                and(col(ColumnIdentifier.POSITION+"2").
                                        equalTo(ColumnFunctions.getRightNeighbour(ColumnIdentifier.POSITION,ColumnIdentifier.LENGTH,ColumnIdentifier.STRAND))).
                                and(col(ColumnIdentifier.DST+"2").notEqual(col(ColumnIdentifier.PREV_VERTEX)))
                ).filter(col(ColumnIdentifier.SRC+"2").notEqual(col(ColumnIdentifier.DST+"2"))).
                drop(ColumnIdentifier.SRC+"2",ColumnIdentifier.RECORD_ID+"2",ColumnIdentifier.STRAND+"2").
                withColumnRenamed(ColumnIdentifier.DST+"2",ColumnIdentifier.START_TRAVERSAL_VERTEX).persist();

//        Dataset<Row> endPoints = startTripelPlus.union(startTripelMinus).
//                groupBy(ColumnIdentifier.START_VERTEX,ColumnIdentifier.START_TRAVERSAL_VERTEX).
//                agg(countDistinct(col(ColumnIdentifier.STRAND).as(ColumnIdentifier.STRAND+ColumnIdentifier.COUNT)),
//                        countDistinct(col(ColumnIdentifier.RECORD_ID)).as(ColumnIdentifier.RECORDS_OUT),
//                        countDistinct(ColumnIdentifier.PREV_VERTEX).as(ColumnIdentifier.COUNT)).
//                filter(col(ColumnIdentifier.STRAND+ColumnIdentifier.COUNT).gt(1).and(col(ColumnIdentifier.COUNT).gt(1)).and(col(ColumnIdentifier.RECORDS_OUT).gt(1))).
//                drop(ColumnIdentifier.STRAND+ColumnIdentifier.COUNT,ColumnIdentifier.COUNT,ColumnIdentifier.RECORDS_OUT).
//                join(SparkComputer.appendIdentifier(startTripelPlus,"s",ColumnIdentifier.START_VERTEX,ColumnIdentifier.PREV_VERTEX,ColumnIdentifier.START_TRAVERSAL_VERTEX),
//                        col(ColumnIdentifier.START_VERTEX).
//                                equalTo(col(ColumnIdentifier.START_VERTEX+"s")).
//                                and(col(ColumnIdentifier.START_TRAVERSAL_VERTEX).equalTo(col(ColumnIdentifier.START_TRAVERSAL_VERTEX+"s")))).
//                drop(ColumnIdentifier.START_VERTEX +"s",ColumnIdentifier.START_TRAVERSAL_VERTEX+"s",ColumnIdentifier.POSITION+"2",
//                        ColumnIdentifier.COUNT,ColumnIdentifier.RECORDS_IN).
////                withColumnRenamed(ColumnIdentifier.RECORDS_IN+"2",ColumnIdentifier.RECORDS_IN).
//        withColumnRenamed(ColumnIdentifier.PREV_VERTEX+"s",ColumnIdentifier.PREV_VERTEX);

        WindowSpec w = Window.partitionBy(ColumnIdentifier.START_VERTEX,ColumnIdentifier.START_TRAVERSAL_VERTEX).
                rowsBetween(Window.unboundedPreceding(),Window.unboundedFollowing());

        Dataset<Row> endPoints = startTripelPlus.union(startTripelMinus).
                withColumn(ColumnIdentifier.STRAND+ColumnIdentifier.COUNT,approx_count_distinct(col(ColumnIdentifier.STRAND),0.01).over(w)).
                filter(col(ColumnIdentifier.STRAND+ColumnIdentifier.COUNT).gt(1)).drop(ColumnIdentifier.STRAND+ColumnIdentifier.COUNT).
                withColumn(ColumnIdentifier.COUNT,approx_count_distinct(col(ColumnIdentifier.PREV_VERTEX),0.01).over(w)).
                filter(col(ColumnIdentifier.COUNT).gt(1)).drop(ColumnIdentifier.COUNT).
                withColumn(ColumnIdentifier.RECORDS_OUT,approx_count_distinct(col(ColumnIdentifier.RECORD_ID),0.01).over(w)).
                filter(col(ColumnIdentifier.RECORDS_OUT).gt(1)).drop(ColumnIdentifier.RECORDS_OUT);

        SparkComputer.persistDataFrameORC(endPoints,endPath);
        startTripelPlus = startTripelPlus.unpersist();
    }

    /***
     * @param edges
     * @param recordData
     * @param minDistance minimum length of single color branch
     * @param maxDistance maximum length of single color branch
     * @param minGeneLength minimum length to accept for an inversion block
     * @param maxGeneLength maximum length to accept for an inversion block
     * @param k
     * @param path
     */
    public static void detectInverseBreakPointCandidatesStart(Dataset<Row> edges, Dataset<Row> recordData, int minDistance, int maxDistance, int minGeneLength, int maxGeneLength, int k, String path) {
        String endIdentifier = "end";
        String endIdentifier2 = "end2";
        String startIdentifier = "start";


        double genelengthdifference = 0.1;


        identifyInverseBPStartAndEndPoints(edges,recordData,path+"/SP",path+"/EP");

        Dataset<Row> startPoints = SparkComputer.readORC(path+"/SP");
        Dataset<Row> endPoints = SparkComputer.readORC(path+"/EP");

        startPoints = Spark.checkPointORC(
                startPoints
                        .filter(col(ColumnIdentifier.STRAND))
                        .withColumn(ColumnIdentifier.TOPOLOGY,lit(true)).
                        // find first and last positions of all  paths between startvertex and endvertex of recordId this should be just composed of plus strand kmers
                                join(
                                SparkComputer.appendIdentifier(endPoints.filter(col(ColumnIdentifier.STRAND)).drop(ColumnIdentifier.LENGTH),endIdentifier).
                                        withColumnRenamed(ColumnIdentifier.POSITION+endIdentifier,ColumnIdentifier.POSITION),
                                col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+endIdentifier))).

                        withColumn(ColumnIdentifier.DISTANCE+"R1",ColumnFunctions.computeDistanceExcludingBoundaryPositions(
                                ColumnIdentifier.POSITION+"2",ColumnIdentifier.POSITION,
                                ColumnIdentifier.LENGTH,ColumnIdentifier.TOPOLOGY,ColumnIdentifier.STRAND)).
                        // make sure path is not too long
                                filter(col(ColumnIdentifier.DISTANCE+"R1").between(minDistance,maxDistance)).
                        drop(ColumnIdentifier.STRAND+endIdentifier,ColumnIdentifier.RECORD_ID+endIdentifier).

                        //of identified endvertex, select edge to previous vertex not on above path with recordId2 !=recordId and composed of negative strand kmers
                                join(
                                SparkComputer.appendIdentifier(endPoints.filter(not(col(ColumnIdentifier.STRAND))),endIdentifier2),
                                col(ColumnIdentifier.START_TRAVERSAL_VERTEX+endIdentifier).equalTo(col(ColumnIdentifier.START_TRAVERSAL_VERTEX+endIdentifier2)).
                                        and(col(ColumnIdentifier.START_VERTEX+endIdentifier).equalTo(col(ColumnIdentifier.START_VERTEX+endIdentifier2))).
                                        and(col(ColumnIdentifier.PREV_VERTEX+endIdentifier).notEqual(col(ColumnIdentifier.PREV_VERTEX+endIdentifier2))).
                                        and(col(ColumnIdentifier.RECORD_ID).notEqual(col(ColumnIdentifier.RECORD_ID+endIdentifier2)))
                        ).drop(ColumnIdentifier.START_TRAVERSAL_VERTEX+endIdentifier2,ColumnIdentifier.START_VERTEX+endIdentifier2).
                        withColumnRenamed(ColumnIdentifier.LENGTH+endIdentifier2,ColumnIdentifier.LENGTH+"2").
                        withColumnRenamed(ColumnIdentifier.RECORD_ID+endIdentifier2,ColumnIdentifier.RECORD_ID+"2").
                        withColumnRenamed(ColumnIdentifier.STRAND+endIdentifier2,ColumnIdentifier.STRAND+"2").
                        // only keep the identified connection to previous vertex, if recordId2 is also annotated on succeeding vertex to startvertex not on this path
                                join(
                                SparkComputer.appendIdentifier(startPoints.filter(col(ColumnIdentifier.STRAND)).drop(ColumnIdentifier.LENGTH,ColumnIdentifier.STRAND),startIdentifier),
                                col(ColumnIdentifier.PREV_VERTEX).equalTo(col(ColumnIdentifier.PREV_VERTEX+startIdentifier)).
                                        and(col(ColumnIdentifier.START_VERTEX).equalTo(col(ColumnIdentifier.START_VERTEX+startIdentifier))).
                                        and(col(ColumnIdentifier.START_TRAVERSAL_VERTEX).notEqual(col(ColumnIdentifier.START_TRAVERSAL_VERTEX+startIdentifier))).
                                        and(col(ColumnIdentifier.RECORD_ID+"2").equalTo(col(ColumnIdentifier.RECORD_ID+startIdentifier)))
                        ).drop(ColumnIdentifier.PREV_VERTEX+startIdentifier,ColumnIdentifier.START_VERTEX+startIdentifier,ColumnIdentifier.RECORD_ID+startIdentifier).
                        select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND,ColumnIdentifier.LENGTH,ColumnIdentifier.TOPOLOGY,
                                ColumnIdentifier.RECORD_ID+"2",ColumnIdentifier.STRAND+"2",ColumnIdentifier.LENGTH+"2",
                                ColumnIdentifier.DISTANCE+"R1",
                                ColumnIdentifier.POSITION+"2",ColumnIdentifier.POSITION,
                                ColumnIdentifier.POSITION+endIdentifier2,ColumnIdentifier.POSITION+"2"+startIdentifier).
                        distinct()
                        .withColumnRenamed("position2",ColumnIdentifier.POSITION+"5")
                        .withColumnRenamed("position",ColumnIdentifier.POSITION+"6")
                        .withColumnRenamed("positionend2",ColumnIdentifier.POSITION+"4")
                        .withColumnRenamed("position2start",ColumnIdentifier.POSITION+"1")
                ,path+"/SP1").persist();



        SparkComputer.persistDataFrameORC(
                startPoints.join(SparkComputer.appendIdentifier(startPoints,"iv"),
                                col(ColumnIdentifier.RECORD_ID).lt(col(ColumnIdentifier.RECORD_ID+"iv"))
                                        .and(col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"2iv")))
                                        .and(col(ColumnIdentifier.RECORD_ID+"2").equalTo(col(ColumnIdentifier.RECORD_ID+"iv")))
                                        .and(col(ColumnIdentifier.POSITION+"5").equalTo(col(ColumnIdentifier.POSITION+"1iv")))
                                        .and(col(ColumnIdentifier.POSITION+"1").equalTo(col(ColumnIdentifier.POSITION+"5iv")))
                        )
//                .withColumn("d",ColumnFunctions.computeSmallestDistance(col(ColumnIdentifier.POSITION+"6"),col(ColumnIdentifier.POSITION+"4iv"),
//                        col(ColumnIdentifier.LENGTH),lit(true),lit(true) ))
//                .withColumn("di",ColumnFunctions.computeSmallestDistance(col(ColumnIdentifier.POSITION+"4"),col(ColumnIdentifier.POSITION+"6iv"),
//                        col(ColumnIdentifier.LENGTH+"2"),lit(true),lit(true) )).orderBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.POSITION+"5")
//                .filter(col("d").equalTo(col("di")))
//                        .withColumn("diff",abs(col("d").minus(col("di"))))

                        .withColumn("ps",pmod(col(ColumnIdentifier.POSITION+"6iv").plus(1).minus(k),col(ColumnIdentifier.LENGTH+"2")))
                        .withColumn("pe",pmod(col(ColumnIdentifier.POSITION+"4").minus(1).plus(k),col(ColumnIdentifier.LENGTH+"2")))

                        .withColumn("psR1",pmod(col(ColumnIdentifier.POSITION+"6").plus(1).minus(k),col(ColumnIdentifier.LENGTH+"2")))
                        .withColumn("peR1",pmod(col(ColumnIdentifier.POSITION+"4iv").minus(1).plus(k),col(ColumnIdentifier.LENGTH+"2")))

//                        .withColumn("d",pmod(col("pe").minus(col("ps")),col(ColumnIdentifier.LENGTH+"2")).plus(1))
                        .withColumn("d",pmod(col("peR1").minus(col("psR1")),col(ColumnIdentifier.LENGTH)).plus(1))
                        .withColumn("di",pmod(col("pe").minus(col("ps")),col(ColumnIdentifier.LENGTH+"2")).plus(1))
                        .filter(col("d").gt(0).and(col("di").gt(0)))
                        .withColumn("ratio",(abs(col("d").minus(col("di")))).divide(least(col("d"),col("di"))))
                        .filter(col("ratio").leq(genelengthdifference))
                        .filter(col("d").between(minGeneLength,maxGeneLength).and(col("di").between(minGeneLength,maxGeneLength))),

                path+"/CANDIDATES");


//        SparkComputer.persistDataFrameORC(
//                SparkComputer.addRowNumber(
//                        startPoints.
//                                // right upper branch -> pos3
//                                        join(SparkComputer.appendIdentifier(startPoints.drop(ColumnIdentifier.LENGTH,ColumnIdentifier.TOPOLOGY),endBranch),
//                                        col(ColumnIdentifier.RECORD_ID+"2").equalTo(col(ColumnIdentifier.RECORD_ID+endBranch)).
//                                                and(col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"2"+endBranch))).
//                                                and(col(ColumnIdentifier.POSITION+endIdentifier2).equalTo(col(ColumnIdentifier.POSITION+endBranch))).
//                                                and(col(ColumnIdentifier.POSITION).equalTo(col(ColumnIdentifier.POSITION+endIdentifier2+endBranch)))
//                                ).drop(ColumnIdentifier.RECORD_ID+endBranch,ColumnIdentifier.PREV_VERTEX+endIdentifier+endBranch,ColumnIdentifier.START_VERTEX+endIdentifier+endBranch,
//                                        ColumnIdentifier.START_TRAVERSAL_VERTEX+endIdentifier+endBranch,ColumnIdentifier.POSITION+endBranch,ColumnIdentifier.POSITION+endIdentifier2+endBranch).
//                                // left upper branch -> pos2
//                                        join(SparkComputer.appendIdentifier(startPoints.drop(ColumnIdentifier.LENGTH,ColumnIdentifier.TOPOLOGY),startBranch),
//                                        col(ColumnIdentifier.RECORD_ID+"2").equalTo(col(ColumnIdentifier.RECORD_ID+startBranch)).
//                                                and(col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"2"+startBranch))).
//                                                and(col(ColumnIdentifier.POSITION+"2"+startIdentifier).equalTo(col(ColumnIdentifier.POSITION+"2"+startBranch))).
//                                                and(col(ColumnIdentifier.POSITION+"2").equalTo(col(ColumnIdentifier.POSITION+"2"+startIdentifier+startBranch)))
//                                ).
//                                withColumnRenamed(ColumnIdentifier.POSITION+"2","p1").
//                                withColumnRenamed(ColumnIdentifier.POSITION+"2"+startIdentifier,"p2").
//                                withColumnRenamed(ColumnIdentifier.POSITION,"p3").
//                                withColumnRenamed(ColumnIdentifier.POSITION+endIdentifier2,"p4").
//                                withColumnRenamed(ColumnIdentifier.POSITION+"2"+endBranch,"p5").
//                                withColumnRenamed(ColumnIdentifier.POSITION+startBranch,"p6").
//                                select(
//                                        ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"2",
//                                        ColumnIdentifier.STRAND,ColumnIdentifier.LENGTH,
//                                        ColumnIdentifier.STRAND+"2",ColumnIdentifier.LENGTH+"2",
//                                        ColumnIdentifier.TOPOLOGY,
//                                        ColumnIdentifier.DISTANCE+"R1",ColumnIdentifier.DISTANCE+"R2",
//                                        "p1","p3",
//                                        "p2","p6",
//                                        "p5","p4").distinct(),ColumnIdentifier.ROW_NUMBER),path+"/SP2");


    }

    public static Dataset<Row>  compareSequenceSimilarityAtBranchEndsForInversions(Dataset<Row> path, Dataset<Row> sequences, int k, int shift, double matchFraction,int minIdentical)  {

        sequences = sequences.drop(ColumnIdentifier.SEQUENCE_LENGTH);
        Dataset<Row> seq =
                PairwiseAligner.addSubsequencesToRight2(sequences,
                        PairwiseAligner.addSubsequencesToRight2(sequences,
                                path
                                        .withColumn("pR2R", pmod(col("ps").plus(shift),col(ColumnIdentifier.LENGTH+"2")))
                                        .withColumn("pR1R",pmod(col(ColumnIdentifier.POSITION+"6").minus(k-1).plus(shift),col(ColumnIdentifier.LENGTH))),
                                col("di"),
                                col("pR2R"),
                                col(ColumnIdentifier.RECORD_ID+"2")).
//                                                pmod(col(ColumnIdentifier.POSITION+"4").plus(1),col(ColumnIdentifier.SEQUENCE_LENGTH+"2")),col(ColumnIdentifier.RECORD_ID+"2")).
        withColumnRenamed(ColumnIdentifier.SUBSEQUENCE,"RR2").withColumn("RR2",reverse(translate(col("RR2"),
                                "ACTGUYRSWKMBVDHN","TGACARYSWMKVBHDN"))),
                        col("d"),
                        col("pR1R"),
                        col(ColumnIdentifier.RECORD_ID)).
//                                pmod(col(ColumnIdentifier.POSITION+"6").plus(1),col(ColumnIdentifier.SEQUENCE_LENGTH)),col(ColumnIdentifier.RECORD_ID)).
        withColumnRenamed(ColumnIdentifier.SUBSEQUENCE,"RR1").persist();



        Dataset<Row> scoresR =

                seq.select("RR1","RR2").distinct().
                        // query is branch with recordId, target branch with recordId2
                                withColumn(ColumnIdentifier.ALIGN_RESULT+"R",callUDF(PairwiseAligner.ALIGN_GLOBAL_FAST_UDF,col("RR1"),col("RR2")));



        seq = seq
                .join(scoresR.select(col("RR1").as("RR1j"),col("RR2").as("RR2j"),col(ColumnIdentifier.ALIGN_RESULT+"R")),
                        col("RR1").equalTo(col("RR1j")).and(col("RR2").equalTo(col("RR2j"))))
                .drop("RR1j","RR2j").
                withColumn("pQ1right",col("pR1R")).
                withColumn("pT1right",col("pR2R")).
                withColumn("pQ2right",pmod(col("pQ1right").plus(col(ColumnIdentifier.ALIGN_RESULT+"R.pQ2")).minus(1),col(ColumnIdentifier.LENGTH))).
                withColumn("pT2right",pmod(col("pT1right").plus(col(ColumnIdentifier.ALIGN_RESULT+"R.pT2")).minus(1),col(ColumnIdentifier.LENGTH+"2")))
                .filter(col(ColumnIdentifier.ALIGN_RESULT+"R."+ColumnIdentifier.IDENTICAL).geq(minIdentical))
                .filter(col(ColumnIdentifier.ALIGN_RESULT+"R."+ColumnIdentifier.EVALUE).geq(matchFraction));

        return seq;
    }

    public static void identifyInversionBreakPoints(Dataset<Row> edges, Dataset<Row> sequences, Dataset<Row> recordData, int minDistance, int maxDistance, int minGeneLength, int maxGeneLength,
                                                    double matchFraction, int minIdentical, int k, String path, boolean cyclic) throws IOException {


        {

            LOGGER.debug("Computing candidates");

            Spark.clearDirectory(path+"/CANDIDATES");
            detectInverseBreakPointCandidatesStart(edges,recordData,minDistance,maxDistance,minGeneLength,maxGeneLength,k,path);
            Spark.clearDirectory(path+"/EP");
            Spark.clearDirectory(path+"/SP");
            Spark.clearDirectory(path+"/SP1");


        }
        {

            LOGGER.debug("Checking validity");

            Dataset<Row> scores = compareSequenceSimilarityAtBranchEndsForInversions(
                    SparkComputer.readORC(path+"/CANDIDATES"),sequences.drop(ColumnIdentifier.SEQUENCE_NAME),k,0,matchFraction,minIdentical);

            Spark.clearDirectory(path+"/SCORED_CANDIDATES_GB");
            Spark.clearDirectory(path+"/INVERSION_BREAKPOINTS_GB");
            SparkComputer.persistDataFrameORC(scores,
                    path+"/SCORED_CANDIDATES_GB");



            Dataset<Row> bps =scores

                    .select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"2",ColumnIdentifier.LENGTH,ColumnIdentifier.LENGTH+"2",

                            "d",

                            "psR1","peR1",
                            "ps","pe"
                    ).distinct()
                    .withColumnRenamed("ps","psR2").withColumnRenamed("pe","peR2")
                    .withColumn("dR1",ColumnFunctions.getCyclicRangeLength(col("psR1"),col("peR1"),col(ColumnIdentifier.LENGTH)) )
                    .withColumn("dR2",ColumnFunctions.getCyclicRangeLength(col("psR2"),col("peR2"),col(ColumnIdentifier.LENGTH+"2")) )
                    .persist();




            Dataset<Row> overlapFree = ColumnFunctions.computeOverlapPositions2(
                            ColumnFunctions.computeOverlapPositions2(bps.join(SparkComputer.appendIdentifier(bps.drop(ColumnIdentifier.SEQUENCE_NAME),"O"),col(ColumnIdentifier.RECORD_ID)
                                                    .equalTo(col(ColumnIdentifier.RECORD_ID+"O"))
                                                    .and(col(ColumnIdentifier.RECORD_ID+"2").equalTo(col(ColumnIdentifier.RECORD_ID+"2O")))
                                                    .and(not(col("psR1").equalTo(col("psR1O")).and(col("psR2").equalTo(col("psR2O")))
                                                            .and(col("peR1").equalTo(col("peR1O"))) .and(col("peR2").equalTo(col("peR2O"))) )))
//                                                    .orderBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.RECORD_ID+"2","psR1")
                                            ,
                                            col("psR1"),col("peR1"),col("psR1O"),col("peR1O"),col("dR1"),col("dR1O"),col(ColumnIdentifier.LENGTH),
                                            ColumnIdentifier.OVERLAP+"R1"

                                    )

                                    .withColumn("f1",
                                            col("dR1O").divide(
                                                    ColumnFunctions.getCyclicRangeLength(element_at(col(ColumnIdentifier.OVERLAP+"R1"),1),
                                                            element_at(col(ColumnIdentifier.OVERLAP+"R1"),2),col(ColumnIdentifier.LENGTH))))
                                    .filter(col("f1").between(0.9,1))
                            ,
                            col("psR2"),col("peR2"),col("psR2O"),col("peR2O"),col("dR2"),col("dR2O"),col(ColumnIdentifier.LENGTH+"2"),
                            ColumnIdentifier.OVERLAP+"R2"

                    )                .withColumn("f2",
                            col("dR2O").divide(
                                    ColumnFunctions.getCyclicRangeLength(element_at(col(ColumnIdentifier.OVERLAP+"R2"),1),
                                            element_at(col(ColumnIdentifier.OVERLAP+"R2"),2),col(ColumnIdentifier.LENGTH))))
                    .filter(col("f2").between(0.9,1));





            bps = bps.join(
                    overlapFree.select("recordIdO","recordId2O","psR1O","peR1O","psR2O","peR2O").distinct(),
                    col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.RECORD_ID+"O"))
                            .and(col(ColumnIdentifier.RECORD_ID+"2").equalTo(col(ColumnIdentifier.RECORD_ID+"2O")))
                            .and(col("psR1").equalTo(col("psR1O")))
                            .and(col("psR2").equalTo(col("psR2O")))
                            .and(col("peR1").equalTo(col("peR1O")))
                            .and(col("peR2").equalTo(col("peR2O"))) ,
                    SparkComputer.JOIN_TYPES.LEFT_ANTI
            );

            if(!cyclic){
                bps = bps.filter(col("psR1").leq(col("peR1")).and(col("psR2").leq(col("peR2"))));
            }





            SparkComputer.writeToCSVTab(bps
                    .join(sequences.withColumnRenamed(ColumnIdentifier.RECORD_ID,"r"),col(ColumnIdentifier.RECORD_ID).equalTo(col("r")))
                            .withColumnRenamed(ColumnIdentifier.SEQUENCE_NAME,"seq_id_1").drop("r")
                            .join(sequences.withColumnRenamed(ColumnIdentifier.RECORD_ID,"r"),col(ColumnIdentifier.RECORD_ID+"2").equalTo(col("r")))
                            .withColumnRenamed(ColumnIdentifier.SEQUENCE_NAME,"seq_id_2")

                            .withColumnRenamed("psR1","ps_1")
                            .withColumnRenamed("peR1","pe_1")
                            .withColumnRenamed("psR2","ps_2")
                            .withColumnRenamed("peR2","pe_2")
                            .withColumnRenamed(ColumnIdentifier.LENGTH,ColumnIdentifier.SEQUENCE_LENGTH+"_1")
                            .withColumnRenamed(ColumnIdentifier.LENGTH+"2",ColumnIdentifier.SEQUENCE_LENGTH+"_2")
                            .select("seq_id_1","seq_id_2","ps_1","pe_1","ps_2","pe_2",ColumnIdentifier.SEQUENCE_LENGTH+"_1",ColumnIdentifier.SEQUENCE_LENGTH+"_2")
                            .orderBy("seq_id_1","ps_1","pe_1")
                    ,path+"/TMP");

            String filename = Arrays.stream(new File(path+"/TMP").listFiles()).filter(f -> f.getName().endsWith(".csv"))
                    .map(f -> f.getAbsolutePath()).collect(Collectors.toList()).get(0);
//            System.out.println(filename);

            Files.move(new File(filename).toPath(),new File(path+"/../inversion_breakpoints").toPath());
            Spark.clearDirectory(path);
        }



    }






}

