package de.uni_leipzig.informatik.pacosy.mitos.core.util;

import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.FileIO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class ProjectDirectoryManager {

    private static Map<DIRECTORIES,String> directories;

    private static Logger LOGGER = LoggerFactory.getLogger(ProjectDirectoryManager.class);


    public enum DIRECTORIES {



        PROJECT_DIR,
        GRAPH_DATA_DIR,
        STATISTICS_DIR,
        RECORD_SUBGRAPH_DIR,
        ALIGN_SCORERS_DIR,
        EDGE_WEIGHTS_DIR,
        ACCESS_DIR,
        SCRIPTS_DIR;
    }

    static {
//        init(FileIO.getEntryFromPropertyFile(new File(Resources.getResource("mitos.properties").getFile()),"project.dir"));
        InputStream in = ProjectDirectoryManager.class.getResourceAsStream("/mitos.properties");
        if(in == null) {
            in = ProjectDirectoryManager.class.getResourceAsStream("mitos.properties");
        }
        init(FileIO.getEntryFromPropertyFile(in,"project.dir"));

    }

    private ProjectDirectoryManager() {

    }


    private static void init(String projectDirectory) {
        directories = new HashMap<>();

        directories.put(DIRECTORIES.PROJECT_DIR, projectDirectory);

        directories.put(DIRECTORIES.GRAPH_DATA_DIR,projectDirectory+ "/graphData");
        directories.put(DIRECTORIES.ACCESS_DIR,projectDirectory+ "/accessProperties");

        directories.put(DIRECTORIES.STATISTICS_DIR,projectDirectory+ "/statistics");
        directories.put(DIRECTORIES.EDGE_WEIGHTS_DIR,directories.get(DIRECTORIES.STATISTICS_DIR)+"/edgeWeights");
        directories.put(DIRECTORIES.ALIGN_SCORERS_DIR,directories.get(DIRECTORIES.STATISTICS_DIR)+ "/alignScores" );
        directories.put(DIRECTORIES.SCRIPTS_DIR,projectDirectory+ "/scripts");

        mkdirs();

    }

    public static void setProjectDirectory(String projectDirectory) {
        init(projectDirectory);
    }

    public static void printDirectories() {
        for(Map.Entry<DIRECTORIES,String> entry: directories.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
    }



    private static void mkdirs() {
        directories.values().stream().forEach(d -> new File(d).mkdirs());
    }


    // get directories
    public static Map<DIRECTORIES, String> getDirectories() {
        return directories;
    }



    public static String getACCESS_DIR() {
        return directories.get(DIRECTORIES.ACCESS_DIR);
    }

    public static String getPROJECT_DIR() {
        return directories.get(DIRECTORIES.PROJECT_DIR);
    }

    public static void setPROJECT_DIR(String PROJECT_DIR) {
        directories.put(DIRECTORIES.PROJECT_DIR,PROJECT_DIR);
    }

    public static String getGRAPH_DATA_DIR() {
        return directories.get(DIRECTORIES.GRAPH_DATA_DIR);
    }

    public static void setGRAPH_DATA_DIR(String GRAPH_DATA_DIR) {
        directories.put(DIRECTORIES.GRAPH_DATA_DIR,GRAPH_DATA_DIR);
    }

    public static String getSTATISTICS_DIR() {
        return directories.get(DIRECTORIES.STATISTICS_DIR);
    }

    public static void setSTATISTICS_DIR(String STATISTICS_DIR) {
        directories.put(DIRECTORIES.STATISTICS_DIR,STATISTICS_DIR);
    }

    public static String getALIGN_SCORERS_DIR() {
        return directories.get(DIRECTORIES.ALIGN_SCORERS_DIR);
    }

    public static void setALIGN_SCORERS_DIR(String ALIGN_SCORERS_DIR) {
        directories.put(DIRECTORIES.ALIGN_SCORERS_DIR,ALIGN_SCORERS_DIR);
    }



    public static String getSCRIPTS_DIR() {
        return directories.get(DIRECTORIES.SCRIPTS_DIR);
    }


    public static void setRECORD_SUBGRAPH_DIR(String GRAPH_FILES) {
        directories.put(DIRECTORIES.RECORD_SUBGRAPH_DIR,GRAPH_FILES);
    }


}
