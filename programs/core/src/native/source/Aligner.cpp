#include "seqanWrapper.h"
//#include <jni.h>

#include <iostream>

#include <seqan/align.h>
#undef NDEBUG
#define NDEBUG

std::string javaClassName = "de/uni_leipzig/informatik/pacosy/mitos/core/fastalignment/align/Alignment";

// compute full local sequence alignment return true if alignment is of sufficient quality, false otherwise
JNIEXPORT jboolean JNICALL Java_de_uni_1leipzig_informatik_pacosy_mitos_core_fastalignment_Aligner_alignunbandedfilter
(JNIEnv * env, jclass /*classObj*/, jstring seqA, jstring seqB,jint match, jint mismatch, jint gap_open, jint gap_ext, jdouble K, jdouble H, jdouble lambda, jint minMatchCount, jdouble eValueThreshold) {


  using Score_t = int;
  using Sequence_t = seqan::String<char>;
  using Align_t = seqan::Align<Sequence_t, seqan::ArrayGaps>;


  // convert java strings to seqan strings
  Sequence_t seqACpp = env->GetStringUTFChars(seqA, nullptr);
  Sequence_t seqBCpp = env->GetStringUTFChars(seqB, nullptr);



  #ifndef NDEBUG
  std::cout << "SeqA:      " << seqACpp << std::endl;
  std::cout << "SeqB:      " << seqBCpp << std::endl;
  std::cout << "match:     " << match << std::endl;
  std::cout << "mismatch:  " << mismatch << std::endl;
  std::cout << "gap_open:  " << gap_open << std::endl;
  std::cout << "gap_ext:   " << gap_ext << std::endl;
  #endif

  // compute alignment

  Align_t align;
  seqan::resize(rows(align), 2);
  seqan::assignSource(seqan::row(align, 0), seqACpp);
  seqan::assignSource(seqan::row(align, 1), seqBCpp);


  Score_t score = seqan::localAlignment(align,
				     seqan::Score<Score_t>(match,
							   mismatch,
							   gap_ext,
							   gap_open));

  if(score == seqan::MinValue<Score_t>::VALUE) {
   #ifndef NDEBUG
    std::cout << "No alignment could be found within the specified band" << std::endl;
   #endif
   return false;
  }

  #ifndef NDEBUG
  std::cout << align << std::endl;
  #endif

  auto posSeqABegin = seqan::clippedBeginPosition(seqan::row(align, 0));

  if(posSeqABegin != 0) {
    #ifndef NDEBUG
    std::cout << "seqA position not 0" << std::endl;
    #endif
    return false;
  }
    auto posSeqBBegin = seqan::clippedBeginPosition(seqan::row(align, 1));
    if(posSeqBBegin != 0) {
        #ifndef NDEBUG
        std::cout << "seqB position not 0" << std::endl;
        #endif
      return false;
    }

  // compute number of matching nucleotides in alignment

//  auto alignedSeqA = seqan::row(align, 0);
//  auto alignedSeqB = seqan::row(align, 1);
  auto alignedSeqA = seqan::row(align, 1);
  auto alignedSeqB = seqan::row(align, 0);

  int matchCount = 0;
  for(std::size_t alignIdx = 0; alignIdx < seqan::length(alignedSeqB); alignIdx++)
  {
      if(alignedSeqA[alignIdx] == alignedSeqB[alignIdx])
        matchCount++;
  }
    #ifndef NDEBUG
    std::cout << "match count" << matchCount << std::endl;
    #endif

  if(matchCount <= minMatchCount) {
          #ifndef NDEBUG
          std::cout << "Too small match count" << matchCount << std::endl;
          #endif
          return false;
  }

  int l1 = length(seqACpp);
  int l2 = length(seqBCpp);

  double shiftValue = log(K*l1*l2)/H;
  double eVal =  K*(l1-shiftValue)*(l2-shiftValue)*exp(-lambda*score);
   #ifndef NDEBUG
   std::cout << eVal << std::endl;
   #endif
  if(eVal >= eValueThreshold  ) {
          #ifndef NDEBUG
          std::cout << "Insufficient evalue" << eVal << std::endl;
          #endif
          return false;
  }


  return true;
}


// compute full local sequence alignment
JNIEXPORT jobject JNICALL Java_de_uni_1leipzig_informatik_pacosy_mitos_core_fastalignment_Aligner_alignunbanded
(JNIEnv * env, jclass /*classObj*/, jstring seqA, jstring seqB,jint match, jint mismatch, jint gap_open, jint gap_ext) {

  using Score_t = int;
  using Sequence_t = seqan::String<char>;
  using Align_t = seqan::Align<Sequence_t, seqan::ArrayGaps>;

  // create java object holding the alignment results
  jclass javaLocalClass = env->FindClass(javaClassName.c_str());

  #ifndef NDEBUG
  if (javaLocalClass == nullptr) {
    std::cout << "Find Class Failed." << std::endl;
  } else {
    std::cout << "Found class." << std::endl;
  }
  #endif
  jclass javaGlobalClass = reinterpret_cast<jclass>(env->NewGlobalRef(javaLocalClass));

  jmethodID javaConstructor = env->GetMethodID(javaGlobalClass, "<init>", "(IIIIII)V");

  #ifndef NDEBUG
  if (javaConstructor == NULL) {
    std::cout << "Find method Failed." << std::endl;
  } else {
    std::cout << "Found method." << std::endl;
  }
  #endif

  // convert java strings to seqan strings
  Sequence_t seqACpp = env->GetStringUTFChars(seqA, nullptr);
  Sequence_t seqBCpp = env->GetStringUTFChars(seqB, nullptr);



  #ifndef NDEBUG
  std::cout << "SeqA:      " << seqACpp << std::endl;
  std::cout << "SeqB:      " << seqBCpp << std::endl;
  std::cout << "match:     " << match << std::endl;
  std::cout << "mismatch:  " << mismatch << std::endl;
  std::cout << "gap_open:  " << gap_open << std::endl;
  std::cout << "gap_ext:   " << gap_ext << std::endl;
  #endif

  // compute alignment

  Align_t align;
  seqan::resize(rows(align), 2);
  seqan::assignSource(seqan::row(align, 0), seqACpp);
  seqan::assignSource(seqan::row(align, 1), seqBCpp);


  Score_t score = seqan::localAlignment(align,
				     seqan::Score<Score_t>(match,
							   mismatch,
							   gap_ext,
							   gap_open));

  if(score == seqan::MinValue<Score_t>::VALUE) {
   #ifndef NDEBUG
    std::cout << "No alignment could be found within the specified band" << std::endl;
   #endif
   return env->NewObject(javaGlobalClass, javaConstructor, score, 0, 0, 0, 0, 0);
  }
 #ifndef NDEBUG
  std::cout << align << std::endl;
  #endif

  auto posSeqABegin = seqan::clippedBeginPosition(seqan::row(align, 0));
  auto posSeqAEnd = seqan::clippedEndPosition(seqan::row(align, 0));
  auto posSeqBBegin = seqan::clippedBeginPosition(seqan::row(align, 1));
  auto posSeqBEnd = seqan::clippedEndPosition(seqan::row(align, 1));

  // compute number of matching nucleotides in alignment

//  auto alignedSeqA = seqan::row(align, 0);
//  auto alignedSeqB = seqan::row(align, 1);
  auto alignedSeqA = seqan::row(align, 1);
  auto alignedSeqB = seqan::row(align, 0);

  std::size_t matchCount = 0;
  for(std::size_t alignIdx = 0; alignIdx < seqan::length(alignedSeqB); alignIdx++)
  {
      if(alignedSeqA[alignIdx] == alignedSeqB[alignIdx])
        matchCount++;
  }






  jobject alignmentObj = env->NewObject(javaGlobalClass, javaConstructor, score, posSeqABegin, posSeqAEnd, posSeqBBegin, posSeqBEnd, matchCount);
  return alignmentObj;
}

// compute banded local sequence alignment
JNIEXPORT jobject JNICALL Java_de_uni_1leipzig_informatik_pacosy_mitos_core_fastalignment_Aligner_align
(JNIEnv * env, jclass /*classObj*/, jstring seqA, jstring seqB, jint lowerDiag, jint upperDiag, jint match, jint mismatch, jint gap_open, jint gap_ext) {

  assert(upperDiag > 0);
  assert(lowerDiag < 0);

  using Score_t = int;
  using Sequence_t = seqan::String<char>;
  using Align_t = seqan::Align<Sequence_t, seqan::ArrayGaps>;

  // create java object holding the alignment results
  jclass javaLocalClass = env->FindClass(javaClassName.c_str());

  #ifndef NDEBUG
  if (javaLocalClass == nullptr) {
    std::cout << "Find Class Failed." << std::endl;
  } else {
    std::cout << "Found class." << std::endl;
  }
  #endif

  jclass javaGlobalClass = reinterpret_cast<jclass>(env->NewGlobalRef(javaLocalClass));

  jmethodID javaConstructor = env->GetMethodID(javaGlobalClass, "<init>", "(IIIIII)V");

  #ifndef NDEBUG
  if (javaConstructor == NULL) {
    std::cout << "Find method Failed." << std::endl;
  } else {
    std::cout << "Found method." << std::endl;
  }
  #endif

  // convert java strings to seqan strings
  Sequence_t seqACpp = env->GetStringUTFChars(seqA, nullptr);
  Sequence_t seqBCpp = env->GetStringUTFChars(seqB, nullptr);



  #ifndef NDEBUG
  std::cout << "SeqA:      " << seqACpp << std::endl;
  std::cout << "SeqB:      " << seqBCpp << std::endl;
  std::cout << "lowerDiag: " << lowerDiag << std::endl;
  std::cout << "upperDiag: " << upperDiag << std::endl;
  std::cout << "match:     " << match << std::endl;
  std::cout << "mismatch:  " << mismatch << std::endl;
  std::cout << "gap_open:  " << gap_open << std::endl;
  std::cout << "gap_ext:   " << gap_ext << std::endl;
  #endif

  // compute alignment

  Align_t align;
  seqan::resize(rows(align), 2);
  seqan::assignSource(seqan::row(align, 0), seqACpp);
  seqan::assignSource(seqan::row(align, 1), seqBCpp);


  Score_t score = seqan::localAlignment(align,
				     seqan::Score<Score_t>(match,
							   mismatch,
							   gap_ext,
							   gap_open),
				     lowerDiag,
				     upperDiag);

  if(score == seqan::MinValue<Score_t>::VALUE) {
   #ifndef NDEBUG
    std::cout << "No alignment could be found within the specified band" << std::endl;
   #endif
   return env->NewObject(javaGlobalClass, javaConstructor, score, 0, 0, 0, 0, 0);
  }

  #ifndef NDEBUG
  std::cout << align << std::endl;
  #endif

  auto posSeqABegin = seqan::clippedBeginPosition(seqan::row(align, 0));
  auto posSeqAEnd = seqan::clippedEndPosition(seqan::row(align, 0));
  auto posSeqBBegin = seqan::clippedBeginPosition(seqan::row(align, 1));
  auto posSeqBEnd = seqan::clippedEndPosition(seqan::row(align, 1));

  // compute number of matching nucleotides in alignment
  auto alignedSeqA = seqan::row(align, 0);
  auto alignedSeqB = seqan::row(align, 1);

  std::size_t matchCount = 0;
  for(std::size_t alignIdx = 0; alignIdx < seqan::length(alignedSeqB); alignIdx++)
  {
      if(alignedSeqA[alignIdx] == alignedSeqB[alignIdx])
        matchCount++;
  }




  jobject alignmentObj = env->NewObject(javaGlobalClass, javaConstructor, score, posSeqABegin, posSeqAEnd, posSeqBBegin, posSeqBEnd, matchCount);
  return alignmentObj;
}

// compute full global sequence alignment
JNIEXPORT jobject JNICALL Java_de_uni_1leipzig_informatik_pacosy_mitos_core_fastalignment_Aligner_alignunbandedglobal
(JNIEnv * env, jclass /*classObj*/, jstring seqA, jstring seqB,jint match, jint mismatch, jint gap_open, jint gap_ext) {

  using Score_t = int;
  using Sequence_t = seqan::String<char>;
  using Align_t = seqan::Align<Sequence_t, seqan::ArrayGaps>;

  // create java object holding the alignment results
  jclass javaLocalClass = env->FindClass(javaClassName.c_str());

  #ifndef NDEBUG
  if (javaLocalClass == nullptr) {
    std::cout << "Find Class Failed." << std::endl;
  } else {
    std::cout << "Found class." << std::endl;
  }
  #endif
  jclass javaGlobalClass = reinterpret_cast<jclass>(env->NewGlobalRef(javaLocalClass));

  jmethodID javaConstructor = env->GetMethodID(javaGlobalClass, "<init>", "(IIIIII)V");

  #ifndef NDEBUG
  if (javaConstructor == NULL) {
    std::cout << "Find method Failed." << std::endl;
  } else {
    std::cout << "Found method." << std::endl;
  }
  #endif

  // convert java strings to seqan strings
  Sequence_t seqACpp = env->GetStringUTFChars(seqA, nullptr);
  Sequence_t seqBCpp = env->GetStringUTFChars(seqB, nullptr);



  #ifndef NDEBUG
  std::cout << "SeqA:      " << seqACpp << std::endl;
  std::cout << "SeqB:      " << seqBCpp << std::endl;
  std::cout << "match:     " << match << std::endl;
  std::cout << "mismatch:  " << mismatch << std::endl;
  std::cout << "gap_open:  " << gap_open << std::endl;
  std::cout << "gap_ext:   " << gap_ext << std::endl;
  #endif

  // compute alignment

  Align_t align;
  seqan::resize(rows(align), 2);
  seqan::assignSource(seqan::row(align, 0), seqACpp);
  seqan::assignSource(seqan::row(align, 1), seqBCpp);


  Score_t score = seqan::globalAlignment(align,
				     seqan::Score<Score_t>(match,
							   mismatch,
							   gap_ext,
							   gap_open));

  if(score == seqan::MinValue<Score_t>::VALUE) {
   #ifndef NDEBUG
    std::cout << "No alignment could be found within the specified band" << std::endl;
   #endif
   return env->NewObject(javaGlobalClass, javaConstructor, score, 0, 0, 0, 0, 0);
  }
 #ifndef NDEBUG
  std::cout << align << std::endl;
  #endif

  auto posSeqABegin = seqan::clippedBeginPosition(seqan::row(align, 0));
  auto posSeqAEnd = seqan::clippedEndPosition(seqan::row(align, 0));
  auto posSeqBBegin = seqan::clippedBeginPosition(seqan::row(align, 1));
  auto posSeqBEnd = seqan::clippedEndPosition(seqan::row(align, 1));

  // compute number of matching nucleotides in alignment

//  auto alignedSeqA = seqan::row(align, 0);
//  auto alignedSeqB = seqan::row(align, 1);
  auto alignedSeqA = seqan::row(align, 1);
  auto alignedSeqB = seqan::row(align, 0);

  std::size_t matchCount = 0;
  for(std::size_t alignIdx = 0; alignIdx < seqan::length(alignedSeqB); alignIdx++)
  {
      if(alignedSeqA[alignIdx] == alignedSeqB[alignIdx])
        matchCount++;
  }

  jobject alignmentObj = env->NewObject(javaGlobalClass, javaConstructor, score, posSeqABegin, posSeqAEnd, posSeqBBegin, posSeqBEnd, matchCount);
  return alignmentObj;
}
