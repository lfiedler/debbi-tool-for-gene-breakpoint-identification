package de.uni_leipzig.informatik.pacosy.mitos.core.fastalignment;


import de.uni_leipzig.informatik.pacosy.mitos.core.fastalignment.align.Alignment;


public class Aligner {

    static {
        System.loadLibrary("align");

    }

    private static native Alignment align(String seqA, String seqB, int lowerDiag, int upperDiag, int match, int mismatch, int gap_open, int gap_ext);

    private static native Alignment alignunbandedglobal(String seqA, String seqB, int match, int mismatch, int gap_open, int gap_ext);
    
    private static native Alignment alignunbanded(String seqA, String seqB, int match, int mismatch, int gap_open, int gap_ext);

    private static native boolean alignunbandedfilter(String seqA, String seqB,
                                                      int match, int mismatch, int gap_open, int gap_ext,
                                                      double K, double H, double lambda,
                                                      int minMatchCount, double eValueThreshold);

    public static boolean alignunbandedfilter(String seqA, String seqB, int minMatchCount, double eValueThreshold){
        return alignunbandedfilter(seqA,seqB,
                PairwiseAligner.match,PairwiseAligner.missmatch,PairwiseAligner.open+PairwiseAligner.extend,PairwiseAligner.extend,
                PairwiseAligner.evalue.getK(),PairwiseAligner.evalue.getH(),PairwiseAligner.evalue.getLambda(),
                minMatchCount, eValueThreshold
                );
    }

    public static Alignment alignLocal(String seqA, String seqB, int lowerDiag, int upperDiag) {
        return align(seqA,seqB,lowerDiag,upperDiag, PairwiseAligner.match,PairwiseAligner.missmatch,PairwiseAligner.open+PairwiseAligner.extend,PairwiseAligner.extend);
    }

    public static Alignment alignLocal(String seqA, String seqB) {;
        return alignunbanded(seqA,seqB, PairwiseAligner.match,PairwiseAligner.missmatch,PairwiseAligner.open+PairwiseAligner.extend,PairwiseAligner.extend);
    }

    public static Alignment alignGlobal(String seqA, String seqB) {;
        return alignunbandedglobal(seqA,seqB, PairwiseAligner.match,PairwiseAligner.missmatch,PairwiseAligner.open+PairwiseAligner.extend,PairwiseAligner.extend);
    }



}
