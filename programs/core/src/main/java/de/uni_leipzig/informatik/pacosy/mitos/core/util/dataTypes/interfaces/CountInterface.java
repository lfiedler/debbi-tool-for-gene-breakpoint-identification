package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces;

import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.ColumnIdentifier;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.expressions.javalang.typed;
import scala.Tuple2;

public interface CountInterface {

    int getCount();

    void setCount(int count);

    static <T extends CountInterface> FilterFunction<T> countGtZeroFilter(Class<T> tClass) {
        return countGtFilter(0,tClass);
    }

    static <T extends CountInterface> FilterFunction<T> countGtFilter(int minCount, Class<T> tClass) {
        return k -> k.getCount() > minCount;
    }

    static <T extends CountInterface> Dataset<Tuple2<Integer,Long>> countDistribution(Dataset<T> dataset) {
        return dataset.groupByKey((MapFunction<T,Integer>) t -> t.getCount(), Encoders.INT()).
                agg(typed.count((MapFunction<T,Object>) r -> r.getCount()).name(ColumnIdentifier.COUNT));
    }
}
