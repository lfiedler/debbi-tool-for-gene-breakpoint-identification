package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

public class DBGEdgeDT extends EdgeDT{

    protected int recordId;
    private int position;
    private boolean strand;
    public static final Encoder<DBGEdgeDT> ENCODER = Encoders.bean(DBGEdgeDT.class);

    public DBGEdgeDT(String src, String dst, int recordId, int position, boolean strand) {
        super(src, dst);
        this.recordId = recordId;
        this.position = position;
        this.strand = strand;
    }

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean isStrand() {
        return strand;
    }

    public void setStrand(boolean strand) {
        this.strand = strand;
    }
}
