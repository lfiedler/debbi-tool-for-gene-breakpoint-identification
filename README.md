# DeBBI
Commandline tool for gene breakpoint identification

## Prerequisites
- Java 8 (jdk + jre)
- Maven
- SeqAn, for installation instructions see: https://seqan.readthedocs.io/en/master/Infrastructure/Use/Install.html
- R (>= 3.6) and packages: dplyr, stats4, usedist, cluster, extRemes


## Build

./init.sh

## Run

For both dislocation and inversion breakpoints, the genome sequences of interest must be supplied in a multi fasta file. At least two sequences must be supplied.
The path to the file is specified with --fasta-file &lt;path to the fasta file&gt;. Also, the directory to where all results should be stored needs to be specified with --result-directory &lt;path to desired result directory&gt;.

To detect dislocation breakpoints the --dislocation-breakpoints flag must be set. To detect inversion breakpoints the --inversion-breakpoints flag must be set.

The number of threads can be adapted by changing parameter threads in the accessProperties/spark.properties file.

## Examples

Example data can be found in the test-data folder. The test-data/dislocation folder contains three examples for dislocation breakpoint analysis and the test-data/dislocation folder contains one example for inversion breakpoint analysis.

**Example 1: Minimal working example for dislocation breakpoints**
./DeBBI --fasta-file ./test-data/dislocation/decapodiformes.fna --result-directory ./results --dislocation-breakpoints
Detect dislocation breakpoints of the sequences stored in the genomes.fna multi fasta file and store the results in /home/results.

**Example 2: Minimal working example for inversion breakpoints**
./DeBBI --fasta-file ./test-data/clupeocephala.fna --result-directory ./results --inversion-breakpoints
Detect inversion breakpoints of the sequences stored in the genomes.fna multi fasta file and store the results in /home/results.

**Example 3: Find out what other parameters can be specified**
./DeBBI --help


## Bugs

Should you encounter any type of misbehavior please report to lfiedler@informatik.uni-leipzig.de
